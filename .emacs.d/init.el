;;; init.el --- Kisaragi Hiu's Emacs config  -*- lexical-binding: t; -*-
;;; Commentary:

;; Note on a convention: when configuring how a minor mode works in a
;; major mode, prefer to put the configuration in the major mode
;; block. So this:
;;
;; (leaf rust-mode :config (with-eval-after-load 'flycheck (leaf flycheck-rust)))
;;
;; Instead of this:
;;
;; (leaf flycheck :config (with-eval-after-load 'rust-mode (leaf flycheck-rust)))
;;
;; It works either way, but at least be consistent.

;;; Code:

;;;; Refuse to load on old Emacsen
(let ((minver "27"))
  (when (version< emacs-version minver)
    (error "This configuration depends on Emacs %s or higher" minver)))

;;;; Requires
(require 'seq)
(require 'map)
(require 'xdg)
(require 'subr-x)
(require 'mode-local)
(require 'cl-lib)

;;;; Set up load
(defun k/setup-load ()
  "Set up module loading."
  ;; Don't load old bytecode / native code
  (setq load-prefer-newer t)
  (cl-pushnew (expand-file-name "kisaragi" user-emacs-directory) load-path)
  (cl-pushnew (expand-file-name "ext" user-emacs-directory) load-path))

(defun k/setup-gc ()
  "Set up garbage collection."
  ;; Set `gc-cons-threshold' to a sensible value.
  (setq gc-cons-threshold (* 100 1000 1000)))

(defun k/setup-tls ()
  "Set up TLS."
  (require 'gnutls)
  ;; don't defeat the whole point of TLS
  (setq gnutls-verify-error t
        ;; "acceptably modern value" according to Radian.el
        gnutls-min-prime-bits 3072))

(defun k/setup-straight ()
  "Bootstrap Straight.el."
  (defvar straight-recipes-gnu-elpa-use-mirror t)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
  (require 'straight)
  (require 'straight-x)
  (setq straight-vc-git-default-clone-depth 1)
  ;; absolutely make sure builtin org is never loaded
  (setq load-path
        (cl-remove (thread-last data-directory
                     directory-file-name
                     file-name-directory
                     (expand-file-name "lisp")
                     (expand-file-name "org"))
                   load-path
                   :test #'equal))
  ;; Make sure we use updated python mode
  ;; Normally you'd do this for Org as well, but the above is enough
  ;; I have zero idea why (straight-use-package 'python) doesn't work.
  (straight-use-package (straight-recipes-retrieve
                         'python)))

(with-eval-after-load 'straight
  (define-advice straight-vc-clone (:around (_orig recipe) support-pin)
    "Add support for a `:pin' keyword."
    (straight--with-plist recipe
        (pin type local-repo)
      (let ((straight--default-directory (straight--repos-dir)))
        (when (file-exists-p (straight--repos-dir local-repo))
          (error "Repository already exists: %S" local-repo))
        ;; It's called "commit", but is actually just a ref as it's passed
        ;; straight to `git checkout'. See `straight-vc-git-clone'.
        ;;
        ;; The pin has precedence over the lock file.
        (let ((commit (or pin
                          (cdr (assoc local-repo (straight--lockfile-read-all))))))
          (straight-vc 'clone type recipe commit))))))

(defun k/setup-leaf ()
  "Set up leaf."
  (straight-use-package 'leaf)
  (straight-use-package 'leaf-keywords)
  (require 'leaf)
  (require 'leaf-keywords)
  (leaf-keywords-init))

(defun k/setup-leaf-timing ()
  "Advise `leaf' to report how much time each block took."
  (defvar k/leaf-timing-data nil)
  (advice-add 'leaf :around
              (lambda (old pkg &rest body)
                `(progn
                   (push (list ',pkg (current-time)) k/leaf-timing-data)
                   (message "leaf block: %s" ',pkg)
                   ,(apply old pkg body)
                   (with-current-buffer (get-buffer-create "*k/leaf timing*")
                     (when (= 0 (buffer-size))
                       (tabulated-list-mode)
                       (setq tabulated-list-format
                             (vector
                              '("leaf block" 30 t)
                              (list "seconds" 20
                                    (lambda (a b)
                                      ;; An entry is (ID ["<name>" "<seconds>"]).
                                      (setq a (elt (cadr a) 1))
                                      (setq b (elt (cadr b) 1))
                                      (< (string-to-number a) (string-to-number b))))))
                       (tabulated-list-init-header))
                     (push (list
                            nil
                            (vector
                             (format "%s" ',pkg)
                             (-some--> k/leaf-timing-data
                               (assoc ',pkg it)
                               cadr
                               (time-subtract (current-time) it)
                               float-time
                               prin1-to-string)))
                           tabulated-list-entries)
                     (revert-buffer))))))

(defun k/setup-use-package ()
  "Set up use-package."
  (straight-use-package 'use-package)
  (require 'use-package)
  (setq use-package-always-defer t))

(defun k/setup-base-libraries ()
  "Install and load a set of \"base\" libraries."
  (dolist (pkg '((tst :repo "kisaragi-hiu/tst.el" :host gitlab)
                 all-the-icons
                 async
                 parseedn
                 git
                 request
                 ts
                 uuidgen
                 xmlgen
                 xr
                 a dash f s ht))
    (straight-use-package pkg)
    (require (or (car-safe pkg)
                 pkg)))
  (require 'kisaragi-constants)
  (require 'kisaragi-helpers))

(k/setup-load)
(k/setup-tls)
(k/setup-straight)
(k/setup-gc)
(k/setup-leaf)
(with-eval-after-load 'dash
  (k/setup-leaf-timing))
(k/setup-use-package)
(k/setup-base-libraries)

;;;;; packages

(defun k/setup-modeline ()
  "Set up modeline."
  ;; Somewhat copied from https://github.com/hlissner/doom-emacs/blob/b681a2e1/modules/ui/modeline/config.el
  (dolist (pkg '(doom-modeline anzu evil-anzu))
    (straight-use-package pkg))
  (add-hook 'after-init-hook #'doom-modeline-mode)
  (add-hook 'doom-modeline-mode-hook #'size-indication-mode)
  (add-hook 'doom-modeline-mode-hook #'column-number-mode)
  (with-eval-after-load 'doom-modeline
    (add-hook 'k/load-theme-hook #'doom-modeline-refresh-bars)
    (require 'evil-anzu)
    (global-anzu-mode))
  (setq projectile-dynamic-mode-line nil
        doom-modeline-bar-width 3
        doom-modeline-icon nil
        doom-modeline-unicode-fallback t
        doom-modeline-github nil
        doom-modeline-mu4e nil
        doom-modeline-persp-name nil
        doom-modeline-minor-modes nil
        doom-modeline-major-mode-icon nil
        doom-modeline-buffer-file-name-style 'auto)
  ;; prevent flash of unstyled modeline at startup
  (unless after-init-time
    (setq-default mode-line-format nil)))

(defvar k/load-theme-hook nil "Hook run after `k/load-theme'.")
(defun k/load-theme (theme)
  "Load THEME, unless it's already the only enabled theme."
  (interactive
   (list
    (completing-read "Load custom theme: "
                     (custom-available-themes))))
  (when (symbolp theme) (setq theme (symbol-name theme)))
  (unless (and (= 1 (length custom-enabled-themes))
               (eq (car custom-enabled-themes)
                   (intern theme)))
    (mapc #'disable-theme custom-enabled-themes)
    (load-theme (intern theme) t)
    (run-hooks 'k/load-theme-hook)))

(defun k/setup-fonts ()
  "Set up fonts."
  (when (fboundp 'create-fontset-from-fontset-spec)
    (defvar k/fontset-proportional
      (create-fontset-from-fontset-spec "-*-*-*-*-*-*-*-*-*-*-*-*-fontset-proportional"))
    ;; Emacs for some reason sees Iosevka and resolves that to Noto
    ;; Serif for all characters, even when I specify it just for `latin'.
    (prog1 'default
      (set-face-attribute 'default nil
                          :font "Iosevka"
                          :height 140)
      (dolist (script '(han hangul kana bopomofo cjk-misc))
        (set-fontset-font (face-attribute 'default :fontset)
                          script "Noto Sans Mono CJK JP")))
    (prog1 'emoji
      (unless (version< emacs-version "27")
        (set-fontset-font t 'symbol "Noto Color Emoji")))
    (prog1 'variable-pitch
      (set-face-attribute 'variable-pitch nil
                          :font (k/first-available-font '("Equity OT" "Noto Serif"))
                          :height 'unspecified
                          :fontset k/fontset-proportional)
      ;; And if I don't do this it doesn't use Equity for eg. ö :)
      (set-fontset-font (face-attribute 'variable-pitch :fontset)
                        'latin
                        (k/first-available-font
                         '("Equity OT" "Noto Serif")))
      (dolist (script '(han hangul kana bopomofo cjk-misc))
        (set-fontset-font (face-attribute 'variable-pitch :fontset)
                          script
                          (k/first-available-font
                           '("Noto Serif CJK JP" "Noto Sans CJK JP")))))
    (set-face-attribute 'fixed-pitch-serif nil
                        :font (k/first-available-font
                               '("mononoki" "Fira Mono" "Source Code Pro")))))

(defun k/setup-cursors ()
  "Set up cursors."
  (cl-flet ((cursor (face &optional (style 'box))
                    (list (face-attribute face :foreground nil t) style)))
    (with-eval-after-load 'doom-modeline
      (setq evil-emacs-state-cursor     (cursor 'doom-modeline-evil-emacs-state 'bar)
            evil-normal-state-cursor    (cursor 'doom-modeline-evil-normal-state)
            evil-operator-state-cursor  (cursor 'doom-modeline-evil-normal-state 'hollow)
            evil-insert-state-cursor    (cursor 'doom-modeline-evil-insert-state 'bar)
            evil-visual-state-cursor    (cursor 'doom-modeline-evil-visual-state)
            evil-motion-state-cursor    (cursor 'doom-modeline-evil-motion-state)
            evil-replace-state-cursor   (cursor 'doom-modeline-evil-replace-state)))))

(defun k/setup-frame (&optional frame)
  "Set up my font and cursor preferences in FRAME.
Doing this mainly allows face customizations to take effect even
when using the server."
  (interactive)
  ;; otherwise the theme hasn't been applied
  (run-at-time 1 nil #'k/setup-cursors)
  (when frame (select-frame frame))
  (k/setup-fonts)
  ;; FIXME: These should be added to a hook, and setup-appearance
  ;; should then run it.
  (with-eval-after-load 'avy
    (dolist (face '(avy-lead-face avy-background-face avy-lead-face-2 avy-lead-face-1 avy-lead-face-0 avy-goto-char-timer-face))
      (set-face-attribute face nil :weight 'unspecified)))
  (with-eval-after-load 'speed-type
    (set-face-attribute 'speed-type-correct nil
                        :foreground 'unspecified
                        :inherit 'success)
    (set-face-attribute 'speed-type-mistake nil
                        :foreground 'unspecified
                        :underline 'unspecified
                        :inherit 'error))
  (with-eval-after-load 'org
    (set-face-attribute 'org-headline-done nil
                        :background "unspecified"
                        :foreground "unspecified"
                        :strike-through t
                        :underline 'unspecified)
    (set-face-attribute 'org-verse nil
                        :inherit 'unspecified
                        :background (face-attribute
                                     'org-block :background)
                        :extend t)
    ;; There doesn't seem to be a way to cancel out italics.
    (set-face-attribute 'org-quote nil
                        :slant 'unspecified))
  (with-eval-after-load 'ledger
    (set-face-attribute 'ledger-font-payee-uncleared-face nil
                        :foreground "unspecified"
                        :weight 'bold
                        :slant 'italic))
  (with-eval-after-load 'anzu
    (set-face-attribute 'anzu-mode-line nil
                        :inherit 'font-lock-constant-face
                        :foreground nil)))

(defun k/setup-appearance ()
  "Set up appearance."
  (leaf monokai-theme :straight t)
  (leaf spacemacs-theme :straight t)
  (leaf doom-themes
    :straight t
    :config
    (setq doom-org-special-tags nil)
    (doom-themes-org-config))
  (leaf k-kde-window-colors
    :unless (getenv "ANDROID_ROOT")
    :require t)
  (if (daemonp)
      (add-hook 'after-make-frame-functions #'k/setup-frame)
    (k/setup-frame))
  (add-hook 'k/load-theme-hook #'k/setup-cursors)
  (blink-cursor-mode -1)
  (ignore-errors
    (tool-bar-mode -1)
    (menu-bar-mode -1)
    (scroll-bar-mode -1))
  (if (cl-evenp (ts-month (ts-now)))
      (k/load-theme "doom-one")
    (k/load-theme "doom-solarized-light")))

(defun k/setup-compdef ()
  "Set up compdef."
  (straight-use-package '(compdef :type git :host gitlab :repo "jjzmajic/compdef"))
  (require 'compdef))

(defun k/setup-transient ()
  "Setup transient.el."
  (straight-use-package 'transient)
  (require 'transient)
  (setq transient-display-buffer-action
        '(display-buffer-in-side-window (side . bottom))))

(defun k/setup-general ()
  "Set up general.el."
  (straight-use-package 'general)
  (require 'general)
  (general-auto-unbind-keys)
  (require 'k-general-definers))

(defun k/setup-keys ()
  "Set up general keybinds."
  (general-def
    ;; this just switches to *Messages* by default
    "C-h e" #'describe-face)
  (general-def
    :keymaps 'key-translation-map
    [hiragana-katakana] [escape]
    "M-<f2>" [escape]
    "C-SPC" [escape]
    "C-@" [escape])
  (k/general-leader/frame
    "d" '(delete-frame :wk "Close frame")
    "n" '(make-frame-command :wk "New frame")
    "o" '(other-frame :wk "Jump to another frame"))
  (k/general-leader/file
    "#" '(k/open-current-file-as-sudo :wk "Edit this file with sudo")
    "b" '(consult-bookmark :wk "Bookmarks")
    "c" '(copy-file :wk "Copy a file")
    "d" '(delete-file :wk "Delete a file")
    "D" '(delete-this-file :wk "Delete this file")
    "f" #'find-file
    "l" #'find-library
    "r" '(rename-file :wk "Rename a file")
    "R" '(rename-this-file :wk "Rename this file")
    "o" '(consult-recent-file :wk "Recent files")
    "p" #'find-file-at-point ; also available as <normal>gf
    "q" (general-simulate-key
          ('evil-ex "wq RET")
          :which-key ":wq<CR>")
    ;; Some modes provide custom save functions and bind them to
    ;; C-x C-s; binding <leader>fs to `save-buffer' would miss
    ;; those bindings.
    "s" (general-simulate-key "C-x C-s"
          :which-key "Save")
    "S" (general-simulate-key
          (#'save-some-buffers "!") ; means "save the rest" in prompt
          :which-key "Save all")
    "x" '(consult-file-externally :wk "Open file externally")
    "X" '(k/open-this-file-with-system :wk "Open this externally")
    "y" (general-simulate-key
          (#'eval-expression
           "buffer-file-name RET")
          :which-key "Show current file name")
    "v" '(k/open-this-file-with-nvim
          :wk "Edit this file with Neovim"))
  (k/general-leader/file
    "e" '(:ignore t :wk "fast file access")
    "eb" `(,(k/find-file-command (f-join k/notes/ "books.org")) :wk "notes: books.org")
    "ec" '(k/visit-core-repo :wk "Core repositories")
    "ei" '(k/visit-init-file :wk "init.el")
    "eI" `(,(k/find-file-command (f-join k/notes/ "Inbox.org")) :wk "notes: inbox.org")
    "eh" `(,(k/find-file-command (f-join k/notes/ "habits/habits.org")) :wk "notes: habits.org")
    "el" '(k/ledger-visit-latest :wk "latest ledger file")
    "en" `(,(k/find-file-command (f-join k/notes/ "readme.org")) :wk "notes: main index")
    "et" '(canrylog-visit :wk "canrylog tracking file")
    "eT" `(,(k/find-file-command (f-join k/notes/ "todos.org")) :wk "notes: todos")
    "ep" `(,(k/find-file-command (f-join k/notes/ "projects.org")) :wk "notes: projects.org"))
  (k/general-leader/toggle
    ;; `visual-line-mode' redefines some editing commands. I don't
    ;; want that.
    "v" '(toggle-truncate-lines :wk "Toggle soft line wrap")
    "de" '(toggle-debug-on-error :wk "Debug on error")
    "dq" '(toggle-debug-on-quit :wk "Debug on quit"))
  (k/general-leader/buffer
    ;; `counsel-switch-buffer' previews selected buffer. I don't know
    ;; if I want that.
    "b" '(consult-buffer :wk "Switch buffer")
    "r" '(rename-buffer :wk "Rename buffer")
    "d" '(evil-delete-buffer :wk "Delete buffer and window")
    "D" '(kill-current-buffer :wk "Delete buffer only")
    ;; alternative to delete -> the key next to "d"
    ;; not f because bf and bd would be a set, making fd seem like a
    ;; similar action
    "c" '(bury-buffer :wk "Bury buffer")
    "i" '(clone-indirect-buffer :wk "New buffer visiting the same file")
    "n" '(k/new-buffer :wk "New buffer")
    "h" '(previous-buffer :wk "Switch to previous buffer")
    "l" '(next-buffer :wk "Switch to next buffer"))
  ;; Trying this out: works well in emacs-lisp-mode
  (k/general-g*
    "j" #'outline-next-heading
    "k" #'outline-previous-heading)
  ;; Like vimium
  (general-def
    :states 'motion
    "H" '(previous-buffer :wk "Switch to previous buffer")
    "L" '(next-buffer :wk "Switch to next buffer"))
  (k/general-leader
   "z" #'text-scale-adjust
   "h" '(:keymap help-map :wk "Help"))
  (k/general-leader/extra-insert-commands
    "c" '(counsel-unicode-char :wk "Insert character")
    "t" '(k/date-iso8601 :wk "Insert ISO 8601 timestamp")
    "d" '(k/today :wk "Insert a date")
    "u" '(uuidgen :wk "Insert random UUID (v4)")
    "f" '(k/insert-buffer-file-name :wk "Insert file name")
    "U" (general-simulate-key (#'universal-argument "C-x x du")
          :which-key "Insert time-based UUID (v1)")
    "n" '(k/insert-note :wk "Insert an inline note"))
  (k/general-leader/extra-editing-commands
    "c" '(cangjie-at-point :wk "Get cangjie code")
    "w" '(count-words :wk "Count words (buffer or region)")
    "h" '(opencc-replace-at-point :wk "Convert Han characters")
    "u" '(k/encode-url :wk "Encode URL")
    "U" '(k/decode-url :wk "Decode URL")
    "o" '(delete-blank-lines :wk "Delete blank lines")))

(k/setup-general)

;;;;;; Frameworks

(leaf prescient
  :straight t
  :require t
  :config
  (prescient-persist-mode)
  (setq prescient-history-length 100
        prescient-filter-method '(literal regexp fuzzy initialism)))

(leaf minibuffer
  :setq (enable-recursive-minibuffers . t)
  :init (minibuffer-depth-indicate-mode)
  :config
  (leaf selectrum
    :straight t
    :init (selectrum-mode))
  (leaf selectrum-prescient
    :straight t
    :init
    (setq selectrum-prescient-enable-filtering nil)
    (selectrum-prescient-mode))
  (leaf vertico
    :straight (vertico :host github :repo "minad/vertico"
                       :files ("*.el" "extensions/*.el"))
    :init (vertico-mode)
    :disabled t
    :config
    ;; https://github.com/hlissner/doom-emacs/blob/1ebac6f798/modules/completion/vertico/config.el#L28
    (general-def :keymaps 'vertico-map :states 'insert
      "<backspace>" #'vertico-directory-delete-char)
    (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy))
  (leaf consult
    :straight t consult-flycheck
    :init
    ;; Use Consult for `completion-at-point' et al
    (setq completion-in-region-function #'consult-completion-in-region)
    (advice-add #'completing-read-multiple
                :override #'consult-completing-read-multiple)
    (advice-add #'multi-occur
                :override #'consult-multi-occur)
    :config
    (setq consult-project-root-function #'projectile-project-root
          consult-preview-key nil
          consult-async-min-input 2
          consult-async-refresh-delay  0.15
          consult-async-input-throttle 0.2
          consult-async-input-debounce 0.1))
  (leaf marginalia
    :straight t
    :init (marginalia-mode)
    :config
    ;; List from Doom Emacs
    (dolist (x '((flycheck-error-list-set-filter . builtin)
                 (projectile-find-file . project-file)
                 (projectile-recentf . project-file)
                 (projectile-switch-to-buffer . buffer)
                 (projectile-switch-project . project-file)))
      (cl-pushnew x marginalia-command-categories)))
  (leaf orderless
    :straight t
    :require t
    :config
    (setq completion-styles '(orderless)
          ;; Use "&" to seperate strings when spaces can't be used
          ;; (eg. in Company)
          orderless-component-separator "[ &]"))
  (leaf embark
    :straight t
    :config
    (setq embark-prompter #'embark-keymap-prompter)
    (add-hook 'embark-after-export-hook
              (lambda ()
                (display-buffer (current-buffer)))))
  (leaf embark-consult
    :straight t
    :after embark consult
    :require t)
  ;; Install helm as `org-ref''s helm frontend is more usable.
  (dolist (pkg '(helm))
    (straight-use-package pkg))
  (prog1 'keys
    (general-def :keymaps 'minibuffer-local-map
      "<backtab>" #'marginalia-cycle)
    (k/general-leader
     "SPC" #'embark-act)
    (general-def :keymaps 'selectrum-minibuffer-map
      "M-i" #'selectrum-insert-current-candidate)
    (general-def :keymaps '(selectrum-minibuffer-map
                            vertico-map
                            minibuffer-local-map)
      "TAB" #'embark-act)
    (general-def
      :states 'normal
      "go" #'consult-outline)
    (k/general-leader
     "/" #'helm-occur)))

(defun k/setup-company ()
  "Set up Company auto-completion."
  (straight-use-package 'company)
  (straight-use-package 'company-prescient)
  (global-company-mode)
  (company-tng-configure-default)
  (setq company-tooltip-limit 14
        company-tooltip-align-annotations t
        company-require-match 'never
        company-global-modes '(not erc-mode message-mode help-mode gud-mode eshell-mode)
        company-show-numbers t
        company-idle-delay nil
        ;; company-auto-complete t
        ;; company-auto-complete-chars '(?\) ?.)
        ;; otherwise this causes lag with many open buffers
        company-dabbrev-other-buffers nil
        ;; case sensitive company-dabbrev
        company-dabbrev-downcase nil
        company-dabbrev-ignore-case nil)
  (leaf company-prescient
    :straight t
    :init (company-prescient-mode))
  (prog1 'keys
    (general-define-key
     :keymaps '(text-mode-map markdown-mode-map pollen-mode-map evil-org-mode-map)
     :states 'insert
     "<tab>" 'complete-symbol)
    ;; sometimes I want to turn Company off, since it seems to interfere
    ;; with Fcitx.
    (k/general-leader/toggle
      "c" #'company-mode)
    (general-def
      :keymaps 'company-active-map
      "M-/" #'company-other-backend
      "C-n" #'company-select-next-or-abort
      "C-p" #'company-select-previous-or-abort
      "<tab>" #'company-complete-common-or-cycle))
  ;; Integration
  (with-eval-after-load 'evil
    (add-hook 'company-mode-hook #'evil-normalize-keymaps))
  (with-eval-after-load 'counsel
    (advice-add 'completion-at-point :around
                (defun k/advice-cap (func)
                  "Run `company-complete' instead if company-mode is on."
                  (if company-mode
                      (company-complete)
                    (funcall func))))))

(defun k/setup-projectile ()
  "Set up projectile."
  (leaf counsel-projectile
    :straight t
    :after projectile
    :config
    (setq counsel-ag-base-command "ag --hidden --vimgrep %s"))
  ;; consult-ripgrep fails to search properly on my system for some
  ;; reason.
  (leaf projectile
    :straight t
    :config
    (projectile-mode)
    (with-eval-after-load 'projectile
      (push "project.edn" projectile-project-root-files)
      (setq projectile-project-search-path
            (cond
             (k/cloud/
              `("~/git" ,(f-join k/cloud/ "Projects")))
             ((equal (k/system-name) "Nexus 7")
              `("~/git"))
             (t
              `(,(file-truename "/sdcard/0.git")))))))
  (k/general-leader
   "p" '(projectile-command-map :wk "Projectile" :package projectile))
  (k/general-leader
   "pss" #'counsel-projectile-ag)
  (k/general-leader/apps
    "G" #'projectile-ag))

(defun k/setup-ag ()
  "Set up `ag' and writable ag (`wgrep-ag')."
  (straight-use-package 'ag)
  (straight-use-package 'wgrep-ag)
  ;; These are already autoloaded in wgrep-ag.el.
  ;; (autoload 'wgrep-ag-setup "wgrep-ag")
  ;; (add-hook 'ag-mode-hook 'wgrep-ag-setup)
  (general-def
    :keymaps 'ag-mode-map
    :states 'normal
    "i" #'wgrep-change-to-wgrep-mode)
  (k/general-leader/apps
    "g" #'ag))

(defun k/setup-flycheck ()
  "Set up Flycheck."
  (straight-use-package 'flycheck)
  (setq flycheck-emacs-lisp-load-path 'inherit
        flycheck-textlint-config "~/.config/textlint/textlintrc.json")
  (global-flycheck-mode)
  (leaf flycheck-inline
    :disabled t
    :straight t
    :hook flycheck-mode-hook))

(defun k/setup-undo-tree ()
  "Set up Undo Tree."
  (straight-use-package 'undo-tree)
  (setq
   ;; It is ridiculous how Emacs has 3 size-based undo limits.
   ;; Feels like a desparate hack back when Emacs still OOMs people's
   ;; entire systems.
   undo-limit (* 128 1024 1024) ; normal limit
   undo-strong-limit (* 256 1024 1024) ; larger limit
   undo-outer-limit (* 2048 1024 1024)) ; "must not exceed" limit
  (global-undo-tree-mode)
  (k/general-leader
   "u" '(undo-tree-visualize :wk "Undo Tree")))

(defun k/setup-evil ()
  "Set up Evil."
  (k/setup-undo-tree)
  (straight-use-package 'evil)
  (straight-use-package 'evil-collection)
  (setq evil-want-keybinding nil
        evil-collection-setup-minibuffer t
        evil-collection-want-find-usages-bindings nil
        evil-want-integration t
        evil-want-C-g-bindings t
        evil-want-C-w-in-emacs-state t
        ;; FIXME: TAB -> z a binding overwrites this
        evil-want-C-i-jump t
        evil-move-beyond-eol t
        evil-insert-skip-empty-lines t
        evil-search-module 'evil-search
        evil-ex-search-persistent-highlight nil
        evil-want-Y-yank-to-eol t
        evil-jumps-cross-buffers nil
        ;; should Evil collect one insertion into one undo?
        ;; I want to try without.
        evil-want-fine-undo t
        evil-cross-lines t)
  (with-eval-after-load 'evil
    (evil-set-initial-state 'comint-mode 'normal))
  ;; (leaf evil/require
  ;;   :config
  ;;   (require 'evil)
  ;;   (require 'evil-collection))
  (general-setq
   ;; we need to run the custom setter with `general-setq'
   ;; see `evil-set-undo-system'
   evil-undo-system 'undo-tree)
  (leaf evil/init
    :config
    (profiler-start 'cpu)
    (evil-mode)
    ;; use my own keybinds for Company
    (evil-collection-init (remq 'company evil-collection-mode-list))
    ;; Weird autoload issues (`evil-collection-minibuffer-setup' not
    ;; being defined even though it should be autoloaded) come up
    ;; without this
    (evil-collection-require 'minibuffer)
    (evil-collection-minibuffer-setup)
    (profiler-report))
  (prog1 'keys
    ;; I keep losing window configurations because I expected
    ;; "window-quit" to never mean "quit the application".
    (general-def
      :keymaps 'evil-window-map
      "q" #'delete-window)
    (k/general-leader
     "w" '(:keymap evil-window-map :which-key "window" :package evil)
     "wO" #'other-window
     "c" (general-simulate-key "/ C-g"
           :docstring "Simulate '/ C-g' to disengage evil-search."))
    (general-def
      :states 'normal
      "Q" (general-simulate-key "@@")
      ;; This is intended to be a "delete into the blackhole register"
      ;; binding, but it doesn't work for some reason.
      ;;
      ;; Just cut as usual, letting it write into the kill ring, paste,
      ;; then use C-p (evil-paste-pop) to swap the pasted content with
      ;; the previous entry in the kill ring. (C-n goes to the next
      ;; entry.)
      ;;
      ;; "s" (general-simulate-key "\"_d")
      "gu" #'k/change-case
      "gr" #'revert-buffer
      "gi" #'imenu
      ;; Because I want TAB to toggle folding
      "M-i" #'evil-jump-backward
      "TAB" (general-simulate-key "za"))
    (general-def
      :states 'insert
      ;; Emacs style eol / bol commands
      "C-e" #'k/end-of-line ; also makes sure we don't land at bol
      "C-a" #'move-beginning-of-line
      ;; better way to insert parens; idea originated from
      ;; http://xahlee.info/kbd/best_way_to_insert_brackets.html
      "C-j" "("
      "C-k" "["
      "C-l" "{")
    (general-def
      :states 'visual
      "TAB" #'indent-for-tab-command))
  (evil-define-operator k/change-case (beg end type case)
    "Change case of text."
    (let ((case (or case
                    (-some-->
                        (completing-read "Change to case: "
                                         '("UPPER CASE"
                                           "lower case"
                                           "Title Case"))
                      (s-split " " it)
                      car
                      downcase
                      intern))))
      (if (eq type 'block)
          (evil-apply-on-block #'k/change-case beg end nil case)
        (pcase case
          (`upper (upcase-region beg end))
          (`lower (downcase-region beg end))
          (`title (capitalize-region beg end))))))
  (define-advice evil-insert-newline-above (:around (_func))
    "For performance, don't narrow to field."
    (evil-move-beginning-of-line)
    (insert (if use-hard-newlines hard-newline "\n"))
    (forward-line -1)
    (back-to-indentation))
  (define-advice evil-insert-newline-below (:around (_func))
    "For performance, don't narrow to field."
    (evil-move-end-of-line)
    (insert (if use-hard-newlines hard-newline "\n"))
    (back-to-indentation))
  ;; Fixes
  (defun evil-switch-to-normal-if-state-nil ()
    "Switch to normal state if `evil-state' is nil."
    (when (and evil-mode (not evil-state))
      (evil-normal-state)))
  (general-add-hook '(pre-command-hook
                      dired-mode-hook)
    #'evil-switch-to-normal-if-state-nil)
  (add-hook 'special-mode-hook #'evil-local-mode))

(defun k/setup-evil-extensions ()
  "Set up Evil extension packages."
  (leaf evil-goggles
    :straight t
    :config
    (setq evil-goggles-async-duration 0.5
          evil-goggles-blocking-duration 0.1)
    (evil-goggles-mode)
    (evil-goggles-use-diff-faces))
  (leaf evil-terminal-cursor-changer
    :straight (evil-terminal-cursor-changer
               :host github
               :repo "kisaragi-hiu/evil-terminal-cursor-changer")
    :when k/android?
    :config (evil-terminal-cursor-changer-activate))
  (leaf evil-commentary
    :straight t
    :config (evil-commentary-mode))
  (leaf evil-surround
    :straight t
    :config (global-evil-surround-mode))
  (leaf evil-easymotion
    :straight t
    :config
    (general-def :states 'motion
      "s" '(:keymap evilem-map :package evil-easymotion :wk "Easymotion")
      "f" #'evilem-motion-find-char
      "F" #'evilem-motion-find-char-backward))
  (leaf evil-numbers
    :straight t
    :config
    (general-define-key
     :states '(normal visual motion)
     "<kp-add>" #'evil-numbers/inc-at-pt
     "<kp-subtract>" #'evil-numbers/dec-at-pt))
  ;; ax, ix to select an XML / HTML attribute
  (leaf exato
    :straight t
    :require t)
  (leaf evil-textobj-line
    ;; il, al to select current line
    :straight (evil-textobj-line :type git :host github
                                 :repo "syohex/evil-textobj-line")
    :require t))

;; Loading Evil here takes less than a second, but if we load
;; it after the block below (after ivy, flycheck, etc.) it easily
;; takes 20 seconds. Somehow.
(leaf evil
  :config
  (k/setup-evil)
  (k/setup-evil-extensions))

(k/setup-modeline)
(k/setup-appearance)

(k/setup-compdef)
(k/setup-transient)
(k/setup-keys)

(k/setup-company)
(k/setup-projectile)
(k/setup-ag)
(k/setup-flycheck)

;;;;;; Features / Minor modes

(leaf dogears
  :straight (dogears :type git :host github
                     :repo "alphapapa/dogears.el")
  :unless k/android?
  :init
  (general-def
    "<f6>" (lambda ()
             (interactive)
             (call-interactively #'dogears-remember)
             (message "Saved..."))
    "<f9>" #'dogears-go
    "S-<f9>" #'dogears-list))

(leaf flyspell
  :unless k/android?
  :when (executable-find "hunspell")
  :init
  (leaf ispell
    :init
    ;; ispell.el relies on Hunspell to load a default and report it,
    ;; but Hunspell just errors out if it can't find a dictionary for
    ;; the system locale. And because ispell.el is trying to get a
    ;; default dictionary, it doesn't pass `ispell-dictionary' onto
    ;; Hunspell.
    ;;
    ;; The only way around this is to set the DICTIONARY environment
    ;; variable. It is supposed to do the same thing as the `-d'
    ;; option for Hunspell.
    ;;
    ;; It's ispell.el that needs to be fixed here: it should know that
    ;; `ispell-dictionary' is already the default and pass `-d' onto
    ;; Hunspell.
    (setenv "DICTIONARY" "en_US")
    (setq ispell-program-name (executable-find "hunspell")
          ispell-dictionary "en_US")
    :config
    (define-advice ispell-parse-output (:around (orig-func &rest args)
                                                dont-report-words-without-replacements)
      "Don't report words without replacements as misspelt."
      (let ((orig-return (apply orig-func args)))
        ;; specifically for case 3: ("ORIGINAL-WORD" OFFSET MISS-LIST GUESS-LIST)
        ;; Replace return value with t if MISS-LIST and GUESS-LIST are
        ;; both empty.
        (if (and (listp orig-return)
                 (null (elt orig-return 2))
                 (null (elt orig-return 3)))
            t
          orig-return)))
    (byte-compile #'ispell-parse-output@dont-report-words-without-replacements))
  (general-add-hook '(org-mode-hook
                      markdown-mode-hook
                      text-mode-hook
                      message-mode-hook)
    #'flyspell-mode)
  ;; ledger-mode inherits hooks from text-mode, but we want flyspell
  ;; off there.
  (general-add-hook '(ledger-mode-hook)
    (k/turn-off-minor-mode flyspell-mode))
  (leaf flyspell-correct
    :straight t)
  (leaf flyspell-correct-helm
    :straight t
    :after flyspell-correct
    :require t)
  (general-def
    :keymaps 'flyspell-mode-map
    "<f12>" #'flyspell-correct-wrapper
    ;; "M-RET" #'flyspell-correct-wrapper
    "<f10>" #'k/add-word-to-hunspell-personal-dictionary)
  (defun k/hunspell-personal-dictionary ()
    "Return the path to the current Hunspell personal dictionary, if it exists."
    (let ((dict (f-join "~/" (format ".hunspell_%s"
                                     ispell-current-dictionary))))
      (when (and (f-readable? dict)
                 (f-exists? dict))
        dict)))
  ;; Note that I've realized this isn't actually necessary. Saving new
  ;; words into the personal dictinary can already be done with the
  ;; "Save <word>" option in flyspell-correct-wrapper.
  ;;
  ;; I might tear this down if I find that more convenient.
  (defun k/add-word-to-hunspell-personal-dictionary (word)
    "Add WORD to the current Hunspell personal dictionary."
    (interactive (let ((forward (equal current-prefix-arg '(4))))
                   (list (car (flyspell-get-word forward)))))
    (let* ((dict (k/hunspell-personal-dictionary))
           (undo? (eq last-command 'k/add-word-to-hunspell-personal-dictionary)))
      (unless (not dict)
        (cond (undo?
               (k/with-file dict t
                 (goto-char (point-max))
                 (skip-chars-backward "\n\r")
                 (delete-region (line-beginning-position) (point-max))))
              (t
               ;; skip if word is already in there
               (k/with-file dict nil
                 (re-search-forward (format "^%s$" word) nil t))
               (k/with-file dict t
                 (goto-char (point-max))
                 (skip-chars-backward "\n\r")
                 (insert "\n" word))
               (message
                (substitute-command-keys
                 "Added \"%s\" to personal dictionary. Undo (\\[k/add-word-to-hunspell-personal-dictionary])?")
                word)))
        (when (process-live-p ispell-process)
          (kill-process ispell-process)
          (ispell-init-process))))))

(leaf writeroom-mode
  :straight t
  :custom
  ((writeroom-fullscreen-effect . 'maximized)
   (writeroom-header-line . ""))
  :config
  (k/general-leader/toggle
    "w" #'writeroom-mode))

(leaf fzf
  ;; note that I'm not using fzf.el
  :config
  (when (executable-find "fd")
    ;; use `fd' because it understands .gitignore
    ;; use -H to still show hidden directories
    (setenv "FZF_DEFAULT_COMMAND" "fd -H"))
  (k/general-leader/file
    "F" #'counsel-fzf))

(leaf unpackaged
  :require unpackaged-subset
  ;; Functions copied from https://github.com/alphapapa/unpackaged.el
  ;; because I don't want to load the entire file.
  :config
  (general-def
    :keymaps (--map (intern (format "%s-map" it)) k/lisp-modes)
    "C-c ^" #'unpackaged/sort-sexps))

(leaf hl-line
  :config
  (general-add-hook '(canrylog-file-mode-hook
                      org-agenda-mode-hook
                      profiler-report-mode-hook)
    #'hl-line-mode))

(leaf rainbow-delimiters :straight t)

(leaf tabs
  :config
  (cond
   ((version< emacs-version "27")
    (leaf evil-tabs
      :straight t
      :when (version< emacs-version "27")
      :config
      (global-evil-tabs-mode)
      (defun k/elscreen-hide-tabs-if-only-one ()
        "Hide elscreen tabs if there's only one."
        (defvar elscreen-display-tab)
        (if (elscreen-one-screen-p)
            (setq elscreen-display-tab nil)
          (setq elscreen-display-tab t))
        (elscreen-tab-update))
      (defun k/elscreen-goto (screen)
        "Switch to screen SCREEN.

When SCREEN is an existing screen id, switch to it. If it's not,
create a new screen (by cloning the current) and rename it to
SCREEN. This mimicks the behavior of `ivy-switch-buffer'."
        (interactive
         `(,(--> (completing-read "Switch to screen: "
                                  (mapcar
                                   (pcase-lambda (`(,id . ,name))
                                     (format "[%s] %s" id name))
                                   (elscreen-get-screen-to-name-alist)))
              (or (ignore-errors
                    (string-to-number
                     (cadr (s-match "\\[\\(.*\\)\\].*" it))))
                  it))))
        (if (member screen (elscreen-get-screen-list))
            (elscreen-goto screen)
          (elscreen-clone)
          (elscreen-screen-nickname screen)))
      (add-hook 'elscreen-screen-update-hook #'k/elscreen-hide-tabs-if-only-one)
      (k/general-leader/tabs
        "c" '(elscreen-clone :wk "Clone tab")
        "d" '(elscreen-kill :wk "Close tab")
        "t" '(k/elscreen-goto :wk "Go to tab…")
        "o" '(elscreen-kill-others :wk "Delete other tabs")
        "n" '(elscreen-create :wk "New tab")
        "r" '(elscreen-screen-nickname :wk "Rename this tab"))))
   (t
    (leaf tab-bar
      :config
      ;; only show tab bar when there's more than one tab
      ;; show the tab bar and enable C-TAB / C-S-TAB tab switching
      (tab-bar-mode)
      ;; (The mode is enabled in the custom setter, so use
      ;; `general-setq' here)
      (general-setq
       tab-bar-show 1)
      ;; The binding... doesn't come back?
      (general-def
        "C-<tab>" #'tab-next
        "C-S-<tab>" #'tab-previous)
      (k/general-leader/tabs
        "c" (and (fboundp 'tab-duplicate)
                 '(tab-duplicate :wk "Clone tab"))
        "d" '(tab-close :wk "Close tab")
        "t" '(tab-bar-switch-to-tab :wk "Go to tab…")
        "o" '(tab-close-other :wk "Delete other tabs")
        "n" '(tab-new :wk "New tab")
        "r" '(tab-rename :wk "Rename this tab"))))))

(leaf multiple-cursors
  :straight t
  :config
  (general-def
    :prefix "gp"
    :states 'motion
    "h" 'mc/edit-lines
    "j" 'mc/mark-next-like-this
    "k" 'mc/mark-previous-like-this
    "m" 'mc/mark-all-like-this
    "n" 'mc/mark-next-like-this)
  (k/general-leader
   "j" 'mc/mark-next-like-this
   "k" 'mc/mark-previous-like-this)
  (general-def
    "C-S-<mouse-1>" 'mc/add-cursor-on-click))

(leaf highlight-numbers :straight t)

(leaf kisaragi-timestamp-highlight
  :commands kisaragi-timestamp-highlight-mode
  :init
  (add-hook 'org-mode-hook #'kisaragi-timestamp-highlight-mode))

(leaf format-all-mode
  :straight format-all
  :config
  (add-to-list 'display-buffer-alist
               '("\\*format-all-errors\\*" display-buffer-no-window)))

(leaf which-key
  :straight t
  :setq ((which-key-idle-delay . 0.4)
         (which-key-idle-secondary-delay . 0.1)
         (which-key-sort-order . #'which-key-key-order-alpha)
         (which-key-enable-extended-define-key . t))
  :config
  (autoload #'which-key-mode "which-key-replacements")
  (general-def
    "C-h w" '(which-key-show-top-level
              :wk "Show top level keybinds")
    "C-h W" '(which-key-show-full-major-mode
              :wk "Show major mode keybinds"))
  (which-key-mode))

(leaf editorconfig
  :straight t
  :config (editorconfig-mode))

(leaf didyoumean
  :straight (didyoumean :type git :host gitlab :repo "kisaragi-hiu/didyoumean.el")
  :config (didyoumean-mode))

(leaf lsp-mode
  :straight t
  :unless k/android?
  :config
  (setq lsp-prefer-flymake nil
        lsp-ui-flycheck-enable t
        lsp-auto-guess-root t
        ;; "[...] Again the emacs default 4k [sic] is too low
        ;; considering that the some of the language server responses
        ;; are in 800k - 3M range."
        read-process-output-max (* 1024 1024)
        lsp-keymap-prefix "C-c l"
        lsp-idle-delay 0.5
        lsp-headerline-arrow
        (propertize "›" 'face 'lsp-headerline-breadcrumb-separator-face))
  (general-add-hook 'lsp-mode-hook
    #'lsp-enable-which-key-integration)
  (setq lsp-clients-typescript-javascript-server-args
        (list "--tsserver-log-file"
              (expand-file-name "ts-log" user-emacs-directory)
              "--tsserver-log-verbosity" "off"))
  (leaf dap-mode
    :straight t)
  (leaf lsp-ui
    :straight t
    :commands lsp-ui-mode)
  (leaf lsp-ivy
    :straight t
    :commands lsp-ivy-workspace-symbol))

(leaf goto-addr
  :hook ((text-mode-hook . goto-address-mode)
         (eshell-mode-hook . goto-address-mode))
  :config
  (general-add-hook '(markdown-mode-hook
                      org-mode-hook)
    (k/turn-off-minor-mode goto-address-mode)))

(leaf link-hint
  :straight t
  :config
  (k/general-leader
   "o" '(link-hint-open-link :wk "Open a link")
   "O" '(link-hint-copy-link :wk "Copy a link")))

(leaf page-break-lines
  :straight t
  :config
  ;; (add-to-list 'page-break-lines-modes 'lisp-mode)
  (global-page-break-lines-mode))

;; Miscellaneous Emacs settings that have nothing to do with sanity
(leaf emacs
  :config
  (setq initial-buffer-choice (f-join k/notes/ "readme.org")
        find-file-visit-truename t
        display-time-format "%H:%M %Z"
        ;; This sets the default major mode for new buffers.
        major-mode 'text-mode
        scroll-conservatively 150
        scroll-margin 15
        scroll-step 0))

(leaf woman
  :commands woman
  :config
  (when-let ((usr (getenv "PREFIX")))
    (setq woman-manpath
          (--map (s-replace "/usr" usr it)
                 woman-manpath))))

(leaf menu-bar
  :when (fboundp #'menu-bar-mode)
  :config
  (k/general-leader/toggle
    "m" '(menu-bar-mode :wk "Menu bar")))

(leaf sanity
  ;; name inspired by https://github.com/rougier/elegant-emacs
  :config
  (leaf fix-ffap-on-iso8601
    :after ffap tramp ; this code should run after ffap and tramp
    :require t)
  (when (eq system-type 'windows-nt)
    (set-locale-environment "ja_JP.UTF-8"))
  (setq-default
   create-lockfiles nil
   ;; This also applies to "A and B are the same file" when
   ;; `find-file-visit-truename' is non-nil.
   ;;
   ;; Alternatively, you can redefine `files--message' (see
   ;; files.el::L2222), because the original isn't actually silent ---
   ;; it just clears the echo area immediately after. The message
   ;; still shows for a split second.
   save-silently t
   ;; Copy elsewhere -> copy in Emacs -> the text copied from
   ;; elsewhere is overwritten without being saved into `kill-ring'.
   ;;
   ;; Setting this to t makes Emacs first save the text copied from
   ;; elsewhere (the "interprogram paste") into `kill-ring' so that
   ;; it's not lost.
   ;;
   ;; WHY IS THIS NOT DEFAULT?
   save-interprogram-paste-before-kill t
   ;; save backups and autosaves to system temp dir
   auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
   backup-directory-alist `((".*" . ,temporary-file-directory))
   ;; make sure Custom never looks at an existing file
   custom-file (make-temp-file "emacs-custom")
   ;; If I decide to load a theme, it serves no purpose to remind me
   ;; that I need to trust it.
   custom-safe-themes t
   delete-by-moving-to-trash t
   ;; If I do know what a command does, why would I need a confirmation?
   ;; If I don't know what a command does, how would I know if I want
   ;; to use it or not?
   disabled-command-function nil
   ;; Never abbreviate.
   ;; If it's too slow I'll cancel it myself.
   ;; Abbreviation doesn't speed up pretty printing anyways.
   eval-expression-print-level nil
   eval-expression-print-length nil
   print-level nil
   print-length nil
   frame-title-format "%b - GNU Emacs" ; buffer name
   frame-resize-pixelwise t
   inhibit-compacting-font-caches t
   inhibit-startup-screen t
   inhibit-x-resources t
   initial-scratch-message nil
   large-file-warning-threshold (* 100 1024 1024)
   ;; Don't do automatic line breaking to code.
   comment-auto-fill-only-comments t
   ;; This forces dates to be between 1970-01-01 and 2038-01-01. I
   ;; don't know how useful this would be right now (2020-12-05) with
   ;; 2038 being 17 years away, but I don't want to be bitten by this.
   ;;
   ;; The docstring says "Currently this setting is not recommended
   ;; because the likelihood that you will open your Org files in an
   ;; Emacs that has limited date range is not negligible." As this
   ;; option was added in Emacs 24.1 (and my config requires Emacs 27
   ;; anyways) I'm pretty sure it is negligible for me.
   org-read-date-force-compatible-dates nil
   mouse-drag-and-drop-region t
   ;; mouse wheel acceleration? hell no
   mouse-wheel-progressive-speed nil
   require-final-newline t
   ;; why ask me when you know I haven't modified the buffer?
   revert-without-query (list ".*")
   ;; This being nil means to use the "default", which on Termux means
   ;; vibrate. Set to `ignore' to ensure it never does that. (The
   ;; meaning of its value can be seen in its `custom-type' property.)
   ring-bell-function #'ignore
   ;; don't force double space on me.
   sentence-end-double-space nil
   tab-always-indent 'complete
   ;; Never use the "graphical" dialog box.
   use-dialog-box nil
   vc-follow-symlinks t
   warning-minimum-level :emergency
   widget-image-enable nil) ; widget images are ugly
  :config
  (delete-selection-mode)
  (column-number-mode)
  (savehist-mode)
  ;; <rant>
  ;; I don't need a keybinding just to see NEWS...
  ;; Or just to visit the GNU website...
  ;; That should be a default bookmark, not a default keybind...
  ;; </rant>
  (general-unbind :keymaps '(ehelp-map help-map)
    "C-n" "n" "g")
  ;; This literally runs the same command as TAB by default. Give
  ;; `tab-bar-mode' this binding.
  (general-unbind
    :keymaps 'magit-section-mode-map
    "C-<tab>")
  ;; Heading demoting and promoting are already provided by `evil-org'
  ;; through << and >> commands.
  (general-unbind
    :keymaps 'org-mode-map
    "M-h" "M-l" "M-<left>" "M-<right>")
  (general-unbind
    :keymaps 'org-mode-map
    :states 'normal
    "M-h" "M-l" "M-<left>" "M-<right>")
  (general-unbind
    :keymaps 'outline-mode-map
    :states 'normal
    "M-j" "M-k")
  ;; outline-mode defines ^ to jump to the closest header. overwrite that.
  (general-def
    :keymaps 'outline-mode-map
    :states 'normal
    "^" 'evil-first-non-blank)
  (general-unbind
    ;; suspend-frame
    "C-x C-z"
    ;; disable facemenu
    "M-o")
  (defalias 'yes-or-no-p 'y-or-n-p)
  (add-to-list 'display-buffer-alist
               '("\\*Async Shell Command\\*.*" display-buffer-no-window))
  (with-eval-after-load 'org-agenda
    (defun k/org-agenda-todo-not-broken ()
      "Running `org-agenda-todo' sometimes don't actually advance an item with a repeater.

I DON'T KNOW WHY."
      (interactive)
      (org-agenda-show)
      (other-window 1)
      (org-todo)
      (other-window 1))
    (define-key org-agenda-mode-map
      [remap org-agenda-todo] #'k/org-agenda-todo-not-broken)))

(leaf image-mode
  :config
  (define-advice image-mode (:around (func))
    "Open image externally if the display don't support images."
    (if (display-images-p)
        (call-interactively func)
      (counsel-find-file-extern (buffer-file-name))))
  (general-def
    :keymaps 'image-mode-map
    :states 'motion
    "+" #'image-increase-size
    "-" #'image-decrease-size))

(leaf so-long
  ;; New minor mode in Emacs 27 to work around long lines.
  ;; Enable it when we upgrade.
  :unless (version< emacs-version "27")
  :config (global-so-long-mode))

(leaf recentf
  :config
  ;; or `nil' for unlimited
  (setq recentf-max-saved-items 10000)
  (recentf-mode))

(leaf display-line-numbers
  :setq-default (display-line-numbers-width . 4)
  :config
  (defun k/change-line-number-style ()
    "Change line number style."
    (interactive)
    ;; (setq display-line-numbers
    ;;       (k/next '[t relative visual nil] display-line-numbers))
    (setq display-line-numbers
          (alist-get (completing-read "Style: "
                                      '("off" "absolute" "relative" "visual"))
                     '(("off" . nil)
                       ("absolute" . t)
                       ("relative" . relative)
                       ("visual" . visual))
                     nil nil #'equal))
    (message "Line number style: %s" (cl-case display-line-numbers
                                       ((nil) "off")
                                       ((relative visual) display-line-numbers)
                                       ((t) "absolute")
                                       (t "off"))))
  (global-display-line-numbers-mode -1)
  (k/set-mode-local '(conf-mode)
    display-line-numbers 'relative)
  (k/set-mode-local 'text-mode display-line-numbers t)
  (k/set-mode-local
      '(magit-mode
        deft-mode eshell-mode shell-mode
        eww-mode org-mode org-agenda-mode
        doc-view-mode)
    display-line-numbers nil)
  (k/general-leader/toggle
    "n" '(k/change-line-number-style
          :wk "Change line number style")))

(leaf paren
  :setq
  ((show-paren-delay . 0)
   (show-paren-when-point-inside-paren . t)
   (blink-matching-paren . :show))
  :config (show-paren-mode))

(leaf cleanup
  :config
  (define-minor-mode k/delete-trailing-whitespace-mode
    "Minor mode to `delete-trailing-whitespace' on save."
    :global nil
    (if k/delete-trailing-whitespace-mode
        (add-hook 'before-save-hook #'delete-trailing-whitespace nil t)
      (remove-hook 'before-save-hook #'delete-trailing-whitespace t)))
  (general-add-hook 'text-mode-hook
    '(k/delete-trailing-whitespace-mode)))

(leaf whitespace
  :config
  (global-whitespace-mode)
  (setq-default whitespace-style '(face tabs newline space-mark tab-mark newline-mark)
                whitespace-display-mappings '(((newline-mark 10 [?¬ 10])
                                               (tab-mark 9 [?▷ 9] [?\\ 9])))
                show-trailing-whitespace nil
                indent-tabs-mode nil)
  ;; don't touch whitespace if I'm still in insert state
  (advice-add 'delete-trailing-whitespace :around
              (lambda (func &rest args)
                (unless (eq evil-state 'insert)
                  (apply func args))))
  (define-minor-mode k/show-trailing-whitespace-mode
    "`show-trailing-whitespace' as a minor mode."
    :global nil
    (setq show-trailing-whitespace
          (not show-trailing-whitespace)))
  (general-add-hook '(org-mode-hook
                      magit-mode-hook
                      ledger-mode-hook)
    #'k/show-trailing-whitespace-mode))

(leaf literate-calc-mode
  ;; calculate and overlay arrows like this in a buffer:
  ;; a = 3 => a: 3
  ;; a + 5 => 8
  ;; like CIDER, but for math (`calc') statements.
  :straight (literate-calc-mode :host github :repo "sulami/literate-calc-mode.el"))

;;;;;; Commands

(leaf dumb-jump
  :straight t
  :setq ((dumb-jump-selector . 'ivy))
  :config
  (add-hook 'xref-backend-functions
            #'dumb-jump-xref-activate))

(leaf unfill
  :straight t
  :init
  (general-def
    ;; unfill-paragraph, unfill-region
    ;; Maybe bind M-S-q instead of toggling
    "M-q" #'unfill-toggle))

(leaf eval-in-repl
  :straight t
  :require t
  :config
  (setq eir-repl-placement 'right)
  (leaf eval-in-repl-ielm :require t :after elisp-mode
    :config
    (setq eir-ielm-eval-in-current-buffer t)
    (general-def :keymaps '(emacs-lisp-mode-map
                            lisp-interaction-mode-map
                            Info-mode-map)
      "<C-return>" #'eir-eval-in-ielm))
  (leaf eval-in-repl-cider :require t :after clojure-mode
    :config
    (general-def :keymaps '(clojure-mode-map)
      "<C-return>" #'eir-eval-in-cider))

  (leaf eval-in-repl-slime :require t :after slime
    :config
    (general-def :keymaps 'lisp-mode-map
      "<C-return>" #'eir-eval-in-slime))
  (leaf eval-in-repl-racket :require t :after racket-mode
    :config
    (general-def :keymaps 'racket-mode-map
      "<C-return>" #'eir-eval-in-racket))
  (leaf eval-in-repl-python :require t :after python
    :config
    (general-def :keymaps 'python-mode-map
      "<C-return>" #'eir-eval-in-python))
  (leaf eval-in-repl-javascript :require t :after js2-mode js-comint
    :config
    (general-def :keymaps 'js2-mode-map
      "<C-return>" #'eir-eval-in-javascript))
  ;; Shell support
  (leaf eval-in-repl-shell :require t :after sh-mode
    :config
    (general-def :keymaps 'sh-mode-map
      "<C-return>" #'eir-eval-in-shell)))

(leaf org-babel-eval-in-repl
  :straight t
  :require t
  :after ob
  :config
  (general-def :keymaps 'org-mode-map
    "C-<return>" #'ober-eval-in-repl
    "M-<return>" #'ober-eval-block-in-repl))

;;;;;; Misc.

(leaf yasnippet
  :straight yasnippet yasnippet-snippets
  :config
  ;; "3 is too chatty about initializing yasnippet. 2 is just right
  ;; (only shows errors)." --- doom-emacs
  (setq yas-verbosity 2)
  (yas-global-mode)
  ;; (general-unbind :keymaps 'yas-minor-mode-map
  ;;   "TAB" "<tab>")
  (cl-delete 'yas-dropdown-prompt yas-prompt-functions)
  (with-eval-after-load 'smartparens
    ;; tell smartparens overlays not to interfere with yasnippet keybinds
    (advice-add #'yas-expand :before #'sp-remove-active-pair-overlay))
  (k/general-leader/yasnippet
    "n" #'yas-new-snippet
    "i" #'yas-insert-snippet
    "v" #'yas-visit-snippet-file))

(leaf default-text-scale
  :straight t
  :unless k/android?
  :config
  (general-def
    :states 'motion
    "+" #'default-text-scale-increase
    "-" #'default-text-scale-decrease
    "M-0" #'default-text-scale-reset))

(leaf typo
  :straight t
  :config
  (general-add-hook '(markdown-mode-hook
                      org-mode-hook)
    #'typo-mode)
  (general-unbind
    :keymaps 'typo-mode-map
    "<" ">" "'"))

;; center text
(leaf olivetti
  :straight t
  :config
  (k/general-leader/toggle
    "o" '(olivetti-mode :wk "Distraction-free writing")))

(leaf lacarte
  :straight t
  :config
  ;; Bind to SPC x because it might be useful in new programming modes
  (k/general-leader
   "x" #'lacarte-execute-menu-command))

(leaf Info
  :config
  (add-hook 'Info-mode-hook #'variable-pitch-mode)
  (general-define-key
   :keymaps 'Info-mode-map
   "H" #'Info-history-back
   "C-S-h" #'Info-history
   "L" #'Info-history-forward
   "l" nil)
  (setq-mode-local Info-mode display-line-numbers nil))

(leaf shell
  :config
  ;; Set SHELL to bash
  (when-let ((bash (executable-find "bash")))
    (setenv "SHELL" bash)
    (setq-default explicit-shell-file-name bash
                  shell-file-name bash)))

(leaf yasearch
  :straight (yasearch :type git :repo "https://git.sr.ht/~kisaragi_hiu/yasearch")
  :after evil
  :init
  (k/general-g*
    "b" #'yasearch-operator)
  (k/general-leader
   "s" #'yasearch)
  :config
  (setq
   yasearch-engine-alist
   '((dictgoo . "https://dictionary.goo.ne.jp/srch/all/%s/m0u/")
     (dictwebliocjjc
      . "https://cjjc.weblio.jp/content_find?searchType=exact&query=%s")
     (wikipedia-en . "https://en.wikipedia.org/w/index.php?search=%s")
     (wikipedia-zh . "https://zh.wikipedia.org/w/index.php?search=%s")
     (wikipedia-ja . "https://ja.wikipedia.org/w/index.php?search=%s")
     (kotobank
      . "https://kotobank.jp/word/%s")
     (dictweblio
      . "https://www.weblio.jp/content_find?query=%s&searchType=exact")
     (google . "https://google.com/search?q=%s")
     (duckduckgo . "https://duckduckgo.com/?q=%s")
     (wiktionary-zh . "https://zh.wiktionary.org/w/index.php?search=%s")
     (wiktionary-en . "https://en.wiktionary.org/w/index.php?search=%s")
     (wiktionary-ja . "https://ja.wiktionary.org/w/index.php?search=%s")
     (jsl-nhk . "https://www2.nhk.or.jp/signlanguage/index.cgi?md=search&tp=page&qw=%s"))
   yasearch-browser-function #'eww-browse-url))

(leaf cangjie :straight t)

(leaf helpful
  :straight t
  :setq (helpful-switch-buffer-function . #'k/pop-to-buffer)
  :config
  (let ((dir "~/git/emacs/"))
    (when (f-exists? dir)
      (setq find-function-C-source-directory
            (f-join dir "src"))))
  (setq helpful-max-highlight 50000
        helpful-max-buffers nil
        counsel-describe-function-function
        #'helpful-callable
        counsel-describe-variable-function
        #'helpful-variable)
  (general-def
    "C-h k" #'helpful-key
    "C-h f" #'helpful-callable
    "C-h v" #'helpful-variable))

(leaf smartparens
  :straight t
  :require t smartparens-config
  :config
  (sp-pair "「" "」")
  (smartparens-global-mode)
  (general-def :states 'visual :keymaps 'smartparens-mode-map
    "(" #'sp-wrap-round
    ")" #'sp-wrap-round
    "[" #'sp-wrap-square
    "]" #'sp-wrap-square
    "{" #'sp-wrap-curly
    "}" #'sp-wrap-curly
    "\"" (k/expose (-partial #'sp-wrap-with-pair "\""))))

(leaf parinfer
  :straight (parinfer :host github
                      :repo "kisaragi-hiu/parinfer-mode"
                      :type git)
  :require t
  :config
  (general-add-hook (--map (k/symbol-append it "-hook")
                           k/lisp-modes)
    #'parinfer-mode)
  (general-def
    :keymaps 'parinfer-mode-map
    "M-p" #'parinfer-toggle-mode
    ;; parinfer-double-quote does not insert a pair of quotes as documented.
    "\"" nil)
  (k/general-mode-leader
   :keymaps 'parinfer-mode-map
   "p" #'parinfer-toggle-mode)
  (leaf rainbow-parinfer
    :require t
    :commands rainbow-parinfer-mode
    :config (rainbow-parinfer-mode))
  (setq parinfer-extensions '(defaults evil smart-yank)
        parinfer-auto-switch-indent-mode-when-closing t)
  (with-eval-after-load 'smartparens
    (parinfer-strategy-add 'default 'sp-insert-pair)))

(leaf atomic-chrome
  :when
  (and (not k/android?)
       (or (executable-find "google-chrome")
           (executable-find "google-chrome-unstable")
           (executable-find "google-chrome-stable")
           (executable-find "chromium")
           (executable-find "firefox")))
  :straight t
  :config (atomic-chrome-start-server))
(leaf buttercup :straight t)
(leaf esup :straight t)

;;;;;; Apps

;; Library to manage a tag library
(leaf kisaragi-tags
  :after org
  :require t
  :init
  (k/general-leader/extra-insert-commands
    "T" '(kisaragi-tags/insert :wk "Insert tag from library"))
  (defun k/org-tag ()
    "Add or remove tags in `org-mode', using tags from `kisaragi-tags'."
    (interactive)
    (let ((counsel-org-tags (org-get-tags)))
      (ivy-read (counsel-org-tag-prompt)
                (kisaragi-tags/library)
                :history 'org-tags-history
                :action #'counsel-org-tag-action
                :caller 'counsel-org-tag)))
  (general-def
    :keymaps 'evil-org-mode-map
    :states 'normal
    "T" 'k/org-tag)
  (k/general-mode-leader
   :keymaps 'org-mode-map
   "T" 'k/org-tag))

(leaf explain-pause-mode
  :straight (explain-pause-mode
             :repo "lastquestion/explain-pause-mode"
             :host github)
  :unless k/android?)

(leaf kisaragi-apps
  :require t
  :config
  (k/general-leader/apps
    "a" #'kisaragi-apps
    "A" '(awk-ward :wk "WIP Awk editing environment")
    ;; just find-file a directory
    ;; "d" #'dired
    "n" #'deft
    "o" #'org-agenda
    "p" #'pacfiles
    "P" #'proced
    "s" #'suggest
    "w" #'eww))

(leaf speed-type
  :straight t
  :unless k/android?
  :init
  (pcase-dolist (`(,str ,func)
                 '(("Typing practice (buffer)" speed-type-buffer)
                   ("Typing practice (builtin text)" speed-type-text)
                   ("Typing practice (top words)" speed-type-top-1000)))
    (kisaragi-apps-register str func)))

(leaf docker
  :straight t
  :when (and (not k/android?)
             (executable-find "docker"))
  :init (kisaragi-apps-register "Docker" #'docker))

(leaf kisaragi-log
  :straight (kisaragi-log :repo "kisaragi-hiu/kisaragi-log"
                          :host gitlab)
  :config
  (setq kisaragi-log-data-file
        (f-join k/notes/ "kisaragi-log.json")
        kisaragi-log-types
        '(("water" ((:amounts . (350 600))
                    (:target . 1500)))
          ("typing speed" ((:amounts . (65 70 80))
                           (:target . 80)
                           (:skip-zero . t)))))
  (k/general-leader/apps
    "h" '(kisaragi-log :wk "Tracker for stuff")))

(leaf proced
  :init (kisaragi-apps-register "Process Editor" #'proced))

(leaf magit
  :straight t
  ;; If there's a magit blob buffer open (like file.x<master~1>), update
  ;; it when moving in the log buffer
  ;; https://www.reddit.com/r/emacs/comments/bcpexy/magit_how_to_quickly_view_the_history_of_a_file/ekt41mg/
  :hook ((magit-section-movement-hook . magit-log-maybe-update-blob-buffer))
  :commands magit-inside-worktree-p
  :init
  (setq magit-commit-ask-to-stage 'stage
        magit-no-confirm '(stage-all-changes unstage-all-changes))
  (k/general-leader/git
    "s" '(magit-status :wk "Status")
    "i" '(magit-init :wk "Init")
    "g" '(magit-file-dispatch :wk "+Magit file commands")
    "G" '(magit-dispatch :wk "+Magit commands")
    "C" '(k/git-commit-everything
          :wk "Commit every change")
    "c" '(k/git-commit
          :wk "Commit with generic message")
    "a" '(k/git-commit-this-file-with-message
          :wk "Commit this (prompt)")
    "A" '(k/git-stage-everything
          :wk "Commit every change")
    "j" '(magit-pull-branch :wk "Pull")
    "k" '(magit-push-other :wk "Push"))
  :config
  (general-define-key
   :keymaps '(magit-log-mode-map)
   :states 'normal
   "C-<return>" #'k/magit-log-visit-changed-file)
  ;; Git's translations are incomplete or incompatible with Magit
  ;; (zh_TW, 2.28.0, rebase commands aren't shown properly in Magit).
  ;; Force it to use English.
  (define-advice magit-start-process (:around (func &rest args))
    "Force Git to use English to avoid incomplete translations."
    (let ((process-environment (cons "LANG=en" process-environment)))
      (apply func args)))
  ;; Unbind `evil-magit-toggle-text-mode' on C-t. It's still provided
  ;; on \, and IMO is more consistent with Evil that way.
  (general-unbind
    :keymaps '(evil-collection-magit-toggle-text-minor-mode-map
               magit-mode-map)
    :states 'normal
    "C-t")
  (leaf git-gutter
    :straight t
    :require t
    :config
    (global-git-gutter-mode)
    (cl-pushnew 'focus-in-hook git-gutter:update-hooks)
    (cl-pushnew #'ivy-switch-buffer git-gutter:update-commands)
    ;; `git-gutter-mode' isn't turned on if a buffer isn't visiting a
    ;; file or if its major mode is in this list.
    (setq git-gutter:disabled-modes '(image-mode))
    (k/general-leader/git
      "h" '(:ignore t :wk "Hunks")
      "hS" '(git-gutter:statistic :wk "Unstaged stats")
      "hx" '(git-gutter:revert-hunk :wk "Revert this")
      "hj" '(git-gutter:next-hunk :wk "Next")
      "hk" '(git-gutter:previous-hunk :wk "Previous")
      "hs" '(git-gutter:stage-hunk :wk "Stage")))
  (leaf magit-todos
    :straight t
    :after org))

(leaf eshell
  :require em-smart em-tramp
  :init (kisaragi-apps-register "Eshell" #'eshell)
  :config
  ;; company's auto completion is broken in Eshell (the way I
  ;; configured it), so just disable it.
  ;; If I'm not happy with Emacs's completion, I can reach for `vterm'
  ;; & zsh, which would also be consistent with shell use outside Emacs.
  (add-hook 'eshell-mode-hook (k/turn-off-minor-mode company-mode))
  (setq eshell-prompt-function
        (lambda ()
          (concat (abbreviate-file-name (eshell/pwd))
                  (if (= (user-uid) 0) "\n# " "\n$ ")))
        ;; only needs to match last line of multi-line prompt
        eshell-prompt-regexp "[#$] "
        eshell-ls-use-colors t
        eshell-history-size 1024
        eshell-hist-ignoredups t
        eshell-destroy-buffer-when-process-dies t
        eshell-where-to-jump 'begin
        eshell-review-quick-commands nil
        eshell-smart-space-goes-to-end t)
  (k/general-leader
   "ae" '(eshell :wk "Eshell")
   "aE" (general-simulate-key (#'universal-argument "SPC ae")
          :which-key "Eshell (new)")))

(leaf taskrunner
  :straight
  (taskrunner
   :host github
   :repo "emacs-taskrunner/emacs-taskrunner")
  (ivy-taskrunner
   :host github
   :repo "emacs-taskrunner/ivy-taskrunner")
  :config
  (k/general-leader
   "l" #'ivy-taskrunner))

(leaf vterm
  :straight t
  :when (and (executable-find "cmake")
             (executable-find "libtool")
             module-file-suffix)
  :init
  (kisaragi-apps-register "Vterm" #'vterm)
  ;; don't ask, just compile
  (setq vterm-always-compile-module t)
  :config
  ;; banish ansi-term :)
  (defalias 'ansi-term (lambda (&rest _) (call-interactively #'vterm)))
  (setq vterm-shell (executable-find "zsh"))
  (defun k/vterm (&optional arg)
    "Create an interactive Vterm buffer.

This implements the same window behavior as `eshell', but for
`vterm':

- Switch to the Vterm session at \"vterm\" (Vterm's
hard-coded buffer name) if it already exists;
- prefix argument determines which session to switch to;
- a new session will be created when needed;
- returns the buffer selected or created."
    (interactive "P")
    ;; Just in case
    (require 'vterm)
    (let* ((vterm-buffer-name "vterm") ; vterm hard-codes it
           (buf (cond ((numberp arg)
                       (get-buffer-create (format "%s<%d>"
                                                  vterm-buffer-name
                                                  arg)))
                      (arg
                       (generate-new-buffer vterm-buffer-name))
                      (t
                       (get-buffer-create vterm-buffer-name)))))
      (cl-assert (and buf (buffer-live-p buf)))
      (pop-to-buffer-same-window buf)
      (unless (derived-mode-p 'vterm-mode)
        (vterm-mode))
      buf))
  (k/general-leader
   "at" '(k/vterm :wk "Vterm")
   "aT" (general-simulate-key (#'universal-argument "SPC at")
          :which-key "Vterm (new)")))

(leaf trashed
  :straight (trashed :host github :repo "shingo256/trashed")
  :init
  (kisaragi-apps-register "View system trash" #'trashed))

(leaf canrylog
  :straight (canrylog :type git :host gitlab :repo "canrylog/canrylog.el")
  :init (kisaragi-apps-register "Canrylog" #'canrylog)
  :require t
  :config
  (with-eval-after-load 'kisaragi-extra-functions
    (add-hook 'canrylog-after-save-hook #'k/git-commit-this-and-push))
  (add-hook
   'canrylog-before-switch-task-hook
   (byte-compile
    (lambda ()
      (let ((default-directory (f-dirname canrylog-file)))
        (shell-command "git pull")))))
  (add-hook 'canrylog-before-save-hook
            (byte-compile
             (lambda ()
               (call-interactively #'canrylog-file-cleanup))))
  (setq canrylog-file (f-join k/timetracking/ "tracking.canrylog")
        canrylog-meta-file (f-join k/timetracking/ "tasks.json")
        canrylog-ivy-sort-tasks-with-recency (not (getenv "ANDROID_ROOT")))
  (k/general-leader
   "r" #'canrylog-dispatch))

(leaf suggest
  :straight t
  :init (kisaragi-apps-register "Suggest" #'suggest)
  :config
  (setq-mode-local suggest-mode show-trailing-whitespace nil))

(leaf org
  :straight (org :type git
                 :repo "https://git.savannah.gnu.org/git/emacs/org-mode.git"
                 :branch "main"
                 :pin "release_9.5"
                 :local-repo "org")
  :init
  (prog1 'fix-org
    (require 'org-version)
    (setq org-version (org-version)))
  (setq-mode-local org-mode
    fill-column 90)
  (setq org-directory k/notes/
        org-log-into-drawer t
        ;; Make items with deadline within a year show up in agenda.
        ;; this is used by `org-agenda-deadline-faces' to determine
        ;; which face to use (eg. 50% = upcoming, 0% = distant), so
        ;; don't set it to `most-positive-fixnum'.
        org-deadline-warning-days 365
        ;; don't insert hard indent when org-indent-mode is off
        org-adapt-indentation nil
        ;; Don't deny my explicit command
        ;; What should actually happen is show-all skipping archived
        ;; trees. But this is just the way it is.
        org-cycle-open-archived-trees t
        ;; also don't start org-indent-mode automatically
        org-startup-indented nil
        org-agenda-use-tag-inheritance nil
        ;; FILETAGS relies on this
        org-use-tag-inheritance t
        org-startup-indented t
        org-startup-truncated nil
        org-catch-invisible-edits 'show
        org-cycle-separator-lines 0
        org-cycle-emulate-tab nil
        org-refile-targets '((nil . (:maxlevel . 2))
                             (org-agenda-files . (:maxlevel . 3)))
        org-refile-use-outline-path nil
        org-outline-path-complete-in-steps nil
        org-ellipsis "..."
        org-pretty-entities t
        org-priority-highest ?A
        org-todo-keywords
        '((sequence
           "TODO" ; generic todo item
           "WAITING" ; blocked
           "NEXT" ; asap
           "MAYBE" ; I want to do it eventually, but not now
           "WISH" ; optional
           "WIP" ; in-progress projects
           "HABIT" ; highlight this heading is a habit entry
           "|"
           "CANCELLED(c@)" "DONE(d)"))
        org-todo-keyword-faces '(("TODO" org-todo)
                                 ("NEXT" org-todo)
                                 ("WAITING" org-todo)
                                 ("DONE" org-done)
                                 ("CANCELLED" org-done))
        org-priority-lowest ?F
        ;; show me all the tags
        org-complete-tags-always-offer-all-agenda-tags t
        org-hide-emphasis-markers t
        org-return-follows-link t
        org-src-fontify-natively t
        ;; stop throwing extra indentation into source code
        org-edit-src-content-indentation 0
        ;; Use a sensible display size; otherwise the image will push
        ;; everything else away from the screen. We can always just
        ;; visit the link itself to view the full size image.
        ;;
        ;; Putting it in a list allows for #+attr_org: :width overrides.
        org-image-actual-width '(400)
        org-imenu-depth 6
        org-fontify-done-headline t
        org-fontify-whole-heading-line t
        org-fontify-quote-and-verse-blocks t
        ;; consider time before 01:00 to be yesterday
        org-extend-today-until 1
        org-display-inline-images t)
  :commands org-mode
  :config
  (add-hook 'org-mode-hook (k/turn-off-minor-mode company-mode))
  (dolist (lang '(("go-html-template" . web)
                  ("go-text-template" . text)))
    (add-to-list 'org-src-lang-modes lang))
  (prog1 'keys
    (general-def
      :keymaps 'org-mode-map
      :states 'insert
      "[" #'k/insert-link/double-key)
    ;; FIXME: not sure what's the point of this
    (general-def
      :states 'normal
      :keymaps 'org-mode-map
      "TAB" 'org-cycle
      "RET" 'org-open-at-point)
    (general-def :keymaps 'org-mode-map
      "M-p" #'k/org-toggle-hiding-stuff
      "M-o" #'org-mark-ring-goto)
    (k/general-mode-leader
     :keymaps 'org-mode-map
     "s" '(org-schedule :wk "Schedule")
     "t" '(org-todo :wk "Set todo")
     "f" '(org-table-next-field :wk "Next table field")
     "D" '(k/org-set-heading-timestamp :wk "Set CREATED")
     "u" '(k/org-set-updated-keyword :wk "Set #+updated")
     "d" '(org-deadline :wk "Deadline")
     "h" '(k/org-turn-heading-into-habit :wk "Turn heading into habit")
     "^" '(org-sort :wk "Sort heading")
     "-" 'org-ctrl-c-minus
     "," '(org-priority :wk "Priority")
     "o" '(org-open-at-point :wk "Open")
     "I" '(org-toggle-inline-images :wk "Toggle inline images")
     "p" '(:ignore t :wk "Properties")
     "ps" '(org-set-property :wk "Set property...")
     "pc" '(org-columns :wk "Turn on column view")
     "i" '(:ignore t :wk "Insert")
     "il" (general-simulate-key (#'org-insert-link-global "i") :which-key "link")
     "iH" (general-simulate-key (#'org-insert-heading "i") :which-key "heading (before)")
     "ih" '(evil-org-org-insert-heading-respect-content-below :which-key "heading")
     "is" (general-simulate-key (#'org-insert-subheading "i") :which-key "subheading")
     "id" (general-simulate-key (#'org-insert-drawer "i") :which-key "drawer")
     "ip" (general-simulate-key (#'org-insert-property-drawer "i") :which-key "property drawer")
     "L" 'org-toggle-link-display
     "y" '(:ignore t :wk "Table")
     "yn" 'org-table-next-row
     "ya" 'org-table-align))
  (setf (alist-get (rx "."
                       (or "mkv"
                           "mp4"
                           "ogv")
                       eos)
                   org-file-apps)
        "mpv %s"
        (alist-get (image-file-name-regexp)
                   org-file-apps)
        'default
        (alist-get (rx "." (or "kra"))
                   org-file-apps)
        "xdg-open %s")
  (when (executable-find "evince")
    (setf (alist-get (rx "." (or "pdf") "::" (group (+ (any "0-9"))) eos)
                     org-file-apps)
          "evince -p %1 %s"))
  ;; Fixes
  (define-advice org-open-file (:around (func &rest args))
    "Set `process-connection-type' so that xdg-open actually works.

See https://askubuntu.com/questions/646631/emacs-doesnot-work-with-xdg-open"
    (let ((process-connection-type nil))
      (apply func args)))
  ;; Original version of this function hangs the Emacs server because
  ;; it always prompts if FILE does not exist. As I specify my agenda
  ;; files to be files from some directories, any entry that does not
  ;; exist is likely a leftover from, say, Custom. As such, removing
  ;; it is always safe.
  (defun org-check-agenda-file (file)
    "Make sure FILE exists. If not, remove it from the list."
    (unless (file-exists-p file)
      (org-remove-file file)
      (throw 'nextfile t))))

(leaf kisaragi-org-highlight-links-in-title
  :after org
  :require t
  :init
  (add-hook 'org-mode-hook #'kisaragi-org-highlight-links-in-title-mode))

(leaf org-element
  :config
  (defun k/alternate-path-element-wrapper (parser)
    "Make `org-element-link-parser' try some other paths.

The paths are currently hard-coded for a Hugo project.

PARSER is `org-element-link-parser', passed in by the :around advice."
    (let* ((elem (funcall parser))
           (path (org-element-property :path elem)))
      (when (and (equal "file" (org-element-property :type elem))
                 (stringp path)
                 (not (f-exists? path)))
        (-when-let* ((project-root (projectile-project-root))
                     ;; Allow f-join to properly join project-root/dir/filename
                     (path (if (f-absolute? path)
                               ;; (substring path 1) is also an
                               ;; option, but that fails to handle
                               ;; "//path", which is supposed to mean
                               ;; the same thing as "/path".
                               (f-relative path "/")
                             path))
                     (no-ext (f-no-ext path))
                     (newpath (-first #'f-exists?
                                      (list
                                       (f-join project-root "content" path)
                                       ;; special support for Hugo list pages
                                       (f-join project-root "content" no-ext "_index.org")
                                       (f-join project-root "static" path)))))
          (setq elem (org-element-put-property elem :path newpath))))
      elem))
  (advice-add 'org-element-link-parser :around #'k/alternate-path-element-wrapper))

(leaf evil-org
  :straight t :after evil org
  :hook ((org-mode-hook . evil-org-mode))
  :config
  (defun k/evil-org-insert-newline (count)
    "Insert a new line, utilizing `evil-org-open-below' if we're at end of line.

Do so COUNT times."
    (interactive "P")
    (if (eolp)
        (evil-org-open-below count)
      (newline-and-indent count)))
  (evil-org-set-key-theme '(navigation insert textobjects calendar todo))
  (general-def
    :keymaps 'org-mode-map
    :states 'insert
    "RET" #'k/evil-org-insert-newline))

(leaf org-link
  :after org
  :config
  (require 'kisaragi-org-link-diary)
  (require 'kisaragi-file-finders)
  (setf (alist-get 'file org-link-frame-setup)
        #'find-file
        ;; allow fuzzy search with file:filename.org::text
        org-link-search-must-match-exact-headline nil
        org-link-abbrev-alist
        (-filter
         #'cdr
         `(("notes" . ,k/notes/)
           ("school" . ,k/school/)
           ("documents" . ,(-some-> k/cloud/
                             (f-join "documents")
                             (concat "/")))
           ;; do we need this to be absolute?
           ("projects" . ,(->> (f-join k/notes/ "projects.org")
                            (format "file:%s::*")))
           ("project" . k/org-link-abbrev-project)
           ("scratchpad" . ,k/cloud/scratchpad/)
           ("uni-conversation-class-journal" .
            "~/git/uni-conversation-class-journal/")
           ("ledger" . ,k/ledger/)
           ("fiction" . ,(f-join k/notes/ "fiction/"))
           ("mandarin" . ,(->> (f-join k/notes/ "vocab/vocab.org")
                            (format "file:%s::*")))
           ("japanese" . ,(->> (f-join k/notes/ "vocab/vocab.org")
                            (format "file:%s::*")))
           ("english" . ,(->> (f-join k/notes/ "vocab/english.org")
                           (format "file:%s::*")))
           ("poems" . ,(f-join k/notes/ "poems/"))
           ("books" . ,k/books/)
           ("photo" . k/org-link-abbrev-photo)
           ("recording" . k/org-link-abbrev-recording)
           ("diary" . k/org-link-abbrev-diary)
           ("reference" . k/org-link-abbrev-reference)
           ("mega" . ,k/cloud/)
           ("wikipedia-en" . "eww:https://en.wikipedia.org/wiki/")
           ("wikipedia-zh" . "eww:https://zh.wikipedia.org/wiki/")
           ("wikipedia-ja" . "eww:https://ja.wikipedia.org/wiki/"))))
  (org-link-set-parameters "eww" :follow #'eww-browse-url)
  (org-link-set-parameters "chromium" :follow #'browse-url-chromium))

(leaf org-habit
  :after org
  :require t
  :setq ((org-habit-graph-column . 60)
         (org-habit-following-days . 1)))

(leaf org-agenda
  :config
  ;; Do this early so my bindings aren't overwritten
  (with-eval-after-load 'evil-org
    (require 'evil-org-agenda)
    (evil-org-agenda-set-keys))
  (general-def :keymaps 'org-agenda-mode-map
    "q" #'quit-window)
  (define-key org-agenda-mode-map
    [remap org-agenda-switch-to] #'org-agenda-show-and-scroll-up)
  ;; Settings
  (defun k/refresh-org-agenda-files ()
    "Refresh `org-agenda-files' to reflect current files."
    (interactive)
    ;; If a large number of Org files proves to be a problem, we
    ;; might be able to only list files that contain eg. SCHEDULED
    ;; or DEADLINE, which can be done quickly with `grep' or `ag'.
    (setf org-agenda-files
          ;; Note that directory-files only lists one level
          (->> (directory-files k/notes/ :full)
            ;; use a whitelist to not have to load hundreds of
            ;; files
            (--filter (let ((base (f-filename it)))
                        (or
                         (member base '("projects.org"
                                        "utau-covers.org"
                                        "readme.org"
                                        "Inbox.org"
                                        "shopping-list.org"
                                        "books.org"
                                        "todos.org"
                                        "certificates.org"
                                        ;; subdirectories
                                        "university"
                                        "habits"))))))))
  (define-minor-mode k/org-vocab-in-agenda-mode
    "Minor mode: when enabled, include vocabulary from org-msr in org-agenda.

If they were included at all times, the agenda view takes a very
long time to load. Only load them when we want to."
    :global t :lighter "vocab"
    (let ((vocab-dir (f-join k/notes/ "vocab")))
      (if k/org-vocab-in-agenda-mode
          (push vocab-dir org-agenda-files)
        (setq org-agenda-files (remove vocab-dir org-agenda-files))))
    (when (equal (buffer-name) "*Org Agenda*")
      (org-agenda-redo)))
  (k/refresh-org-agenda-files)
  (setf org-agenda-window-setup 'other-window
        org-agenda-span 'day
        org-agenda-include-diary nil
        ;; Put #A before #C in agenda
        ;; (habit-down may not be necessary as we're using org-super-agenda)
        (alist-get 'agenda org-agenda-sorting-strategy)
        '(time-up deadline-up effort-up priority-down scheduled-up habit-down category-keep))
  ;; My own effort format
  (defun k/org-maybe-get-effort ()
    "If entry at point has an effort value, format it for display in the agenda.

Otherwise, return an empty string so that there's no effect on
the display."
    (if-let ((effort (org-entry-get (point) org-effort-property)))
        (format "[%s] " effort)
      ""))
  (setf (alist-get 'agenda org-agenda-prefix-format) " %i %-12:c%(k/org-maybe-get-effort)%?t%s")
  (define-advice org-agenda-list (:after (&rest _))
    "Set `default-directory' in agenda."
    (with-current-buffer org-agenda-buffer-name
      (setq default-directory k/notes/)))
  (define-advice org-agenda-list (:around (func &rest args))
    "Announce that Org-agenda is being loaded."
    (message "Loading Org agenda...")
    (apply func args)
    (message "Loading Org agenda...done")
    ;; preserve org-agenda-list's return value
    "")
  ;; Command to skip task for today
  (leaf org-postpone
    :straight (org-postpone :repo "https://github.com/j-cr/org-postpone")
    ;; not actually its own commands, but I want these functions to
    ;; trigger autoload
    :commands k/org-postpone-entry-until-tomorrow org-postpone-skip-if-postponed
    :init
    (setq org-agenda-skip-function-global #'org-postpone-skip-if-postponed)
    :config
    (defun k/org-postpone-entry-until-tomorrow ()
      "Postpone entry at point until tomorrow.
Like `org-postpone-entry-until-tomorrow', but does not redo the
agenda. This allows doing it after postponing a bunch of tasks."
      (interactive)
      (org-agenda-check-type t 'agenda)
      (org-postpone--set-postponed)
      (org-agenda-set-mode-name)
      (message "Task postponed until tomorrow"))
    (general-def
      :states 'motion
      :keymaps 'org-agenda-mode-map
      "a" #'k/org-postpone-entry-until-tomorrow))
  (general-def
    :states 'motion
    :keymaps 'org-agenda-mode-map
    "<return>" #'org-agenda-show-and-scroll-up
    "RET" #'org-agenda-show-and-scroll-up))

(leaf org-capture
  :config
  (setq org-capture-templates
        `(("i" "Inbox" entry (file "Inbox.org")
           "* %?\n:PROPERTIES:\n:created:  %(k/date-iso8601)\n:END:\n"
           :prepend t)
          ("p" "Public")
          ("pd" "Blog entry (public diary)" plan
           (function
            ,(lambda ()
               (let* ((base (read-string "Base name for new public diary entry: "))
                      (today (k/today))
                      (now (k/date-iso8601))
                      (file (f-join k/notes/ "public" "content" "blog"
                                    (concat today base ".org"))))
                 (find-file file)
                 (when (= 0 (buffer-size))
                   (insert "#+title: " base "\n"
                           "#+created: " now "\n"
                           "#+language: en\n"
                           "#+tags[]:")))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("pc" "Public concept note" plain
           (function
            ,(lambda ()
               (k/new-stray-note
                :title (read-string "New public concept note: ")
                :dir (f-join k/notes/ "public" "content"))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("t" "Diary entry (day)" plain
           (function
            ,(-partial #'kisaragi-diary/new-entry t))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("d" "Diary entry" plain
           ;; Abusing the #' reader syntax
           #'kisaragi-diary/new-entry
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("f" "Fiction entry" plain
           (function ,(-rpartial #'k/new-stray-note :dir "fiction"))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("c" "Stray concept note" plain
           #'k/new-stray-note
           ""
           :immediate-finish t
           :jump-to-captured t)
          ,@(unless k/android?
              '(("s" "Scratchpad entry" plain
                 #'k/new-scratchpad ""
                 :immediate-finish t
                 :jump-to-captured t)))
          ("t" "Immediate todo" plain (file "immediate-todos.org")
           "** %(k/date-iso8601)\n- [ ] %?\n"
           :immediate-finish t
           :jump-to-captured t)
          ("v" "Vocabulary")
          ("vj" "Japanese" entry (file+olp "vocab/vocab.org" "Japanese")
           "* %?\n"
           :prepend t)
          ("vd" "German" entry (file+olp "vocab/german.org" "German")
           "* %?\n"
           :prepend t)
          ("ve" "English" entry (file+olp "vocab/english.org" "English")
           "* %?\n"
           :prepend t)))
  (k/general-leader/notes
    "n" '(org-capture :wk "Add notes")))

(leaf org-columns
  :config
  (general-def :keymaps 'org-columns-map
    "i" #'org-columns-edit-value))

(leaf kisaragi-org-ret-create-new-link
  :require t
  :after org
  :config (kisaragi-org-ret-create-new-link-mode))

(leaf org-babel
  :config
  (leaf ob-racket
    :after racket-mode org
    :straight (ob-racket :host github :repo "hasu/emacs-ob-racket"))
  (setq org-babel-load-languages
        '((emacs-lisp . t)
          (python . t)
          (lisp . t)
          (racket . t)
          (shell . t)
          (R . t))
        org-babel-default-header-args:emacs-lisp
        '((:lexical . "yes")))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

(leaf org-variable-pitch
  :straight t
  :unless k/android?
  :hook ((org-mode-hook . org-variable-pitch-minor-mode))
  :config
  (dolist (face '(org-column-title))
    (add-to-list 'org-variable-pitch-fixed-faces face))
  (set-face-attribute 'org-variable-pitch-face nil
                      ;; Make sure we follow the mono CJK fallback
                      :fontset (face-attribute 'default :fontset)
                      :family "Iosevka"))

(leaf org-msr
  :straight
  (org-msr :type git :host gitlab
           :repo "kisaragi-hiu/org-msr")
  :commands (org-msr-mode)
  :config
  (setq org-msr-keyword-frequency-alist
        '(("DAILY" . "1d")
          ("HARD" . "2d")
          ("SLIGHTLY" . "4d")
          ("UNFAMILIAR" . "8d")
          ("SOMEWHAT" . "2w")
          ("FAMILIAR" . "1m")
          ("BETTER" . "2m")
          ("GREAT" . "4m")
          ("CONFIDENT" . "8m")
          ("JUSTINCASE" . "16m")
          ("YEARS" . "2y")
          ("MEMORIZED" . "never")))
  (k/general-mode-leader
   :keymaps 'org-msr-mode-map
   "j" '(org-msr-insert-current-heading-definition
         :wk "Insert definition for entry")))

(leaf org-super-agenda
  :straight t
  :after org
  :config
  (org-super-agenda-mode)
  ;; Without doing this, I frequently accidentally trigger agenda
  ;; commands when the cursor landed on a heading.
  ;;
  ;; The map is defined as a copy of org-agenda-mode's binding, and
  ;; so evil-org-agenda's bindings don't apply.
  (setq org-super-agenda-header-map (make-sparse-keymap))
  ;; `org-agenda-mode-map' don't seem to apply at the header,
  ;; though. Add the one binding that I actually use.
  (general-def :keymaps 'org-super-agenda-header-map
    "C-x C-s" #'org-save-all-org-buffers)
  (setq org-super-agenda-unmatched-order 9
        ;; multiple normal selectors means OR. use :and (selector ...)
        ;; for AND.
        org-super-agenda-groups `((:discard
                                   (:todo "DONE" :todo "CANCELLED"))
                                  (:name "Morning"
                                         :category "morning")
                                  (:name "Time grid"
                                         :time-grid t)
                                  (:name "School"
                                         :category "school"
                                         :file-path ".*university.*")
                                  (:name "Night"
                                         :category "night")
                                  (:name "Habits"
                                         :habit t
                                         :order 8)
                                  (:name "Now"
                                         :and (:not
                                               (:todo "DONE")
                                               :tag "important"
                                               :deadline future)
                                         :and (:not
                                               (:todo "DONE")
                                               :tag "important"
                                               :deadline today))
                                  (:name "Urgent"
                                         :tag "urgent"
                                         ;; would be better if we
                                         ;; can do "3 days before
                                         ;; deadline", but this will
                                         ;; do for now.
                                         :deadline future
                                         :deadline today)
                                  (:name "Next"
                                         :todo "NEXT"
                                         :tag "next")
                                  (:name "Bookmarks"
                                         :category "bookmarks")
                                  (:name "To Read"
                                         :todo "TOREAD"
                                         :category "books")
                                  (:name "Vocabulary"
                                         :file-path ".*vocab.*"
                                         :order 10)
                                  (:todo "WAITING")
                                  (:name "Shopping list"
                                         :todo "TOBUY"))))

;; my various extra commands
(leaf kisaragi-org
  :after org
  :require t)

;; Show outline in a sidebar
(leaf org-sidebar :straight t)

(leaf org-inline-video-thumbnails
  :after org
  :require t
  :unless k/android?
  :straight (org-inline-video-thumbnails
             :host github
             :repo "kisaragi-hiu/org-inline-video-thumbnails"))

(leaf ox-reveal
  :straight t
  :after org
  :require t
  :unless k/android?
  :config
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))

(leaf ox-texinfo+
  :straight (ox-texinfo+ :host github
                         :repo "tarsius/ox-texinfo-plus")
  :after org
  :require t)

(leaf ox-pollen
  :straight (ox-pollen :host github
                       :repo "kisaragi-hiu/ox-pollen")
  :after org
  :require t
  :disabled t)

(leaf kisaragi-link
  :commands k/insert-link k/insert-link/double-key
  :after org)

(leaf org-roam
  :straight (org-roam :host github
                      :repo "kisaragi-hiu/org-roam"
                      :branch "main")
  :require t
  :after org
  :init
  (k/general-leader/file
    "ed" '(kisaragi-diary/visit-entry-date :wk "diary: today's entry")
    "eD" '(kisaragi-diary/visit-entry-yesterday :wk "diary: yesterday's entry"))
  :config
  (setq org-roam-mode-hook nil)
  (org-roam-mode)
  (run-with-idle-timer 5 nil #'org-roam-db-build-cache)
  (prog1 'org-roam-bibtex
    (org-roam-bibtex-mode)
    (setq orb-templates
          '(("r" "ref" plain
             #'org-roam-capture--get-point ""
             :file-name "reflection/${citekey}"
             :head "#+TITLE: ${author} - ${title}\n#+roam_key: ${ref}\n#+roam_key: ${url}"
             :unnarrowed t)))
    (k/general-mode-leader
     :keymaps 'org-mode-map
     "n" #'kisaragi-notes/literature-note-actions))
  (setq kisaragi-notes-buffer/hidden-tags '("reflection" "diary")
        org-roam-completion-everywhere t
        org-roam-buffer-prepare-hook
        (list
         #'org-roam-buffer--insert-title
         #'kisaragi-notes-buffer//insert-cite-backlinks
         #'kisaragi-notes-buffer//insert-reflection-backlinks
         #'kisaragi-notes-buffer//insert-diary-backlinks
         #'kisaragi-notes-buffer//insert-other-backlinks
         (byte-compile
          (lambda ()
            (unless (< 2 (s-count-matches "\n" (buffer-string)))
              ;; italics
              (insert "\n\n/No backlinks/"))))
         ;; Restore point
         #'kisaragi-notes-buffer//restore-point)
        org-roam-file-extensions '("org" "md")
        kisaragi-notes/tag-sources '(org-roam--extract-tags-prop
                                     org-roam--extract-tags-first-directory)
        kisaragi-notes/slug-replacements '(("[^[:alnum:][:digit:]]" . "-")
                                           ("--*" . "-")
                                           ("^-" . "")
                                           ("-$" . ""))
        org-roam-directory org-directory
        org-roam-buffer-position 'right
        ;; I don't like binding the title to the filename.
        org-roam-rename-file-on-title-change nil
        ;; Don't touch my links.
        org-roam-title-change-hook nil
        org-roam-db-update-method 'immediate
        ;; I don't want a date in most notes; I'll use
        ;; `k/diary-new-entry' if I want it.
        org-roam-capture-templates
        '(("d" "default" plain #'org-roam-capture--get-point "%?"
           :file-name "${slug}"
           :head "#+title: ${title}\n"
           :unnarrowed t))
        org-roam-file-exclude-regexp (rx (or "task-archive.org"
                                             "node_modules"
                                             ;; This is Emacs 27
                                             (regexp
                                              (f-join "templates"
                                                      ".*\\.org")))))
  (prog1 'keys
    (k/general-leader/notes
      "r" '(org-roam-db-build-cache :wk "Update cache explicitly")
      "o" '(kisaragi-notes/open :wk "Open note")
      "f" #'kisaragi-notes/command-palette)
    (general-def
      "C-x f" #'kisaragi-notes/open)
    (k/general-mode-leader
     :keymaps '(org-mode-map markdown-mode-map)
     "l" #'org-roam
     "f" #'kisaragi-notes/open
     "if" #'k/insert-link
     "g" #'org-roam-graph)
    (k/general-mode-leader
     :keymaps 'org-mode-map
     "r" '(:wk "more org-roam commands")
     "rt" '(:ignore t :wk "Tags")
     "rta" '(org-roam-tag-add :wk "Add tag...")
     "rtd" '(org-roam-tag-delete :wk "Delete tag...")
     "ra" '(:ignore t :wk "Aliases")
     "raa" '(org-roam-alias-add :wk "Add alias...")
     "rad" '(org-roam-alias-delete :wk "Delete alias..."))))

(leaf dired
  :hook ((dired-mode-hook . dired-hide-details-mode))
  :setq ((dired-auto-revert-buffer . t)
         (dired-listing-switches . "-avhl")
         (dired-hide-details-hide-symlink-targets . nil))
  :config
  (require 'dired-aux)
  (add-to-list 'dired-compress-files-alist
               '("\\.7z\\'" . "7z a %o %i"))
  (leaf dired-async
    ;; from `async.el'.
    :require async dired-async
    :bind (dired-mode-map
           ;; There is never a reason to block Emacs when we can avoid it.
           ;; This is actually built into Dired.
           ("!" . dired-do-async-shell-command))
    :config (dired-async-mode))
  (leaf dired-git-info :straight t)
  (leaf dired-open
    :straight t :require t
    :config
    (setq dired-open-extensions
          (leaf-normalize-list-in-list
           '((("wav" "ogg" "mkv" "mp4" "pdf") . "xdg-open")
             (("png" "jpg" "exr") . "eog"))
           :dotlist)))
  (leaf diredfl
    :straight t
    :config
    (add-hook 'dired-mode-hook #'diredfl-mode))
  (leaf dired-narrow
    :straight t
    :config
    (k/general-mode-leader
     :keymaps 'dired-mode-map
     "n" '(:ignore t :wk "dired-narrow")
     "nn" '(dired-narrow-fuzzy :wk "Filter (fuzzy)")
     "nr" '(dired-narrow-regexp :wk "Filter by regexp")
     "ns" '(dired-narrow :wk "Filter by string")))
  (leaf dired-collapse
    :straight t :require t
    :config
    (add-hook 'dired-mode-hook #'dired-collapse-mode))
  (leaf dired-show-readme
    :straight (dired-show-readme :host gitlab :repo "kisaragi-hiu/dired-show-readme")
    :config
    (add-hook 'dired-mode-hook #'dired-show-readme-mode))
  (k/general-mode-leader
   :states 'normal
   :keymaps 'dired-mode-map
   "r" 'dired-toggle-read-only)
  (general-def
    :keymaps 'dired-mode-map
    :states 'normal
    "a" #'dired-toggle-read-only
    "C-<return>" #'dired-open-xdg
    ")" #'dired-git-info-mode
    ", o" #'dired-omit-mode))

(leaf awk-ward
  :straight (awk-ward :host gitlab :repo "kisaragi-hiu/awk-ward.el")
  :init (kisaragi-apps-register "Awk-ward.el" #'awk-ward))

(leaf pacfiles-mode
  :straight t
  :when (executable-find "pacman")
  :init (kisaragi-apps-register "Pacfiles" #'pacfiles))

;;;;;; Languages / Major modes

(leaf bnf-mode :straight t)

;; R
(leaf ess
  :straight t
  :unless k/android?)

(leaf make
  :config
  (general-add-hook 'makefile-mode-hook
    (list
     #'outline-minor-mode
     (k/set-x-to-val outline-regexp "# [*]+")))
  (setq-mode-local makefile-mode
    tab-width 4))

(leaf tup-mode :straight t)

(leaf generic-x :require t)

(leaf git-modes
  :straight (git-modes :type git :host github :repo "magit/git-modes"))

(leaf doc-view
  :setq ((doc-view-resolution . 300)))

(leaf sh-mode
  :config
  (compdef :modes 'sh-mode
           :company '(company-shell company-shell-env))
  (with-eval-after-load 'company
    (leaf company-shell :straight t))
  (general-add-hook 'sh-mode-hook
    (list
     #'outline-minor-mode
     #'format-all-mode
     (k/set-x-to-val outline-regexp "# [*]+"))))

(leaf fish-mode
  :straight t
  :when (executable-find "fish")
  :config
  (compdef :modes 'fish-mode
           :capf '(company-fish-shell company-shell-env)))

(leaf markdown-mode
  :straight t edit-indirect
  :config
  (setq-default markdown-hide-markup t
                markdown-enable-wiki-links t)
  (add-hook 'markdown-mode-hook #'variable-pitch-mode)
  (defun k/markdown-toggle-hiding-stuff ()
    "Toggle link display and pretty entities."
    (interactive)
    ;; Don't show any message, just like parinfer.
    (let ((inhibit-message t))
      (k/toggle-multiple
       `((markdown-hide-markup . markdown-toggle-markup-hiding)))))
  (general-def
    :states 'normal
    :keymaps 'markdown-mode-map
    "<tab>" 'markdown-cycle
    "TAB" 'markdown-cycle
    "RET" #'markdown-follow-thing-at-point
    "M-p" #'k/markdown-toggle-hiding-stuff)
  (setq-mode-local markdown-mode
    display-line-numbers nil
    fill-column 120))

(leaf conf-mode
  :mode (("dunstrc" . conf-mode)
         (".*\\.pa\\'" . conf-mode)
         (".*\\.hook\\'" . conf-mode)
         ("\\.directory\\'" . conf-desktop-mode)
         ("_redirects\\'" . conf-space-mode)
         ("Pipfile\\'" . conf-toml-mode))
  :init
  (with-eval-after-load 'org
    (dolist (name '("desktop"))
      (push `(,name . conf-desktop) org-src-lang-modes))))

(leaf meson-mode
  :straight t
  :setq (meson-indent-basic . 4))

(leaf vimrc-mode
  :straight t
  :mode ("\\.vim\\(rc\\)?\\'" . vimrc-mode)
  :init
  (with-eval-after-load 'org
    (dolist (name '("vimscript" "viml" "vimrc"))
      (push `(,name . vimrc) org-src-lang-modes))))

(leaf yaml-mode
  :straight t
  :mode ("\\.yml\\'" . yaml-mode))

(leaf rust-mode
  :straight t
  :when (executable-find "rust")
  :config
  (with-eval-after-load 'rust-mode
    (straight-use-package 'flycheck-rust)
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))

(leaf haskell-mode
  :when (executable-find "ghc")
  :straight t)

(leaf lisp-modes
  :config
  (general-def
    :keymaps (--map (k/symbol-append it "-map") k/lisp-modes)
    :states 'motion
    "zx" #'k/toggle-hs-outline)
  (general-add-hook (--map (k/symbol-append it "-hook")
                           k/lisp-modes)
    (list (k/set-x-to-val evil-shift-width 1)
          #'electric-indent-local-mode
          #'hs-minor-mode)))

(leaf clojure-mode
  :straight t
  :unless k/android?
  :config
  (with-eval-after-load 'lsp
    (general-add-hook 'clojure-mode-hook
      (list (k/set-x-to-val lsp-enable-indentation nil)
            #'lsp)))
  (with-eval-after-load 'flycheck
    (leaf flycheck-clj-kondo
      :straight t :require t
      :when (executable-find "clj-kondo")))
  ;; Copied and adapted from `doom-emacs'.
  (leaf cider
    :straight t
    :config
    (setq nrepl-hide-special-buffers t
          nrepl-log-messages nil
          cider-font-lock-dynamically '(macro core function var deprecated)
          cider-overlays-use-font-lock t
          cider-prompt-for-symbol nil
          cider-repl-history-display-duplicates nil
          cider-repl-history-display-style 'one-line
          cider-repl-history-highlight-current-entry t
          cider-repl-history-quit-action 'delete-and-restore
          cider-repl-history-highlight-inserted-item t
          cider-repl-history-size 1000
          cider-repl-result-prefix ";; => "
          cider-repl-print-length 100
          cider-repl-use-clojure-font-lock t
          cider-repl-use-pretty-printing t
          cider-repl-wrap-history nil
          cider-stacktrace-default-filters '(tooling dup)
          ;; https://github.com/clojure-emacs/cider/issues/1872
          cider-repl-pop-to-buffer-on-connect 'display-only)
    (add-hook 'clojure-mode-hook #'cider-mode)
    (add-to-list 'display-buffer-alist
                 '("\\*cider-error\\*" display-buffer-no-window))
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "'" #'cider-jack-in-clj
     "\"" #'cider-jack-in-cljs
     "c" #'cider-connect-clj
     "C" #'cider-connect-cljs
     "m" #'cider-macroexpand-1
     "M" #'cider-macroexpand-all)
    (k/general-mode-leader
     ;; "e" '(:ignore t :wk "eval")
     ;; "eb" #'cider-eval-buffer
     ;; "ed" #'cider-eval-defun-at-point
     ;; "eD" #'cider-insert-defun-in-repl
     ;; "ee" #'cider-eval-last-sexp
     ;; "eE" #'cider-insert-last-sexp-in-repl
     ;; "er" #'cider-eval-region
     ;; "eR" #'cider-insert-region-in-repl
     ;; "eu" #'cider-undef
     :keymaps '(clojure-mode-map clojurescript-mode-map))
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "g" '(:ignore t :wk "goto")
     "gb" #'cider-pop-back
     "gg" #'cider-find-var
     "gn" #'cider-find-ns
     "h" '(:ignore t :wk "help")
     "hn" #'cider-find-ns
     "ha" #'cider-apropos
     "hc" #'cider-clojuredocs
     "hd" #'cider-doc
     "hj" #'cider-javadoc
     "hw" #'cider-clojuredocs-web)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "i" '(:ignore t :wk "inspect")
     "ie" #'cider-enlighten-mode
     "ii" #'cider-inspect
     "ir" #'cider-inspect-last-result
     "n" '(:ignore t :wk "namespace")
     "nn" #'cider-browse-ns
     "nN" #'cider-browse-ns-all
     "nr" #'cider-ns-refresh)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "r" '(:ignore t :wk "repl")
     "rn" #'cider-repl-set-ns
     "rq" #'cider-quit
     "rr" #'cider-ns-refresh
     "rR" #'cider-restart
     "rb" #'cider-switch-to-repl-buffer
     ;; "rB" #'+clojure/cider-switch-to-repl-buffer-and-switch-ns
     "rc" #'cider-find-and-clear-repl-output
     "rl" #'cider-load-buffer
     "rL" #'cider-load-buffer-and-switch-to-repl-buffer)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "t" '(:ignore t :wk "test")
     "ta" #'cider-test-rerun-test
     "tl" #'cider-test-run-loaded-tests
     "tn" #'cider-test-run-ns-tests
     "tp" #'cider-test-run-project-tests
     "tr" #'cider-test-rerun-failed-tests
     "ts" #'cider-test-run-ns-tests-with-filters
     "tt" #'cider-test-run-test)
    (general-def
      :keymaps 'cider-repl-mode-map
      :states 'insert
      "S-RET" #'cider-repl-newline-and-indent
      "M-RET" #'cider-repl-return)
    (k/general-mode-leader
     :keymaps 'cider-repl-mode-map
     :states 'insert
     "n" #'cider-repl-set-ns
     "q" #'cider-quit
     "r" #'cider-ns-refresh
     "R" #'cider-restart
     "c" #'cider-repl-clear-buffer)
    (general-def
      :keymaps 'cider-repl-history-mode-map
      :states 'insert
      "RET" #'cider-repl-history-insert-and-quit
      "q" #'cider-repl-history-quit
      "l" #'cider-repl-history-occur
      "s" #'cider-repl-history-search-forward
      "r" #'cider-repl-history-search-backward
      "U" #'cider-repl-history-undo-other-window))
  (k/general-mode-leader
   :keymaps '(clojure-mode-map clojurescript-mode-map)
   "eR" #'cider
   "eb" #'cider-eval-buffer
   "ed" #'cider-eval-defun-at-point
   "er" #'cider-eval-region
   "el" #'cider-eval-last-sexp))

(leaf janet-mode
  :straight t
  :when (executable-find "janet"))

(leaf lisp-mode
  :mode ("Cask" . lisp-mode) (".sbclrc" . lisp-mode)
  :config
  (leaf slime
    :when (executable-find "sbcl")
    :straight t
    :config
    (k/general-mode-leader
     :keymaps 'lisp-mode-map
     "eb" #'slime-eval-buffer
     "ed" #'slime-eval-defun
     "er" #'slime-eval-region
     "el" #'slime-eval-last-expression
     "eR" #'slime)
    (setq inferior-lisp-program "sbcl")))

(leaf racket-mode
  :straight t scribble-mode
  :when (executable-find "racket")
  :setq ((racket-indent-sequence-depth . 3))
  :config
  ;; Make absolutely sure Racket files are started in racket-mode
  (defun k/dont-load-.rkt-with-scheme ()
    (setq auto-mode-alist
          (delete '("\\.rkt\\'" . scheme-mode)
                  auto-mode-alist))
    (when (-some->> buffer-file-name
            (s-suffix? ".rkt"))
      (racket-mode)))
  (add-hook 'scheme-mode-hook #'k/dont-load-.rkt-with-scheme)
  (add-hook 'racket-mode-hook #'racket-xp-mode)
  ;; (general-add-hook 'racket-repl-mode-hook
  ;;   (k/set-x-to-val
  ;;    eldoc-documentation-function #'racket-repl-eldoc-function))
  (general-add-hook 'racket-xp-mode-hook
    (k/set-x-to-val
     eldoc-documentation-function #'racket-xp-eldoc-function))
  (k/general-mode-leader
   :keymaps '(racket-mode-map racket-repl-mode-map)
   "d" 'racket-xp-documentation
   "e" '(:ignore t :which-key "eval")
   "er" #'racket-send-region
   "ed" #'racket-send-definition
   "el" #'racket-eval-last-sexp
   "eR" #'racket-run
   "E" '(:ignore t :which-key "expand")
   "Ea" 'racket-expand-again
   "o" 'racket-open-require-path
   "t" 'racket-test))

(leaf web
  :config
  (prog1 'html
    (leaf lsp-tailwindcss
      :straight (lsp-tailwindcss :type git :host github
                                 :repo "merrickluo/lsp-tailwindcss")
      :init (setq lsp-tailwindcss-add-on-mode t)
      :after lsp-mode
      :require t)
    (leaf web-mode
      :straight t
      :init
      (dolist (regexp '("\\.phtml\\'" "\\.tpl\\.php\\'"
                        "\\.[agj]sp\\'" "\\.as[cp]x\\'"
                        "\\.erb\\'" "\\.mustache\\'"
                        "\\.svelte\\'"
                        "\\.djhtml\\'" "\\.html?\\'"))
        (add-to-list 'auto-mode-alist (cons regexp 'web-mode)))
      :config
      (general-add-hook 'web-mode-hook
        (list #'format-all-mode
              #'hs-minor-mode
              #'lsp))
      (with-eval-after-load 'org
        (dolist (name '("svelte" "html"))
          (push `(,name . web) org-src-lang-modes)))))
  (prog1 'js
    (leaf json-mode
      :straight t
      :leaf-defer nil
      :mode ("\\.json\\'" . json-mode)
      :config
      (add-hook 'json-mode-hook #'format-all-mode))
    (leaf js2-mode
      :straight t
      :leaf-defer nil
      :mode ("\\.js\\'" . js2-mode)
      :config
      (leaf flycheck-jest
        :straight t
        :when (executable-find "jest")
        :config
        (flycheck-jest-setup))
      (general-add-hook 'js2-mode-hook
        (list #'origami-mode
              #'format-all-mode
              #'lsp))
      (with-eval-after-load 'org
        (add-to-list 'org-src-lang-modes '("javascript" . js2))))
    (leaf typescript-mode
      :when (executable-find "tsc")
      :straight t
      :config
      (general-add-hook 'typescript-mode-hook
        (list #'origami-mode
              #'format-all-mode
              #'lsp))))
  (prog1 'css
    (leaf ssass-mode :straight t)
    (leaf emmet-mode
      :straight t
      :init
      ;; Expand with C-RET
      (general-add-hook '(css-mode-hook scss-mode-hook web-mode-hook)
        #'emmet-mode))
    :config
    (general-add-hook '(css-mode-hook
                        scss-mode-hook
                        less-css-mode-hook)
      #'format-all-mode))
  ;; Neat tree visualization in CSS / HHTML
  (leaf cakecrumbs
    :straight t
    :config
    ;; Yanked from its README
    (setq cakecrumbs-html-major-modes   '(html-mode web-mode nxml-mode sgml-mode)
          cakecrumbs-jade-major-modes   '(yajade-mode jade-mode pug-mode)
          cakecrumbs-scss-major-modes   '(scss-mode less-css-mode css-mode)
          cakecrumbs-stylus-major-modes '(stylus-mode sass-mode))
    ;; Auto `add-hook' for above major-mode.  (Auto enable `cakecrumbs'
    ;; for the major-modes which have specify in above variables)
    ;; This automatically do this for you:
    ;; (add-hook 'MODE-HOOK 'cakecrumbs-enable-if-disabled)
    (cakecrumbs-auto-setup)
    (setq cakecrumbs-refresh-delay-seconds 0.1
          cakecrumbs-separator " | "
          cakecrumbs-ellipsis "[...] "
          ;; Ignore some patterns in selector string
          cakecrumbs-ignored-patterns
          ;; Bootstrap's .col-*
          '("[.]col-[a-z][a-z]-[0-9]+"))))

(leaf python
  :when (executable-find "python")
  ;; It's like Org: there's a version bundled with Emacs, but the
  ;; latest version is updated more frequently on GNU ELPA
  :straight t
  :commands python-mode
  :config
  (general-add-hook 'python-mode-hook
    (list #'hs-minor-mode
          #'format-all-mode))
  (k/general-mode-leader
   :keymaps '(python-mode-map inferior-python-mode-map)
   "eR" #'run-python
   "ed" #'python-shell-send-defun
   "eb" #'python-shell-send-buffer
   "er" #'python-shell-send-region
   "es" #'python-shell-send-statement)
  (setq-mode-local python-mode
    tab-width 4
    fill-column 79)
  (leaf pipenv
    :when (executable-find "pipenv")
    :straight t
    :config
    (add-hook 'python-mode-hook #'pipenv-mode)
    (setq pipenv-projectile-after-switch-function
          #'pipenv-projectile-after-switch-extended))
  (setq python-shell-interpreter "ipython"
        python-shell-interpreter-args "-i")
  (when (executable-find "black")
    (general-add-hook 'python-mode-hook
      #'format-all-mode))
  (when (executable-find "pyls")
    (add-hook 'python-mode-hook #'lsp)
    (require 'dap-python)))

(leaf ein
  :straight t
  :unless k/android?
  :init (kisaragi-apps-register "EIN (Jupyter Notebook in Emacs)" #'ein:run))

(leaf prog-mode
  :config
  (setq-mode-local prog-mode
    display-line-numbers 'relative
    show-trailing-whitespace t)
  (general-add-hook 'prog-mode-hook
    '(goto-address-prog-mode
      auto-fill-mode
      hl-line-mode
      highlight-numbers-mode
      rainbow-delimiters-mode)))

(leaf bibliography
  :config
  (leaf kisaragi-org-bibtex
    ;; autoload cookies don't work without package.el or straight.el
    :commands
    kisaragi-org-bibtex-export
    kisaragi-org-bibtex-import
    kisaragi-org-bibtex-generate-id
    kisaragi-org-bibtex-auto-export-mode
    kisaragi-org-bibtex-fast-inbook
    kisaragi-org-bibtex-fast-book
    kisaragi-org-bibtex-fast-video
    kisaragi-org-bibtex-fast-article
    kisaragi-org-bibtex-fast-url
    kisaragi-org-bibtex-entries-fill-info
    kisaragi-org-bibtex-entries-fill-bibtex-id
    :config
    ;; bibtex-completion uses this
    (setq org-bibtex-key-property "BIBTEX_ID"
          kisaragi-org-bibtex-ignored-todo-keywords
          '("RETURNED")
          kisaragi-org-bibtex-ignored-properties
          '("PHYSICAL" "WHERE" "RATING" "EFFORT" "POSTPONED")
          kisaragi-org-bibtex-ignored-tags
          '("partial")))
  (with-eval-after-load 'bibtex
    (dolist (entry-type '(("Game" "Interactive media"
                           (("title" "Title of the game")
                            ("author"))
                           (("publisher") ("year"))
                           (("series")
                            ("month")))
                          ("Software" "Software"
                           (("title" "Title of the piece of software")
                            ("author"))
                           nil
                           (("url")))
                          ("Video")
                          ("Course")
                          ("Music")
                          ("Audio")
                          ("Tweet")
                          ("Thing")))
      (add-to-list 'bibtex-biblatex-entry-alist entry-type))
    ;; for mvbook, etc.
    (bibtex-set-dialect 'biblatex)
    (add-hook 'bibtex-mode-hook #'hs-minor-mode))
  ;; Org 9.5 builtin citation
  (leaf org-cite
    :after org
    :config
    (setq org-cite-global-bibliography (map-values k/bibliography-files)))
  (leaf org-ref
    :straight t
    :require t
    :after org)
  (leaf bibtex-completion
    :straight t ivy-bibtex helm-bibtex
    ;; Force leaf to load `:config' only after bibtex-completion is loaded.
    :commands helm-bibtex
    :init
    (general-def
      "C-x b" #'helm-bibtex)
    :config
    ;; No thanks.
    (defun k/bibtex-shorten-names (names)
      "Return a comma-separated list of names in NAMES (a string)."
      (->> names
        (s-replace " and others" " et al.")
        (s-replace " and " ", ")))
    (defalias 'bibtex-completion-shorten-authors
      #'k/bibtex-shorten-names)
    (define-advice bibtex-completion-prepare-entry
        (:around (func entry &optional fields do-not-find-pdf)
                 show-url)
      ""
      (when-let ((prepared-entry (apply func entry fields do-not-find-pdf)))
        (when (and (not do-not-find-pdf)
                   (or (bibtex-completion-get-value "url" prepared-entry)
                       (bibtex-completion-get-value "link" prepared-entry)))
          (push (cons "=has-pdf=" bibtex-completion-pdf-symbol)
                prepared-entry))
        prepared-entry))
    (setf bibtex-completion-bibliography k/bibliography-files
          bibtex-completion-notes-path k/literature-notes-directory
          bibtex-completion-library-path k/books/
          bibtex-completion-edit-notes-function #'orb-edit-notes
          bibtex-completion-additional-search-fields '("keywords" "alias")
          ivy-bibtex-default-action #'ivy-bibtex-edit-notes
          ;; `org-ref-helm-bibtex' used to directly `setq'
          ;; bibtex-completion-display-formats. Now it looks at
          ;; `org-ref-bibtex-completion-add-keywords-field' to decide
          ;; whether to add the keywords field.
          ;; Try `debug-on-variable-change' next time a variable
          ;; is changed for some reason.
          (alist-get t bibtex-completion-display-formats)
          ;; Up/down in Helm will treat the two lines as separate
          ;; entries, but matching and selection still treat the whole
          ;; thing as one entry.
          "${year:4} ${=has-note=:1}${=has-pdf=:1} ${=type=:10} ${author:22}\t${keywords:50}\t${title:100}"
          (alist-get 'course bibtex-completion-display-formats)
          "${year:4} ${=has-note=:1} \t${=type=:10}\t${title:50}\t${keywords:50}"
          (alist-get 'person bibtex-completion-display-formats)
          "${=has-note=:1} ${=type=:10}\t${keywords:20}\t${title:20} ${alias:*}"
          (alist-get 'thing bibtex-completion-display-formats)
          "${year:4} ${=has-note=:1} ${=type=:10} ${keywords:50}\t${title:100}")))

(leaf safe-local-variables
  :config
  ;; see docstring of `safe-local-variable-p'.
  (dolist (sym '(k/org-insert-note-default-prefix
                 bibtex-completion-bibliography
                 bibtex-completion-notes-path
                 kisaragi-org-bibtex-file))
    (put sym 'safe-local-variable #'stringp))
  (dolist (sym '(evil-org-special-o/O
                 org-src-preserve-indentation
                 org-reverse-note-order))
    (put sym 'safe-local-variable #'booleanp))
  (dolist (sym '(org-image-actual-width))
    (put sym 'safe-local-variable
         (-orfn (k/listof #'numberp)
                #'numberp
                #'booleanp)))
  (dolist (sym '(kisaragi-org-bibtex-ignored-tags
                 kisaragi-org-bibtex-ignored-properties))
    (put sym 'safe-local-variable
         (k/listof #'stringp)))
  (dolist (sym '(orb-templates))
    (put sym 'safe-local-variable
         (k/listof
          (-orfn #'stringp
                 #'symbolp
                 #'booleanp
                 #'consp))))
  (dolist (sym '(bibtex-completion-bibliography))
    (put sym 'safe-local-variable
         (-orfn #'stringp
                (k/listof
                 (-orfn #'stringp
                        (k/consof #'stringp))))))
  (add-to-list 'safe-local-eval-forms '(org-content 2)))

(leaf emacs-lisp-mode
  :config
  (k/general-mode-leader
   :keymaps 'emacs-lisp-mode-map
   "eR" #'ielm
   "el" #'eval-last-sexp
   "ed" #'eval-defun
   "er" #'eval-region
   "eb" #'eval-buffer)
  (cl-loop
   for (symbol indent)
   in '((general-add-hook 1)
        (setq-mode-local 1)
        (general-remove-hook 1)
        (flycheck-ert-def-checker-test 3))
   do (put symbol 'lisp-indent-function indent))
  (global-dash-fontify-mode)
  (with-eval-after-load 'org
    ;; Earlier entries shadow later ones, which is a core property of alists
    ;; https://emacs.stackexchange.com/questions/33892/
    (dolist (pair '(("emacs-lisp" . lisp-interaction)
                    ("elisp" . lisp-interaction)))
      (add-to-list 'org-src-lang-modes pair)))
  (with-eval-after-load 'format-all
    (general-add-hook 'emacs-lisp-mode-hook
      #'format-all-mode))
  (leaf flycheck-relint
    :straight t
    :config
    (flycheck-relint-setup))
  (leaf flycheck-package
    :straight t
    :config
    (flycheck-package-setup)
    ;; See https://github.com/purcell/package-lint/issues/153
    ;; The Package-Requires lint relies on package.el, so set it up
    ;; for it.
    (setq package-archives
          '(("gnu" . "https://elpa.gnu.org/packages/")
            ("melpa" . "https://melpa.org/packages/")))
    (package-refresh-contents :async)
    (define-advice package-lint--check-symbol-separators
        (:around (func &rest _) allow-nonstandard-prefix)
      "Ignore the package-lint error about non-standard prefixes."
      nil))
  (leaf macrostep
    :straight t
    :config
    (general-def
      :keymaps 'macrostep-keymap
      :states 'normal
      "c" #'macrostep-collapse))
  (leaf lisp-extra-font-lock
    :straight t
    :config
    ;; The check for whether to start the local minor mode happens
    ;; during buffer creation, so there's no need to worry about slowing
    ;; down editing performance. See `define-global-minor-mode'.
    (lisp-extra-font-lock-global-mode))
  (leaf nameless
    :straight t
    :config
    (setq nameless-private-prefix t)
    (general-def :keymaps 'nameless-mode-map
      "_" #'nameless-insert-name))
  (defvar k/emacs-lisp-eval-keys
    `("e" (:ignore t :wk "eval as elisp")
      "eb" (eval-buffer :wk "Entire buffer")
      "ed" (eval-defun :wk "Defun")
      "er" (eval-region :wk "Region")
      "el" (eval-last-sexp :wk "Last sexp")
      "eL" (pp-eval-last-sexp :wk "Last sexp (pp)")
      "ep" (eval-print-last-sexp :wk "Last sexp (insert)")
      "em" (macrostep-expand :wk "Step through macro")))
  (apply #'k/general-leader k/emacs-lisp-eval-keys)
  (apply #'k/general-mode-leader
         :keymaps '(emacs-lisp-mode lisp-interaction-mode lisp-data-mode)
         k/emacs-lisp-eval-keys))

(leaf origami
  :straight t)

(use-package ledger-mode
  :straight t
  :init
  (leaf kisaragi-ledger
    :require t
    :commands k/ledger-visit-latest)
  (leaf flycheck-ledger :straight t :require t)
  (leaf company-ledger
    :straight t
    :after company
    :init
    (add-to-list 'company-backends 'company-ledger))
  (leaf evil-ledger
    :straight t
    :init
    (add-hook 'ledger-mode-hook #'evil-ledger-mode)
    (add-hook 'ledger-report-mode-hook #'evil-motion-state))
  :config
  (require 'flycheck-ledger)
  (defun k/ledger-format-specifier-period ()
    "Substitutes %(period) in `ledger-reports'."
    (read-string "Reporting period: "))
  (cl-pushnew (cons "period" #'k/ledger-format-specifier-period)
              ledger-report-format-specifiers)
  (setq ledger-highlight-xact-under-point nil
        ledger-clear-whole-transactions t
        ;; Unfortunately ledger does not (yet?) support just using a
        ;; timestamp. Use 2000-01-01 style instead of 2000/01/01 so
        ;; that we don't have to convert dates if support is added.
        ;; We can't use `ledger-iso-date-format' because it's not
        ;; loaded yet.
        ledger-default-date-format "%Y-%m-%d"
        ledger-accounts-file (f-join k/ledger/ "accounts.ledger"))
  (put 'ledger-reports 'safe-local-variable
       (lambda (v)
         (and (listp v)
              (--all? (and (listp it)
                           (= 2 (length it))
                           (stringp (elt it 0))
                           (stringp (elt it 1)))
                      v))))
  (cl-flet ((report (command) (s-join " " (list "%(binary)"
                                                "--date-format %F"
                                                "-f %(ledger-file)"
                                                command))))
    (setq ledger-reports
          `(("budget"                     ,(report "bal Budget"))
            ("budget, in terms of $"      ,(report "bal Budget -X $"))
            ("balance, in terms of $"     ,(report "bal --real -X $"))
            ("balance"                    ,(report "bal --real"))
            ("assets and liabilities"     ,(report "bal --real Assets Liabilities"))
            ("assets and liabilities ($)" ,(report "bal --real Assets Liabilities -X $"))
            ("balance, including budget"  ,(report "bal"))
            ("cash flow (outflow: positive means losing money)"
             ,(report "balance Income Expenses"))
            ("expenses, sorted by date"   ,(report "reg Expenses -S amount --real"))
            ("expenses, sorted by amount" ,(report "reg Expenses -S amount --real"))
            ("prices"                     ,(report "prices"))
            ("reg"                        ,(report "reg"))
            ("payee"                      ,(report "reg @%(payee)"))
            ("account"                    ,(report "reg %(account)"))
            ("account (with period)"      ,(report "reg %(account) --period %(period)")))))
  (setq-mode-local ledger-report-mode
    display-line-numbers nil)
  (setq-default ledger-master-file k/ledger-file)
  (general-define-key
   :keymaps 'ledger-mode-map
   :states 'normal
   "<tab>" (general-simulate-key "za"))
  (general-add-hook 'ledger-mode-hook
    `(outline-minor-mode
      auto-fill-mode
      k/delete-trailing-whitespace-mode
      literate-calc-minor-mode
      ,(lambda () (add-hook 'before-save-hook #'k/ledger-format-buffer nil t))))
  (define-advice ledger-report-redo (:around (func &rest _) keep-point)
    "Try to keep point in ledger report after refresh."
    (when-let ((current-buffer (current-buffer))
               (report-buffer (get-buffer ledger-report-buffer-name)))
      (with-current-buffer report-buffer
        (let ((point-in-report (point)))
          (funcall func)
          (ignore-errors (goto-char point-in-report))))
      (pop-to-buffer current-buffer)))
  (k/general-mode-leader
   :keymaps 'ledger-mode-map
   ;; bindings copied from here
   ;; https://github.com/rememberYou/.emacs.d/blob/master/config.org#hydra--ledger
   "A" #'k/ledger-add-transaction
   "a" '(k/ledger-add-simple-transaction :wk "Add a simple transaction")
   "c" #'ledger-mode-clean-buffer
   "C" #'ledger-copy-transaction-at-point
   "d" #'ledger-delete-current-transaction
   "rr" '(ledger-report :wk "Report (All imported files)")
   "rc" '(k/ledger-report-this-file :wk "Report (this file)")
   "e" #'k/ledger-extract-accounts
   "w" '(:ignore t :wk "write")
   "wa" '(k/ledger-write-accounts :wk "Write existing accounts to file")
   "wc" '(k/ledger-write-commodities :wk "Write existing commodities to file")
   "f" '(k/ledger-format-buffer :wk "Format buffer")))

(leaf ini-mode :straight t)
(leaf batch-mode :straight t)

(leaf ust-mode
  :straight (ust-mode :host github :repo "kisaragi-hiu/ust-mode"))

(leaf csv-mode :straight t)
(leaf systemd :straight t)

(leaf plisp-mode
  :straight t
  :leaf-defer nil
  :when (executable-find "picolisp")
  :mode ("\\.l\\'" . plisp-mode)
  :config
  (setq plisp-documentation-method 'plisp--shr-documentation
        plisp-documentation-directory
        (if k/android?
            (f-join (getenv "PREFIX")
                    "lib/picolisp/doc/")
          "/usr/lib/picolisp/doc/")))

(leaf pollen-mode
  :straight t
  :mode ("\\.ptree\\'" . pollen-mode)
  :config
  (add-hook 'pollen-mode-hook #'rainbow-delimiters-mode)
  (setq-mode-local pollen-mode comment-start "◊;")
  (k/general-mode-leader
   :keymaps 'pollen-mode-map
   "s" '(pollen-server-start :wk "Start Server")
   "p" '(pollen-server-stop :wk "Stop Server")
   "e" '(pollen-edit-block-other-window :wk "Edit Block")))

(leaf kisaragi-desktop
  :require t
  :config
  (defun k/send-time-notification ()
    (k/send-notification
     (format-time-string "Current time: %H:%M")
     :body (format "Emacs has been running for %s." (emacs-uptime)))
    (let ((sound "/usr/share/sounds/Oxygen-Im-Low-Priority-Message.ogg"))
      (when (file-exists-p sound)
        (start-process "mpv" nil "mpv" sound))))
  (defvar k/alarm-timer
    (run-at-time
     ;; The next hour
     (format "%s:00" (1+ (cl-third (decode-time (current-time)))))
     ;; Repeat every hour
     (* 60 60)
     #'k/send-time-notification)))

(leaf fcitx
  :when (executable-find "fcitx")
  :straight t
  :config
  (setq fcitx-use-dbus t)
  (fcitx-default-setup))

(leaf kisaragi-fcitx
  :require t
  :when (executable-find "fcitx")
  :config (general-def
            "<insert>" #'k/fcitx-cycle-current-im
            "S-<insert>" (k/expose (lambda () (k/fcitx-cycle-current-im -1)))))

;;;; Load other config files
(leaf kisaragi-extra-functions
  :require t
  :config
  (general-def
    "M-!" #'k/async-shell-command)
  (general-def :states 'visual
    "A" #'k/ellipsis
    "M" #'k/em-dash))

(leaf kisaragi-hugo
  :require t
  :config
  (k/general-leader
   "mj" #'k/hugo-jump-language))

(leaf kisaragi-units
  :require t)

(leaf synchronize-git
  :commands synchronize-git
  :straight (synchronize-git :type git
                             :host github
                             :repo "kisaragi-hiu/synchronize-git.el")
  :init
  (setq synchronize-git-default-repos
        (-remove #'not (list k/ledger/
                             k/notes/
                             (file-name-directory canrylog-file)
                             (f-expand user-emacs-directory)
                             (when (k/system-is-p "MF-PC")
                               (f-expand "~/git/voice-practice/"))))))

(leaf kisaragi-timestamp-commands
  :require t
  :config
  (k/general-leader/extra-editing-commands
    "tz" '(kisaragi-timestamp-commands/convert-iso8601-timezone
           :wk "Convert to current timezone")
    "ts" '(kisaragi-timestamp-commands/iso8601-no-symbol-to-full
           :wk "20200101... -> 2020-01-01...")
    "tc" '(kisaragi-timestamp-commands/commit-timestamp-to-iso8601
           :wk "Commit timestamp to ISO 8601")))

(require 'format-time-string-patch)

(when k/android?
  (ignore-errors
    (server-start))
  ;; set DISPLAY in Termux so browse-url works
  (setenv "DISPLAY" "dummy")
  ;; This is from Termux's site-lisp
  (xterm-mouse-mode 1)
  (global-set-key [mouse-4] 'scroll-down-line)
  (global-set-key [mouse-5] 'scroll-up-line))

(leaf server
  :when (daemonp)
  :config
  ;; Set XDG_CURRENT_DESKTOP from within Emacs.
  ;;
  ;; If we start Emacs as a systemd unit, it does not inherit
  ;; XDG_CURRENT_DESKTOP from the desktop environment. This causes
  ;; `xdg-open' to act differently: for example, the fallback mode
  ;; `xdg-open' uses does not recognize UST files, and so opens them
  ;; as text files. This works around that.
  ;;
  ;; Let's not think about what happens in a TTY. I'll worry about
  ;; that later.
  (unless (or (getenv "XDG_CURRENT_DESKTOP")
              k/android?)
    (cond ((k/pgrep-boolean "plasmashell")
           (setenv "XDG_CURRENT_DESKTOP" "KDE")
           ;; xdg-open runs code for KDE 3 if this isn't set
           (setenv "KDE_SESSION_VERSION" "5"))))
  (defun k/set-display-if-empty (&optional _)
    "Set the environment variable DISPLAY to \":0\" if it's not set.

This allows running X related stuff even when there's no frame active."
    (unless (getenv "DISPLAY")
      ;; Maybe something will set it to empty again when the frame is
      ;; actually being killed, I don't know...
      ;; Don't block, and allow that something to run.
      (make-thread
       (lambda ()
         (while (not (getenv "DISPLAY"))
           (setenv "DISPLAY" ":0")
           (sleep-for 2))))))
  (general-add-hook '(delete-frame-functions
                      after-init-hook)
    #'k/set-display-if-empty)
  (defun k/signal-startup-complete ()
    "Send a startup ready notification."
    (interactive)
    (k/send-notification "Emacs is ready."))
  (add-hook 'after-init-hook #'k/signal-startup-complete)
  (unless (version< emacs-version "27")
    (general-add-hook 'server-after-make-frame-hook
      ;; Tip from https://www.reddit.com/r/emacs/comments/it4m2w/weekly_tipstricketc_thread/g5kr7z7/
      (lambda () (select-frame-set-input-focus (selected-frame))))))

(leaf kisaragi-preload-org-files
  ;; :unless k/android?
  :disabled t
  :straight (background-job :branch "main"
                            :host github
                            :repo "kisaragi-hiu/background-job.el")
  :require t)

;;; init.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:

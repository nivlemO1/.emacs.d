;;; fix-ffap-on-iso8601.el --- Fix ffap on top of a timestamp -*- lexical-binding: t -*-

;;; Commentary:

;; Try running `ffap' with point on "/2000-01-01T00:00:00Z": Emacs
;; will complain that "Method ‘2000-01-01T00’ is not known.",
;; whereas it's supposed to just fall back to prompting for a file.
;;
;; This tries to detect if the thing under point contains a timestamp,
;; and if it does just tell `ffap' and `tramp' that it's not a remote file.
;;
;; This fails for remote files named with full ISO 8601 names. It'll
;; fail to see it as a remote file.
;;
;; (Colon in files cause breakage elsewhere anyways; This is fine for
;; my personal config.)

;;; Code:

(require 'kisaragi-helpers)

;; These should both return `nil':
;; (tramp-tramp-file-p "/2000-01-01T00:00:00Z")
;; (ffap-file-remote-p "/2000-01-01T00:00:00Z")

(define-advice ffap-file-remote-p (:around (orig filename))
  "An ISO 8601 timestamp is not remote."
  (and (not (k/contains-iso8601-p filename))
       (funcall orig filename)))

(define-advice tramp-tramp-file-p (:around (orig name))
  "An ISO 8601 timestamp is not a tramp file."
  (and (not (k/contains-iso8601-p name))
       (funcall orig name)))

(provide 'fix-ffap-on-iso8601)

;;; fix-ffap-on-iso8601.el ends here

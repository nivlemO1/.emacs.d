;;; k-kde-window-colors --- Synchronize KWin window colors with Emacs theme  -*- lexical-binding: t; -*-

;;; Commentary:

;; macOS has the `ns-transparent-titlebar' frame property. Emulate the
;; same thing in KDE Plasma.

;; Setup:
;; 1. Run `k/kde-window-colors-refresh' once to create the theme file
;; 2. Right click the window decoration of Emacs, then
;;    Other Actions → Configure Special Application Settings
;; 3. Press Add Property..., select Titlebar color scheme
;; 4. Set the first field to Force, the theme field to "Emacs window colors"
;; 5. Press OK
;;
;; Afterwards, run `k/kde-window-colors-refresh' everytime after
;; `load-theme', possibly with an advice. I do it with a custom function.

;;; Code:

(require 'kisaragi-helpers)

(require 'dash)
(require 's)
(require 'f)
(require 'seq)
(require 'xdg)

(defun k/color-hex-to-rgb (hex-string)
  "Convert HEX-STRING in #RRGGBB notation to a list of RGB values.
k/color-hex-to-rgb \"#556b72\" -> (85 107 114)"
  (let* ((without-hash (substring hex-string 1))
         (parts (seq-partition without-hash (/ (length without-hash) 3))))
    (--map (read (concat "#x" it)) parts)))

(defun k/color-hex-to-rgb-comma-string (hex-string)
  "Convert HEX-STRING in #RRGGBB notation to decimal \"RRR,GGG,BBB\"."
  (s-join "," (mapcar #'prin1-to-string (k/color-hex-to-rgb hex-string))))

(cl-defun k/kde-window-colors-generate (&optional (face 'default))
  "Use the colors of FACE to generate a KDE window color scheme."
  (let ((background (k/color-hex-to-rgb-comma-string (face-attribute face :background)))
        (foreground (k/color-hex-to-rgb-comma-string (face-attribute face :foreground))))
    (s-lex-format "[General]
ColorScheme=Emacs window colors
Name=Emacs window colors

[WM]
activeBackground=${background}
activeBlend=${background}
activeForeground=${foreground}
inactiveBackground=${background}
inactiveBlend=${background}
inactiveForeground=${foreground}")))

(defun k/kde-window-colors-write-theme ()
  "Create a KDE window color scheme and write it to the right location."
  (let ((color-scheme-dir (f-join (xdg-data-home) "color-schemes")))
    (make-directory color-scheme-dir t)
    (with-temp-file (f-join color-scheme-dir "emacs-window-colors.colors")
      (insert
       (k/kde-window-colors-generate 'default)))))

(defun k/kde-window-colors-refresh ()
  "Write Emacs colors to a KDE color scheme and reload KWin."
  (interactive)
  (condition-case _
      (progn
        (k/kde-window-colors-write-theme)
        ;; The "org.kde.KWin /KWin reconfigure" dbus method is not enough
        ;; to reload the color scheme. Hopefully this doesn't cause issues.
        (when (k/pgrep-boolean "kwin_x11")
          ;; setsid tip from:
          ;; https://emacs.stackexchange.com/questions/22363/
          (start-process "kwin" nil "setsid" "kwin_x11" "--replace")))
    ;; If something errored out when converting the color... just
    ;; don't do anything.
    (invalid-read-syntax nil)))

(provide 'k-kde-window-colors)
;;; k-kde-window-colors.el ends here

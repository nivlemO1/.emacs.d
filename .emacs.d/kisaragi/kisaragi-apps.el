;;; kisaragi-apps.el --- Register and run "apps" -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1
;; Homepage: https://kisaragi-hiu.com/.emacs.d
;; Package-Requires: ((emacs "25.1") (ivy "0.10.0"))
;; Keywords: convenience

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; M-x lists all commands, making it really hard to find entry points
;; to packages. This is my way of dealing with this in my config.

;;; Code:

(require 'cl-lib)
(require 'pcase)
(require 'ivy)

(defvar kisaragi-apps--history nil
  "History for `kisaragi-apps'.")
(defvar kisaragi-apps-list nil
  "List of apps registered through `kisaragi-apps-register'.")

(defun kisaragi-apps-register (name command)
  "Register an app.

NAME is listed in `kisaragi-apps'; COMMAND is run when selected.

This ensures no entries share the same COMMAND."
  (cl-pushnew `(,name . ,command)
              kisaragi-apps-list
              :test (lambda (x y) (eq (cdr x) (cdr y)))))

(defun kisaragi-apps ()
  "Show a list of registered Emacs-based applications."
  (interactive)
  (ivy-read
   "App: " kisaragi-apps-list
   :history 'kisaragi-apps--history
   :action (pcase-lambda (`(,_name . ,command))
             (call-interactively command))))

(provide 'kisaragi-apps)

;;; kisaragi-apps.el ends here

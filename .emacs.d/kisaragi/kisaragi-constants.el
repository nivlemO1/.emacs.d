;;; kisaragi-constants.el --- Constants to be require'd in other files -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'f)
(require 'dash)
(require 'xdg)
(require 'kisaragi-helpers)

(defconst k/android? (not (not (getenv "ANDROID_ROOT")))
  "Are we running on Android?

Use this to skip having to use (getenv \"ANDROID_ROOT\")
everywhere. Presumably variable access is faster than `getenv'.")

(defconst k/lisp-modes
  '(lisp-mode
    scheme-mode racket-mode fennel-mode emacs-lisp-mode
    clojure-mode hy-mode janet-mode)
  "Major modes that benefit from the same Lisp editing features.")

;;;; Leader keys

(defconst k/primary-leader "SPC" "My primary leader key.")

(defconst k/mode-leader ","
  "My leader key for mode specific bindings.")

(defconst k/global-mode-leader "C-c"
  "My leader key for mode specific bindings in non-command states.")

(defconst k/global-leader "C-x x" "Leader key for use everywhere.")

;;;; Paths

(defconst k/cloud/
  (cond ((k/system-is-p "MF-PC")
         "/run/media/kisaragi-hiu/Data/mega/")
        ((k/system-is-p "MF-manjaro")
         (f-join "~/cloud/"))
        (t nil))
  "Path to my primary cloud storage.")

(defconst k/cloud/scratchpad/ (-some-> k/cloud/
                                (f-join "scratchpad")
                                (concat "/"))
  "Scratchpad directory, storing non-text items.")

(defconst k/assets/
  (-some-> k/cloud/
    (f-join "assets")
    (concat "/"))
  "My assets directory.")

(defconst k/notes/
  (cond ((f-exists? "~/git/notes")
         (f-join "~/git/notes/"))
        ((getenv "ANDROID_ROOT")
         (file-truename "/sdcard/0.git/notes/"))
        ((eq system-type 'windows-nt)
         "d:/notes/")
        (t nil))
  "My directory for notes.")

(cl-assert k/notes/ nil "init.el assumes that k/notes/ exists")

(defconst k/ledger/
  (cond
   (k/notes/
    (f-join k/notes/ "ledger/"))
   (t
    ;; more likely to work when path separator is not /
    (f-join "~" "ledger/")))
  "My ledger directory.")

(defconst k/ledger-file
  (f-join k/ledger/ "journal.ledger")
  "My main ledger file.")

(defconst k/literature-notes-directory
  (when k/notes/
    (f-join k/notes/ "reflection"))
  "My directory for literature notes.")

(defconst k/bibliography-files
  (-some--> k/notes/
   (directory-files it :full "\\.bib$")
   (--map `(,(s-replace-regexp "\\.bib$" ".org" it) . ,it) it))
  "My bibliography files.

Format: ((org-file . bibtex-file) ...)")

(defconst k/school/
  (pcase (k/system-name)
    ((or "MF-PC" "MF-manjaro")
     (f-join k/cloud/ "school-works/"))
    (_ nil))
  "Path to my documents related to school / university.")

(defconst k/books/
  (cond (k/cloud/
         (f-join k/cloud/ "books/"))
        ((f-dir? "/storage/7D36-175C")
         (f-join "/storage/7D36-175C/Books/"))
        (t nil))
  "Directory for books.")

(defconst k/references/
  (cond (k/cloud/
         (f-join k/cloud/ "references/"))
        (t nil)))

(defconst k/recordings/
  (cond (k/cloud/
         (f-join k/cloud/ "recordings/"))
        ((file-exists-p "/sdcard/EasyVoiceRecorder")
         (file-truename "/sdcard/EasyVoiceRecorder/"))
        (t nil))
  "My directory for audio recordings.")

(defconst k/photos/
  (cond (k/cloud/
         (f-join k/cloud/ "photos/"))
        ((file-exists-p "/storage/7D36-175C/DCIM/Camera/")
         (f-join "/storage/7D36-175C/DCIM/Camera/"))
        (t nil))
  "My directory for photos.")

(defconst k/timetracking/
  (pcase (k/system-name)
    ((or "MF-PC" "Nexus 7")
     (f-join "~/git/" "timetracking"))
    ("MF-manjaro"
     (expand-file-name "~/timetracking/"))
    (_ "/sdcard/timetracking/"))
  "Path to where I keep my Canrylog file.")

(defconst k/core-repositories
  (-filter
   #'cdr
   `(("assets" . ,k/assets/)
     ("books" . ,k/books/)
     ("cloud" . ,k/cloud/)
     ("emacs.d" . ,(expand-file-name user-emacs-directory))
     ("ledger" . ,k/ledger/)
     ("notes" . ,k/notes/)
     ("photos" . ,k/photos/)
     ("references" . ,k/references/)
     ("recordings" . ,k/recordings/)
     ("school" . ,k/school/)
     ("scratchpad" . ,k/cloud/scratchpad/)
     ("timetracking" . ,k/timetracking/))))

(defun k/visit-core-repo ()
  "Select a core repository and visit it."
  (interactive)
  (-some--> (mapcar #'car k/core-repositories)
    (completing-read "Repository: " it nil t)
    (assoc it k/core-repositories)
    cdr
    find-file))

(provide 'kisaragi-constants)
;;; kisaragi-constants.el ends here

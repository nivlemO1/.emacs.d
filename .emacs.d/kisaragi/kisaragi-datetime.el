;;; kisaragi-datetime.el --- prototype of a new datetime library
;;; Commentary:
;; I'm not happy with anything, so here's some of my own code.

;;; Code:
(require 'names)
(require 'parse-time)
(require 'subr-x)
(define-namespace date-
(defun subtract (a b)
  "Subtract date A from B, returning signed number of days."
  (thread-last
      (time-subtract (parse-iso8601-time-string (format "%sT00:00:00Z" a))
                     (parse-iso8601-time-string (format "%sT00:00:00Z" b)))
    (format-time-string "%s")
    (string-to-number)
    (format-seconds "%d")
    (string-to-number)))

(defun difference (a b)
  "Return difference between dates A and B."
  (abs (subtract a b))))

(provide 'kisaragi-datetime)
;;; kisaragi-datetime.el ends here

;; Local Variables:
;; eval: (parinfer--switch-to-paren-mode)
;; End:

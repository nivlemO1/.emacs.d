;;; kisaragi-extra-functions.el --- where I dump extra functions  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Dump functions here when they don't yet fit in a package.
;;; Code:
(require 's)
(require 'f)
(require 'ht)
(require 'dash)
(require 'subr-x)
(require 'git)
(require 'cl-seq)
(require 'cl-lib)
(require 'kisaragi-helpers)
(require 'kisaragi-constants)

(require 'transient)

;; Listing albums by album duration
;; Documented in internal note 2021-08-03T02:02:08+0900 and https://kisaragi-hiu.com/sort-albums-by-duration
(defun k/song-duration (song-file)
  "Return duration of SONG-FILE in seconds."
  (with-temp-buffer
    (call-process
     "ffprobe" nil '(t nil) nil
     "-v" "quiet"
     "-print_format" "json"
     "-show_streams"
     song-file)
    (goto-char (point-min))
    (-some--> (json-parse-buffer
               :object-type 'alist)
      (map-elt it 'streams)
      (seq-find (lambda (elem)
                  (equal (map-elt elem 'codec_type)
                         "audio"))
                it)
      (map-elt it 'duration)
      string-to-number)))
(defun k/folder-duration (folder)
  "Return duration of all songs in FOLDER."
  (--> (directory-files folder t)
    (mapcar #'k/song-duration it)
    -non-nil
    (apply #'+ it)))
(defun k/list-albums (dir)
  "List music folders in DIR, providing a duration field for sort."
  (interactive (list (xdg-user-dir "MUSIC")))
  (let (folders)
    (dolist-with-progress-reporter (folder (f-directories dir))
        "Probing folders..."
      (push (cons folder (k/folder-duration folder)) folders))
    (with-current-buffer (get-buffer-create "*k/music folders*")
      (when (= 0 (buffer-size))
        (tabulated-list-mode)
        (setq tabulated-list-format
              (vector
               '("folder" 70 t)
               (list "duration" 20
                     (lambda (a b)
                       ;; An entry is (ID ["<folder>" "<duration>"]).
                       ;;
                       ;; <duration> looks like (label :key val :key val...)
                       ;; when props are given.
                       (< (-> (cadr a) (elt 1) cdr (plist-get :seconds))
                          (-> (cadr b) (elt 1) cdr (plist-get :seconds)))))))
        (tabulated-list-init-header))
      (dolist (folder folders)
        (push (list nil (vector (f-base (car folder))
                                (list (format-seconds "%.2h:%.2m:%.2s" (cdr folder))
                                      :seconds (cdr folder))))
              tabulated-list-entries))
      (revert-buffer))))

(defun k/roc-to-gregorian (date)
  "Convert ROC year in DATE to gregorian, returning it as YYYY-MM-DD.

DATE itself should be year-month-day separated by dashes."
  (-let (((roc-y month day) (s-split "-" date)))
    (format "%s-%s-%s"
            (+ 1911 (string-to-number roc-y))
            month
            day)))

(defun k/vtt-to-txt ()
  "In a buffer containing VTT subtitles, remove everything that isn't the txet."
  (interactive)
  (call-process-region
   (point-min)
   (point-max)
   "awk" t t nil
   ;; From https://stackoverflow.com/questions/56927772/
   "FNR<=4 || ($0 ~ /^$|-->|\\[|\\]|</){next} !a[$0]++"))

(defun k/new-literature-note (bibtex-file)
  "Create a new literature note for a video.

Write an org-bibtex entry into BIBTEX-FILE first. BIBTEX-FILE
should be relative to `k/notes/'. Interactively,
`k/bibliography-files' are offered for selection.

The bibtex-with-org part is actually implemented with
kisaragi-org-bibtex.el."
  (interactive
   (list (completing-read "Bibtex file: "
                          (--map (f-relative it k/notes/)
                                 (map-keys k/bibliography-files)))))
  ;; We `find-file' instead of using `k/with-file'.
  ;; This is because we have to save the bibtex file before creating
  ;; the note file for the entry.
  ;;
  ;; Alternatively, we can open the bibtex file with `k/with-file',
  ;; save the cursor position to a variable, close it, then open it
  ;; again to visit the entry.
  (find-file (f-join k/notes/ bibtex-file))
  (prog1 "To just before the first heading"
    (goto-char (point-min))
    (dotimes (_ 2) (outline-next-heading))
    (forward-char -1))
  (call-interactively #'kisaragi-org-bibtex-fast-url)
  (basic-save-buffer)
  (funcall bibtex-completion-edit-notes-function
           (org-entry-get nil "bibtex_id")))

(defun k/new-buffer ()
  "Create a new buffer and ask for a major mode."
  (interactive)
  (let ((buffer (generate-new-buffer "*new*")))
    (set-window-buffer nil buffer)
    (with-current-buffer buffer
      (let* ((predicate
              ;; Copied from `counsel-major' Not using that directly because
              ;; its prompt is hardcoded and working around that requires an
              ;; ugly hack utilizing
              ;; `ivy-set-prompt-text-properties-function'.
              (lambda (f)
                (and (commandp f) (string-match "-mode$" (symbol-name f))
                     (or (and (autoloadp (symbol-function f))
                              (let ((doc-split (help-split-fundoc (documentation f) f)))
                                ;; major mode starters have no arguments
                                (and doc-split (null (cdr (read (car doc-split)))))))
                         (null (help-function-arglist f))))))
             (mode (completing-read
                    "Major mode for new buffer: "
                    obarray predicate))
             (mode (if (string= mode "")
                       (default-value 'major-mode)
                     (intern mode))))
        (funcall mode)))))

(defun k/end-of-line (&optional n)
  "Move to end of line, making sure we don't jump to the next line.

Sometimes in insert state in Org C-e would jump to the next line.
I don't know why."
  (interactive "P")
  (end-of-line n)
  (when (bolp) (forward-char -1)))

(defun k/files-flatten-1-subdirectory (dir)
  "Move all files in subdirectories of DIR to DIR itself.

Subdirectory names are added onto each file to prevent conflict.

Example:

  dir/2015/0.png -> dir/2015 0.png
  dir/2015/my project/readme.md -> dir/2015 my project/readme.md
    (nested structures are preserved)
  dir/2015/1.png -> dir/2015 1.png
  dir/2016/1.png -> dir/2016 1.png"
  (cl-loop for f in (f-files dir nil t)
           collect
           (let ((newname (--> (s-replace-regexp
                                (format "^%s\\([[:digit:]]\\{8\\}\\)/\\(.*\\)$" dir)
                                (format "%s\\1 \\2" dir)
                                f))))
             ;; (unless (f-exists? (f-parent newname))
             ;;   (make-directory (f-parent newname)))
             (f-move f newname))))

(defun k/org-set-creation-info-from-commit-data ()
  "Set creation properties for heading at point according to a commit header.

Copy the commit header, place the cursor on the target Org heading,
and this will set the right properties."
  (interactive)
  (let-alist (->> (current-kill 0)
               ;; So we don't have to special case the first line
               (concat "commit-id:")
               ;; Convert "abc: def\nghi: jkl" to
               ;; '((abc . "def") (ghi . "jkl"))
               (s-split "\n")
               (--map (s-split-up-to ":" it 1))
               (-tree-map #'s-trim)
               (--map `(,(intern (car it)) . ,(cadr it))))
    (org-entry-put nil "commit" .commit-id)
    (org-entry-put nil "created" (kisaragi-timestamp-commands/commit-timestamp-to-iso8601 .AuthorDate))))

(defun k/open-project (id)
  "Open one of my personal project with ID."
  (interactive (list (thing-at-point 'word)))
  (let ((found (k/org-link-abbrev-project id)))
    (unless (s-prefix? "/tmp" found)
      (find-file found))))

(k/defun-string-or-region k/ellipsis ()
  "Return \"[…]\".

Interactively, replace region with \"[…]\"."
  "[…]")

(defun k/em-dash (start end)
  "Replace \"---\" in region (between START and END) with \"—\"."
  (interactive "r")
  (setf (buffer-substring start end)
        (replace-regexp-in-string "---" "—"
                                  (buffer-substring start end))))

(defun k/paste-html-table ()
  "Paste HTML table from clipboard in Org or Markdown."
  (interactive)
  (let ((target-format (cond
                        ((derived-mode-p 'org-mode) "org")
                        (t "markdown"))))
    (-> (with-temp-buffer
          (insert (current-kill 0))
          ;; Clean up table
          (let ((dom (libxml-parse-html-region (point-min) (point-max))))
            ;; Extract <pre> tags
            (setq dom (--tree-map-nodes (and (consp it)
                                             (eq 'pre (dom-tag it)))
                                        (dom-text it)
                                        dom))
            ;; Attempt to remove <hr> and <br>
            (--tree-map-nodes (and (consp it)
                                   (or (memq (dom-tag it)
                                             '(hr br))))
                              (dom-remove-node dom it)
                              dom)
            ;; Replace buffer content for pandoc (which supports XHTML just fine)
            (erase-buffer)
            (insert (shr-dom-to-xml dom)))
          (call-process-region (point-min) (point-max) "pandoc"
                               t '(t nil) nil
                               "--from" "html"
                               "--to" target-format)
          (buffer-string))
      insert)))

(defun k/paste-markdown ()
  "Convert clipboard from Markdown to Org or HTML, then paste."
  (interactive)
  (let ((target (cond
                 ((derived-mode-p 'org-mode) "org")
                 (t "html"))))
    (insert
     (with-temp-buffer
       (insert (current-kill 0))
       (call-process-region (point-min) (point-max) "pandoc"
                            t '(t nil) nil
                            "--from" "markdown"
                            "--to" target)
       (buffer-string)))))

(defun k/toggle-hs-outline ()
  "Toggle between HideShow and Outline."
  (interactive)
  (cond
   ((bound-and-true-p hs-minor-mode) (hs-minor-mode -1) (outline-minor-mode)
    (message "%s" "Using Outline"))
   ((bound-and-true-p outline-minor-mode) (outline-minor-mode -1) (hs-minor-mode)
    (message "%s" "Using HideShow"))
   (t
    (message "%s" "Not using Outline or HideShow"))))

(cl-defun k/org-heading-to-file (&key dir first-word visit?)
  "Write the current heading to a file under DIR.

DIR defaults to current directory (`default-directory').

The name of the created file is based on the heading, where:
- dashes and colons are first removed,
- then spaces are replaced with dashes,
- and everything is turned into lowercase (except the T in a timestamp).
For example, if the heading is \"2020-05-29T00:00:00+0800 my heading\",
the file name will be \"20200529T000000+0800-my-heading.org\".

When FIRST-WORD is non-nil, do the above but only to the first
WORD (up to the first space) of the heading. In other words,
using the same example, the file name will be
\"20200529T000000+0800.org\".

When VISIT? is non-nil, visit the new file after creating it.

Interactively:
- With a digit 1 in the numeric argument, prompt for DIR;
- with a digit 2 in the numeric argument, turn on FIRST-WORD;
- with a digit 3 in the numeric argument, visit the new file.

You can combine these, so for example: \\[universal-argument] 31
\\[k/org-heading-to-file] will prompt for DIR, create the
file, then visit it. The order of the digits does not matter."
  (interactive
   ;; This implements a "simple" way of mapping digits to options.
   (let ((args '()))
     (dolist (char (-some-> current-prefix-arg
                     prefix-numeric-value
                     number-to-string
                     string-to-list
                     -uniq))
       (pcase char
         (?1 (setq args
                   (plist-put
                    args :dir
                    (read-directory-name
                     "Put new file under directory: "))))
         (?2 (setq args
                   (plist-put
                    args :first-word t)))
         (?3 (setq args
                   (plist-put args :visit? t)))))
     args))
  (let* ((dir (or dir default-directory))
         (title (org-entry-get nil "ITEM"))
         (filename (->> (if first-word
                            (car (s-split " " title))
                          title)
                     (s-replace-regexp (rx (any "-/,:?\"!'\\")) "")
                     (s-replace-regexp " +" "-")
                     downcase
                     (s-replace-regexp (rx (group digit) "t" (group digit))
                                       "\\1T\\2")
                     (format "%s.org")))
         (path (f-join dir filename))
         (content (save-mark-and-excursion
                    (org-mark-subtree)
                    (buffer-substring-no-properties
                     (region-beginning)
                     (region-end)))))
    (k/with-file path t
      (insert content))
    (when visit?
      (find-file path))))

(defun k/pollen-code-to-org ()
  "Convert all Pollen inline code to Org syntax."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "◊code{\\(.*?\\)}" nil t)
        (replace-match "=\\1=")))))

(defun k/pollen-link-to-org ()
  "Convert all Pollen links in current buffer to Org links."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx "◊link[\"" (group (*? any)) "\"]"
                  "{" (group (*? any)) "}")
              nil t)
        (replace-match "[[\\1][\\2]]")))))

(defun k/pollen-highlight-block-to-org ()
  "Replace all ◊highlight['abc] blocks with Org source blocks."
  (interactive)
  (save-match-data
    (save-excursion
      (while (re-search-forward
              (rx "◊highlight['" (group (*? any)) "]")
              nil t)
        (let (lang beg end code-start code-end code)
          (setq lang (match-string 1)
                beg (line-beginning-position)
                code-start (1+ (point)))
          (forward-sexp)
          (setq end (point)
                code-end (1- (point))
                code (buffer-substring-no-properties code-start code-end))
          (delete-region beg end)
          (insert "#+begin_src " lang "\n"
                  (string-trim code "\n" "\n")
                  "\n#+end_src"))))))

(defun k/pollen-frontmatter-to-org ()
  "Convert Pollen ◊define-meta[foo] statements to Org #+foo keywords."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx "◊define-meta[" (group (*? any)) "]"
                  "{" (group (*? any)) "}")
              nil t)
        (replace-match "#+\\1: \\2")))))

(defun k/markdown-link-to-org ()
  "Convert all markdown links in current buffer to Org links."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx (not (any "[" "]"))
                  "[" (group (not (any "[")) (*? any)) "]"
                  "(" (group (*? any)) ")")
              nil t)
        (replace-match " [[\\2][\\1]]")))))

(defun k/insert-buffer-file-name (&optional trim)
  "Insert buffer file name at point.
When TRIM is non-nil, or with a \\[universal-argument], trim the
filename and only insert its basename."
  (interactive "P")
  (-when-let* ((f buffer-file-name))
    (when trim (setq f (f-base f)))
    (insert (format "%s" f))))

(define-generic-mode sxhkd-mode
  '(?#)
  '("alt" "Escape" "super" "bspc" "ctrl" "space" "shift")
  nil
  '("sxhkdrc")
  (list
   #'rainbow-delimiters-mode
   #'highlight-numbers-mode)
  "Simple mode for sxhkdrc files.

Modified from Reddit: https://www.reddit.com/r/emacs/comments/6fqj80/")

(defun k/add-current-path-to-load-path ()
  "Add current buffer's directory to `load-path'."
  (interactive)
  (-some->> (buffer-file-name)
    file-name-directory
    (add-to-list 'load-path)))

(defun k/insert-defstruct (name)
  "Insert a `cl-defstruct' with some preferences.

- SLOTS is passed as usual to `cl-defstruct'
- NAME is passed along with :copier set to nil, and :constructor
  set to NAME."
  (interactive "MName: ")
  (insert
   (format "%s"
           `(cl-defstruct (,name (:copier nil)
                                 (:constructor ,name)))))
  (backward-char))

(defun k/eww-termux-share ()
  "Share current eww URL through Termux."
  (interactive)
  (when (bound-and-true-p eww-data)
    (shell-command-to-string
     (format "printf '%s' | termux-share -a send"
             (plist-get eww-data :url)))))

(defun k/qr-encode (str &optional buf)
  "Encode STR as a QR code.

Return a new buffer or BUF with the code in it."
  (interactive "MString to encode: ")
  (let ((buffer (get-buffer-create (or buf "*QR Code*")))
        (format (if (display-graphic-p) "PNG" "UTF8"))
        (inhibit-read-only t))
    (with-current-buffer buffer
      (delete-region (point-min) (point-max)))
    (make-process
     :name "qrencode" :buffer buffer
     :command `("qrencode" ,str "-t" ,format "-o" "-")
     :coding 'no-conversion
     ;; seems only the filter function is able to move point to top
     :filter (lambda (process string)
               (with-current-buffer (process-buffer process)
                 (insert string)
                 (goto-char (point-min))
                 (set-marker (process-mark process) (point))))
     :sentinel (lambda (process change)
                 (when (string= change "finished\n")
                   (with-current-buffer (process-buffer process)
                     (cond ((string= format "PNG")
                            (image-mode)
                            (image-transform-fit-to-height))
                           (t ;(string= format "UTF8")
                            (text-mode)
                            (decode-coding-region (point-min) (point-max) 'utf-8)))))))
    (when (called-interactively-p 'interactive)
      (display-buffer buffer))
    buffer))

(defun k/qr-encode-buffer (in &optional out)
  "Encode buffer IN contents as a QR code.

Return a new buffer or OUT with the code in it."
  (with-current-buffer in
    (k/qr-encode (buffer-substring-no-properties (point-min) (point-max)) out)))

(defun k/restart-emacs ()
  "Restart Emacs daemon."
  (interactive)
  (start-process "systemd" nil "systemctl" "restart" "emacs" "--user"))

(defun k/reset-function (func)
  "Reset FUNC, removing all its advices.

Nullify the function definition of FUNC, then reload its source
file."
  (let ((file (file-name-base (symbol-file func))))
    (setf (symbol-function func) nil)
    (load file)))

;; modified from https://emacs.stackexchange.com/questions/17417
(defun stackexchange/eww-save-image (filename)
  "Save an image opened in an *eww* buffer to FILENAME."
  (interactive "G")
  (let ((image (get-text-property (point-min) 'display)))
    (f-write-bytes
     (plist-get (if (eq (car image) 'image) (cdr image)) :data)
     filename)))

(when (version< emacs-version "28")
  (require 'profiler)
  (define-minor-mode k/profiler-wide-mode
    "Minor mode to widen profiler reports."
    :global t
    (if k/profiler-wide-mode
        (setf (caar profiler-report-cpu-line-format) 80
              (caar profiler-report-memory-line-format) 80)
      (setf (caar profiler-report-cpu-line-format) 50
            (caar profiler-report-memory-line-format) 55))))

(k/defun-string-or-region k/decode-url ()
  "Decode STRING as a url."
  (org-link-decode string))

(k/defun-string-or-region k/encode-url ()
  "Encode STRING as a url."
  (url-encode-url string))

(define-minor-mode k/hide-cursor-mode
  "Minor mode to hide cursor."
  :global t
  (defun k/hide-cursor ()
    "Set cursor type to nil."
    (setq cursor-type nil))
  (if k/hide-cursor-mode
      (add-hook 'post-command-hook #'k/hide-cursor)
    (remove-hook 'post-command-hook #'k/hide-cursor)))

(defun k/show-string-in-buffer (str)
  "Show STR in a new buffer.

Pop to the new buffer with `helpful-switch-buffer-function'."
  (let ((buf (get-buffer-create "*string*")))
    (with-current-buffer buf
      (erase-buffer)
      (insert str)
      (goto-char (point-min)))
    (display-buffer buf)))

(defun k/magit-log-visit-changed-file ()
  "Visit a changed file of revision under point in `magit-log-mode'.

Uses `general-simulate-key', so `general-simulate-RET' will
become defined after invocation."
  (interactive)
  (general-simulate-key "RET")
  ;; visit the commit
  (general-simulate-RET)
  ;; move to first changed file
  (setf (point) (point-min))
  (search-forward " | " nil t)
  ;; open the revision
  (general-simulate-RET))

(defun k/visit-init-file ()
  "Visit `user-init-file'."
  (interactive)
  (find-file user-init-file))

(defun k/pop-to-buffer (buffer-or-name)
  "Display buffer specified by BUFFER-OR-NAME and select its window.

Display BUFFER-OR-NAME with `pop-to-buffer', unless the current
buffer is a Help or Helpful buffer, in which case display it in
the current window."
  (interactive (list (read-buffer "Pop to buffer: " (other-buffer))
                     (if current-prefix-arg t)))
  (funcall
   (if (or (s-contains? "*helpful" (buffer-name (current-buffer)))
           (string= "*Help*" (buffer-name (current-buffer))))
       #'pop-to-buffer-same-window
     #'pop-to-buffer)
   buffer-or-name))

(defun k/async-shell-command (command &optional buf)
  "Start COMMAND in a subprocess.

If BUF is non-nil, associate it with the subprocess."
  (interactive
   (list
    (read-shell-command "Async shell command: " nil nil
                        (let ((filename
                               (cond
                                (buffer-file-name)
                                ((eq major-mode 'dired-mode)
                                 (dired-get-filename nil t)))))
                          (and filename (file-relative-name filename))))))
  (start-process-shell-command "k/async-shell-command" buf command))

(defun k/echo-cangjie-if-han (&optional _dummy)
  "`message' out Cangjie encoding for character at point if it's a Han character."
  (interactive)
  (when-let ((cangjie (ignore-errors (cangjie-at-point))))
    (message "%s" cangjie)))
(define-minor-mode k/show-cangjie-mode
  "Minor mode to show Cangjie code in mode line."
  :global t
  (if k/show-cangjie-mode
      (add-hook 'post-command-hook #'k/echo-cangjie-if-han)
    (remove-hook 'post-command-hook #'k/echo-cangjie-if-han)))

(defun k/start-timer (seconds)
  "Ding after SECONDS.

Interactively, the prompt is evaluated so that \(* 60 30) can
mean 30 minutes."
  (interactive "XHow long? (seconds; evaluated): ")
  (start-process-shell-command
   "ding" nil
   (concat (format "sleep %s;" seconds)
           "play /usr/share/sounds/freedesktop/stereo/complete.oga")))

(defun rename-this-file (newname)
  "Rename this file to NEWNAME."
  (interactive
   (list (read-file-name "New name: " nil nil nil (f-filename buffer-file-name))))
  (if-let ((file (buffer-file-name)))
      (progn (rename-file file newname t)
             (set-visited-file-name newname t t))
    (user-error "Current buffer is not visiting a file")))

(defun delete-this-file (&optional trash)
  "Delete this file.

When called interactively, TRASH is t if no prefix argument is given.
With a prefix argument, TRASH is nil."
  (interactive)
  (when (and (called-interactively-p 'interactive)
             (not current-prefix-arg))
    (setq trash t))
  (if-let ((file (buffer-file-name)))
      (when (y-or-n-p "Delete this file? ")
        (delete-file file trash)
        (kill-buffer (current-buffer)))
    (user-error "Current buffer is not visiting a file")))

(defun k/git-commit (&optional message callback)
  "Commit whatever's staged with MESSAGE.

If MESSAGE is nil or empty (only whitespace), a generic message
is used.

Run CALLBACK with the process object when done."
  (interactive)
  (unless (and (stringp message)
               (not (string= "" (s-trim message))))
    (if-let ((name (buffer-file-name)))
        (setq message
              (format "update %s %s"
                      (file-name-base name)
                      (k/date-iso8601)))
      (setq message
            (format "update %s"
                    (k/date-iso8601)))))
  (async-start-process
   "git commit" "git" callback
   "commit" "-m" message))

(defun k/git-reset (&optional callback)
  "Run \"git reset HEAD\" asynchronously.

Run CALLBACK with the process object when done."
  (async-start-process
   "git reset" "git" callback
   "reset" "HEAD" "--"))

(defun k/git-add (files &optional callback)
  "Run \"git add -- FILES\" asynchronously.

Run CALLBACK with the process object when done."
  (let ((files (if (listp files) files (list files))))
    (apply #'async-start-process
           "git add" "git" callback
           "add" "--" files)))

(defun k/git-stage-everything ()
  "Stage everything."
  (interactive)
  (let ((default-directory (projectile-project-root)))
    (call-process "git" nil nil nil "add" ".")))

(defun k/git-commit-everything ()
  "Stage everything then run `magit-commit'."
  (interactive)
  (require 'magit)
  (k/git-stage-everything)
  (magit-commit-create))

(defun k/git-commit-this-file-with-message (message &optional callback)
  "Stage and commit this file with a one-line MESSAGE.

A generic message is used if MESSAGE is empty (only whitespace);
see `k/git-commit'.

Run CALLBACK with the process object when done."
  (interactive "MMessage: ")
  (require 'magit)
  (when (magit-merge-in-progress-p)
    (error "Merge in progress"))
  (when-let ((file (file-relative-name (buffer-file-name))))
    (k/git-reset
     (lambda (_)
       (k/git-add
        file
        (lambda (_)
          (k/git-commit message callback)))))))

(defun k/git-commit-this-file ()
  "Stage and commit this file, using Magit's editor to edit the commit message."
  (interactive)
  (shell-command-to-string "git reset HEAD --")
  (shell-command-to-string
   (concat "git add -- " (file-relative-name (buffer-file-name))))
  ;; use Magit's editor
  (require 'magit)
  (magit-commit-create))

(defun k/git-push (&optional callback)
  "Run git push asynchronously.

Run CALLBACK with the process object when done."
  (async-start-process
   "git push" "git" callback "push"))

(defun k/git-commit-this-and-push (&optional message)
  "Commit this file to Git and push asynchronously.

MESSAGE is used as the commit message; if nil or empty, a generic
message is used."
  (k/git-commit-this-file-with-message
   message
   (lambda (_)
     (k/git-push))))

(cl-defun k/new-graphics (&optional
                          template
                          (new-file-dir (f-join k/cloud/ "references")))
  "Like `k/new-scratchpad', but:

- put the file under \"references\" instead by default
- ask for the filename by default
- don't create a corresponding note for it.

TEMPLATE: the template file to copy from. Interactively, ask to
select a file under the \"templates\" directory under
`k/assets/'.

NEW-FILE-DIR: where the new file should be in. Asked with a
\\[universal-argument]."
  (interactive
   (list nil (f-join k/cloud/ "references")))
  (unless template
    (setq template
          (-some--> (f-join k/assets/ "templates")
            (directory-files it nil (rx bol (not ".")))
            (completing-read "Template: " it nil t)
            (f-expand it (f-join k/assets/ "templates")))))
  (let* ((new-file (--> (f-filename template)
                     (read-string "New file name: " it)
                     (f-join new-file-dir it))))
    (insert (k/format-link (k/org-apply-link-abbrev new-file)))
    ;; And copy the template over
    (copy-file template new-file)
    (counsel-locate-action-extern new-file)))

(cl-defun k/new-scratchpad (&optional
                            template
                            (new-file-dir k/cloud/scratchpad/))
  "Create a new file in a scratchpad from TEMPLATE.

Put the new file under NEW-FILE-DIR (default `k/cloud/scratchpad/').

The new file is named with current time and inherits the
extention of TEMPLATE, for example with a Krita template it looks
like this: \"20201201T000000+0900.kra\".

Interactively, ask to select a file under the \"templates\"
directory under `k/assets/' for TEMPLATE."
  (interactive
   (list nil k/cloud/scratchpad/))
  ;; We have to do this here as org-capture doesn't go through the
  ;; `interactive' form.
  (unless template
    (setq template
          (-some--> (f-join k/assets/ "templates")
            (directory-files it nil (rx bol (not ".")))
            (completing-read "Template: " it nil t)
            (f-expand it (f-join k/assets/ "templates")))))
  (let* ((now (current-time))
         (new-file (f-join new-file-dir
                           (format (format-time-string "%Y%m%dT%H%M%S%z.%%s" now)
                                   (f-ext template)))))
    ;; Create a note entry for the new file
    (kisaragi-diary/new-entry now)
    (insert (format "\n[[%s]]" (k/org-apply-link-abbrev new-file)))
    ;; And copy the template over
    (copy-file template new-file)
    (counsel-locate-action-extern new-file)))

(cl-defun k/new-stray-note (&key title dir)
  "Add a new concept note / stray note with TITLE.

When DIR is non-nil, put the new file under a subdirectory called
DIR in `k/notes/'. Otherwise, put it in `k/notes/' directly."
  ;; (interactive "MTitle: ")
  (unless title
    (setq title (read-string "Title: ")))
  (find-file
   (--> (kisaragi-notes//title-to-slug title)
     (s-replace "_" "-" it)
     (format "%s.org" it)
     (f-join k/notes/ (or dir "") it)))
  (insert "#+title: " title "\n"
          "#+created: " (k/date-iso8601) "\n"))

(defun k/insert-note ()
  "Insert \"<timestamp>: \", suitable for use as editing notes."
  (interactive)
  (let ((surround (cond
                   ((derived-mode-p 'outline-mode) "/")
                   ((derived-mode-p 'markdown-mode) "*")
                   (t ""))))
    (insert (format "%s%s: %s"
                    surround
                    (k/date-iso8601)
                    surround))
    (backward-char (length surround))
    (when (fboundp #'evil-insert-state)
      (evil-insert-state))))

(defun k/open-this-file-with-nvim ()
  "Open current file with nvim."
  (interactive)
  (let ((file-name (buffer-file-name))
        (terminal (or (getenv "TERMINAL")
                      "konsole")))
    (unless file-name
      (error "Current buffer not visiting a file"))
    (start-process "nvim" nil
                   terminal "-e" "nvim" file-name)))

(defun k/open-this-file-with-system ()
  "Open current file with system default."
  (interactive)
  (let ((path
         (if (derived-mode-p 'dired-mode
                             'eshell-mode)
             (f-expand default-directory)
           (buffer-file-name))))
    (unless path
      (error "Current buffer not visiting a file"))
    (consult-file-externally path)))

(defun k/open-current-file-as-sudo ()
  "Re-open current file with sudo.

From URL `https://www.reddit.com/r/emacs/comments/9sp7hh/show_me_your_functions/e8qibkq/'."
  (interactive)
  (when-let ((file-name (or (buffer-file-name)
                            (and (derived-mode-p 'dired-mode)
                                 default-directory))))
    (find-alternate-file (concat "/sudo::" file-name))))

(defun k/pollen-add-meta (meta)
  "Add a Pollen define-meta entry named META."
  (interactive "MMeta: ")
  (forward-line)
  (insert "◊define-meta[" meta "]{}\n")
  (backward-char 2))

(provide 'kisaragi-extra-functions)
;;; kisaragi-extra-functions.el ends here

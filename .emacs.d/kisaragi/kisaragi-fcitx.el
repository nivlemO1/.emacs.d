;;; kisaragi-fcitx.el --- My functions for interacting with fcitx -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(require 'dash)
(require 'dbus)
(require 'f)
(require 'kisaragi-helpers)

;;;; {get, set, cycle} current IM; retrieve information about IM
(defun k/fcitx-im-info (im)
  "Get other information about IM."
  ;; (k/fcitx-im-full "rime") ; => ("Rime" "rime" "zh" t)
  (-find
   (pcase-lambda (`(,_ ,name ,_ ,_)) (string= im name))
   (dbus-get-property :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "IMList")))

(defun k/fcitx-list-enabled-im ()
  "Return list of enabled fcitx input methods."
  ;; Each IM looks like (name id language enabled)
  (->> (dbus-get-property :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "IMList")
       (--filter (nth 3 it))
       (--map (nth 1 it))))

(defun k/fcitx-cycle-current-im (&optional n)
  "Cycle to the Nth (default 1) next input method."
  (interactive "P")
  (k/fcitx-set-current-im
   (k/next (k/fcitx-list-enabled-im)
                  (k/fcitx-get-current-im)
                  n)))

(defun k/fcitx-get-current-im ()
  "Get the current fcitx input method."
  (dbus-get-property :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "CurrentIM"))

(defun k/fcitx-set-current-im (im)
  "Set current fcitx input method to IM."
  (interactive (list (completing-read "Input Method: "
                                      (k/fcitx-list-enabled-im))))
  (dbus-call-method :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "SetCurrentIM" im))

(defun k/fcitx-activate ()
  "Activate fcitx."
  (dbus-call-method :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "ActivateIM"))

(defun k/fcitx-deactivate ()
  "Deactivate fcitx."
  (dbus-call-method :session "org.fcitx.Fcitx" "/inputmethod" "org.fcitx.Fcitx.InputMethod" "InactivateIM"))

(provide 'kisaragi-fcitx)

;;; kisaragi-fcitx.el ends here

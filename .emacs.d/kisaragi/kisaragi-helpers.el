;;; kisaragi-helpers.el --- various helper functions and macros  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Helper functions used in `kisaragi-settings.el'.
;;; Code:
(require 'cl-lib)
(require 'subr-x)
(require 'map)
(require 'dash)
(require 'parse-time)

;; for `gnus-copy-sequence'
(require 'gnus-range)

(defun k/mark-category (seq category)
  "Mark SEQ as being in CATEGORY.

Return a collection (as defined by `completing-read') that is the
same as passing SEQ to `completing-read' except with category
information attached.

See Info node `(elisp) Programmed Completion' for the official
documentation on this system.

Category information is conveyed by having collection functions
respond metadata when Emacs asks for it. This has begun being
utilized by packages such as Marginalia to display different
information for different types of completion entries, or by
Embark to create what are in effect context menus."
  (lambda (str pred action)
    (pcase action
      ('metadata
       `(metadata (category . ,category)))
      (_
       (all-completions str seq pred)))))

(let ((font-list (font-family-list)))
  (defun k/first-available-font (fonts)
    "Return the first font in FONTS that is available on this system."
    ;; If font-list turns out to be nil (perhaps because this
    ;; definition was run when graphics haven't been initialized),
    ;; try to set it again.
    (unless font-list (setq font-list (font-family-list)))
    (when font-list
      (car (-intersection fonts font-list)))))

(defun k/listof (pred)
  "Is the argument a list of things satisfying PRED?

Return a function that asks the above question."
  (lambda (x)
    (and (listp x)
         (seq-every-p pred x))))

(defun k/consof (pred)
  "Return a function.

That function returns t when its argument is a cons and both
elements satisfy PRED."
  (lambda (x)
    (and (consp pred)
         (funcall pred (car x))
         (funcall pred (cdr x)))))

(defun k/toggle-multiple (toggle-alist)
  "Toggle many things, making sure they're all on or off at once.

TOGGLE-ALIST looks like '((VARIABLE . TOGGLE-FUNCTION) ...),
where:

- VARIABLE determines if the thing is on (non-nil) or off (nil);
- TOGGLE-FUNCTION is the function that actually toggles it."
  ;; Grab the actual current values
  (setq toggle-alist
        (map-apply
         (lambda (key val)
           (cons (symbol-value key) val))
         toggle-alist))
  ;; If everything is nil or everything is non-nil
  (if (or (seq-every-p #'not (map-keys toggle-alist))
          (seq-every-p #'identity (map-keys toggle-alist)))
      ;; Call all toggle functions
      (thread-last toggle-alist
        map-values
        (mapc #'call-interactively))
    ;; Some are non-nil: only toggle those to bring everything to nil
    (thread-last toggle-alist
      (seq-filter #'car)
      map-values
      (mapc #'call-interactively))))

(defun k/all-months-last-year-to-now ()
  "Return a list of months from January last year to this month.

Months are returned as strings of yyyy-mm.

For example, when it is 2021-03-01, this returns the list
'(\"2020-01\" \"2020-02\" ... \"2021-02\" \"2021-03\")."
  (let* ((now (calendar-current-date))
         (current-month (car now))
         (current-year (elt now 2)))
    (append (cl-loop for month in (-iota 12 1)
                     collect (format "%s-%02d" (1- current-year) month))
            (cl-loop for month in (-iota current-month 1)
                     collect (format "%s-%02d" current-year month)))))

(defmacro k/search-in-directory (regexp directory &rest body)
  "Search for REGEXP in DIRECTORY, and run BODY on the results.

Results are put in a buffer, in the form MATCHED FILE:LINE:MATCH STRING.

REGEXP is passed directly to `ag', so it needs to be in a form
that `ag' understands."
  (declare (indent 2))
  `(with-temp-buffer
     (save-excursion
       (call-process "ag" nil t nil "--nocolor"
                     ,regexp
                     ,directory)
       ,@body)))

(defun k/find-file-command (path)
  "Return an interactive command that visits PATH."
  (lambda ()
    (interactive)
    (find-file path)))

;; The default values are probably evaluated twice, therefore needing
;; two quotes. I think.
(cl-defmacro k/map-update& (map key form &key (getter #''map-elt) (setter #''map-put))
  "Update the value at KEY in MAP with FORM.

FORM has access to the current associated value of KEY as the
variable `it'. Its return value is the new value.

Return the new map.

SETTER and GETTER, defaulted to `map-put' and `map-elt', are used
to actually get and set the value. They need to have the same
interface as `map-put' and `map-elt', i.e. (MAP KEY VAL) and (MAP
KEY). This allows for plist support in Emacs 26."
  `(let* ((map (if (hash-table-p ,map) ; Try to make sure we don't mutate the argument
                   (copy-hash-table ,map)
                 ;; Complete copy of a list
                 (gnus-copy-sequence ,map)))
          (it (funcall ,getter map ,key))
          (new-val (progn ,form)))
     (if (eq ,setter #'map-put)
         (funcall #'map-put map ,key new-val)
       (funcall ,setter map ,key new-val))
     map))

(cl-defmacro k/plist-update& (plist key form)
  "Update value associated with KEY in PLIST with FUNC.

Like `k/map-update&', but only supports plists."
  `(k/map-update& ,plist ,key ,form
                  :getter #'plist-get :setter #'plist-put))

(cl-defun k/map-update (map key func &key (getter #'map-elt) (setter #'map-put))
  "Update the value at KEY in MAP with FORM.

FUNC is passed a single argument, the current associated value of
KEY. Its return value is the new value.

Return the new map.

SETTER and GETTER, defaulted to `map-put' and `map-elt', are used
to actually get and set the value. They need to have the same
interface as `map-put' and `map-elt', i.e. (MAP KEY VAL) and (MAP
KEY). This allows for plist support in Emacs 26."
  (let* ((map (if (hash-table-p map)
                  (copy-hash-table map)
                (gnus-copy-sequence map)))
         (new-val (funcall func (funcall getter map key))))
    ;; `map-put' is a macro, handle it specially
    (if (eq setter #'map-put)
        (map-put map key new-val)
      (funcall setter map key new-val))
    map))

;; Try: (let ((map '((a . "a") (b . "b")))) (k/map-update map 'a #'upcase))

(defun k/pgrep-boolean (name)
  "Does a process matching NAME exist?"
  (= 0 (call-process "pgrep" nil nil nil name)))

(defun k/symbol-append (symbol string)
  "Return a symbol that is named SYMBOL appended with STRING."
  (intern (concat (symbol-name symbol) string)))

(defmacro k/set-mode-local (mode &rest args)
  "Like `setq-mode-local', except:

- MODE is evaluated. This allows using variables.
- If MODE is a list, set local values for every MODE."
  (declare (indent 1))
  ;; this ends up emitting error at compile time if MODE is not quoted
  (let ((mode (cadr mode)))
    (cond
     ((and (listp mode)
           (-all? #'symbolp mode))
      `(progn ,@(mapcar (lambda (x) `(k/set-mode-local ',x ,@args)) mode)))
     ((symbolp mode)
      (when args
        (let (i ll bl sl tmp sym val)
          (setq i 0)
          (while args
            (setq tmp  (make-symbol (format "tmp%d" i))
                  i    (1+ i)
                  sym  (car args)
                  val  (cadr args)
                  ll   (cons (list tmp val) ll)
                  bl   (cons `(cons ',sym ,tmp) bl)
                  sl   (cons `(set (make-local-variable ',sym) ,tmp) sl)
                  args (cddr args)))
          `(let* ,(nreverse ll)
             ;; Save mode bindings
             (mode-local-bind (list ,@bl) '(mode-variable-flag t) ',mode)
             ;; Assign to local variables in all existing buffers in MODE
             (mode-local-map-mode-buffers #'(lambda () ,@sl) ',mode)
             ;; Return the last value
             ,tmp)))))))

(defun k/average (&rest numbers)
  "Return average of NUMBERS.

NUMBERS is `-flatten'ed before use."
  (setq numbers (-flatten numbers))
  (/ (apply #'+ numbers) (length numbers)))

;; Modified from https://nullprogram.com/blog/2010/09/29/
;; Now that we have lexical binding, I don't think we need `lexical-let'?
(defun k/expose (function)
  "Return an interactive version of FUNCTION."
  (lambda ()
    (interactive)
    (funcall function)))

;; (cl-typep '(a b c) '(k/listof symbol)) => t
;; (cl-typep '(a b c) '(k/listof string)) => nil
(cl-deftype k/listof (type)
  `(satisfies (lambda (thing) (and (cl-typep thing 'list)
                                   ;; list of t?
                                   (car-safe
                                    (cl-remove-duplicates
                                     (--map (cl-typep it ',type) thing)))))))

(defmacro k/with-file (file save &rest body)
  "Run BODY with FILE opened as the current buffer.

When SAVE is non-nil, write the buffer to FILE afterwards.

This reuses the buffer that visits FILE, including all potential
modifications in it. Cursor position, mark, and narrowing are
reset, however."
  (declare (indent 2))
  (if save
      `(with-current-buffer (find-file-noselect ,file)
         (prog1 (save-mark-and-excursion
                  (save-restriction
                    (goto-char (point-min))
                    ,@body))
           (basic-save-buffer)))
    `(with-temp-buffer
       (insert-file-contents-literally ,file)
       (decode-coding-region (point-min) (point-max) 'utf-8)
       (goto-char (point-min))
       ,@body)))

(defmacro k/with-temp-dir (&rest body)
  "Create a temporary directory, evaluate BODY there, then clean it up.

`default-directory' is set to the temporary directory."
  (declare (indent 0))
  (let ((temp-dir (f-slash
                   (f-join temporary-file-directory
                           (format "emacs%s" (random 10000))))))
    `(unwind-protect
         (progn (f-mkdir ,temp-dir)
                (let ((default-directory ,temp-dir))
                  ,@body))
       (when (f-exists? ,temp-dir)
         (f-delete ,temp-dir t)))))

(defun k/install-system (name message command)
  "Use COMMAND to install a system package.

Emits MESSAGE before and after installation; NAME names the
process."
  (message "%s" message)
  (set-process-sentinel
   (start-process-shell-command
    name nil command)
   (lambda (_process change)
     (when (string= change "finished\n")
       (message "%s...done" message)))))

(defun k/next (sequence val &optional n testfn)
  "Return element in SEQUENCE next to ELT.

N defaults to 1, which means the element at VAL's index plus 1.

TESTFN is passed directly to `seq-position'."
  (unless (numberp n) (setq n 1))
  (seq-elt sequence (% (+ n (or (seq-position sequence val testfn)
                                0))
                       (seq-length sequence))))

(defun k/call-if-bound (function &rest args)
  "Call FUNCTION with ARGS, but only when FUNCTION is bound.

Return whatever the call returns."
  (when (fboundp function)
    (apply function args)))

(defmacro k/each (sequence &rest body)
  "Apply BODY to each element of SEQUENCE, presumably for side effects.

BODY is what goes into implemented as `(lambda (it) ,@body)'.

Reimplementation of `--each', made to not need an external dependency."
  (declare (indent 1))
  `(mapc (lambda (it) ,@body) ,sequence))

(defun k/iso8601-force-timezone (timestamp &optional zone)
  "Take ISO 8601 TIMESTAMP and return the same moment in ZONE."
  (format-time-string "%FT%T%z" (parse-iso8601-time-string timestamp) zone))

(defun k/iso8601-p (thing)
  "Is THING a ISO 8601 timestamp?"
  (ignore-errors
    (parse-iso8601-time-string thing)))

(defun k/contains-iso8601-p (string)
  "Does STRING contain a ISO 8601 timestamp?

This will return a lot of false positives, but speed is more
important than correctness here because the main use of this
function is telling Tramp to not treat a timestamp as a protocal
and attempt to connect to it."
  (not (not (string-match-p
             "....-..-..T..:..:.."
             string))))

(defun k/iso8601->rfc822 (iso8601-date)
  "Convert ISO8601-DATE to RFC 822 format.

More accurately, convert an ISO 8601 date string to one that
`parse-time-string' understands.

Example:
\"2019-08-09T01:40:17+0900\" -> \"Fri,  9 Aug 2019 01:40:17 +0900\""
  (format-time-string
   "%a, %e %b %Y %H:%M:%S %z"
   (parse-iso8601-time-string iso8601-date)))

(defun k/date-iso8601 (&optional date insert?)
  "Get current time in ISO 8601 format.

If DATE is not nil, display it in ISO 8601 format instead of now.

Interactively, or when INSERT? is non-nil, insert the timestamp
at point."
  (interactive (list nil :insert))
  (let ((timestamp (format-time-string "%FT%T%z" date)))
    (if insert?
        (insert timestamp)
      timestamp)))

(defun k/today (&optional n insert?)
  "Return today's date, taking `org-extend-today-until' into account.

Return values look like \"2020-01-23\".

If N is non-nil, return N days from today. For example, N = 1
means tomorrow, and N = -1 means yesterday.

Interactively, or when INSERT? is non-nil, insert the date at
point."
  (interactive (list (and current-prefix-arg
                          (prefix-numeric-value current-prefix-arg))
                     :insert))
  (unless n
    (setq n 0))
  (let ((str (format-time-string
              "%Y-%m-%d"
              (time-add
               (* n 86400)
               (time-since
                ;; if it's bound and it's a number, do the same thing `org-today' does
                (or (and (boundp 'org-extend-today-until)
                         (numberp org-extend-today-until)
                         (* 3600 org-extend-today-until))
                    ;; otherwise just return (now - 0) = now.
                    0))))))
    (if insert?
        (insert str)
      str)))

(defmacro with-eval-and-load (feature &rest body)
  "Require FEATURE; if success, run BODY."
  (declare (indent 1) (debug t))
  `(when (require ,feature nil :noerror)
     ,@body))

(defmacro k/time (&rest body)
  "Run BODY, then return amount of time it ran for."
  `(let ((start (current-time)))
     ,@body
     (float-time (time-subtract (current-time) start))))

(defmacro k/benchmark (n &rest forms)
  "Run each of FORMS N times, then return average amount of time each ran for.

Extra: When AVERAGE is nil (it defaults to t), return the total
elapsed time, not averaged.
\(fn N &key (AVERAGE t) &rest FORMS)"
  (declare (indent 1))
  (let* ((unique-nil (make-symbol "-nil-"))
         ;; This dance with an uninterned symbol allows us to
         ;; distinguish when it's not supplied and when a nil is
         ;; given.
         (average-arg (-some--> forms
                        (seq-take it 2)
                        (-replace nil unique-nil it)
                        (plist-get it :average)))
         (average? (not (eq average-arg unique-nil)))
         (forms
          (if average-arg (cddr forms) forms))
         (elem-vars (mapcar (lambda (_elem) (gensym)) forms))
         (dotimes (cl-find-if #'fboundp
                              '(dotimes-with-progress-reporter dotimes))))
    `(let (,@elem-vars)
       (,dotimes (_ ,n) "Benchmarking"
                 ,@(cl-mapcar
                    (lambda (elem elem-var) `(push (k/time ,elem) ,elem-var))
                    forms
                    elem-vars))
       (list ,@(mapcar
                (lambda (results-var)
                  (if average?
                      `(k/average ,results-var)
                    `(apply #'+ ,results-var)))
                elem-vars)))))

(defmacro k/benchmark-compiled (n &rest forms)
  "Run each of FORMS N times, then return average amount of time each ran for."
  (declare (indent 1))
  ;; uninterned symbol here to hide it from FORMS
  (let ((prep (make-symbol "--prep--"))
        (count (make-symbol "--count--")))
    `(let ((,prep (make-progress-reporter
                   ;; I guess if the string ends with a paren progress
                   ;; reporter doesn't add the ellipsis?
                   "Benchmarking (compiled)..."
                   ;; 1-, because length is 1-indexed but prep is 0-indexed
                   0 (1- ,(* n (length forms)))))
           (,count 0))
       (prog1 (mapcar
               #'k/average
               (list
                ,@(--map
                   ;; doing this because we cannot access our own
                   ;; progress reporter from within `benchmark-run-compiled'
                   `(let (results)
                      (dotimes (i ,n results)
                        (push
                         (car
                          (benchmark-run-compiled 1 ,it))
                         results)
                        (progress-reporter-update
                         ,prep (setq ,count (1+ ,count)))))
                   forms)))
         (progress-reporter-done ,prep)))))

(defmacro k/turn-off-minor-mode (minor-mode)
  "Define a function that will call MINOR-MODE with -1.

MINOR-MODE should NOT be a quoted."
  (let ((name (intern (format "k/turn-off-%s" minor-mode))))
    `(progn
       (defun ,name ()
         ,(format "Turn off `%s'.

Defined by `k/turn-off-minor-mode'."
                  minor-mode)
         (funcall #',minor-mode -1))
       ',name)))

(defmacro k/set-x-to-val (x val)
  "Define a function to set X to VAL.

Return the symbol to the function."
  (let ((name (intern (format "k/set-%s-to-%s" x val))))
    `(progn
       (defun ,name ()
         ,(format "Set `%s' to %s.\n\nDefined by `k/set-x-to-val'."
                  x val)
         (setq ,x ,val))
       ;; in case `defun' decides to no longer return the symbol
       ',name)))

(defun k/system-name ()
  "Return name and type of this system."
  (pcase (system-name)
    ("localhost"
     (if (executable-find "getprop")
         (string-trim
          (shell-command-to-string
           "getprop ro.product.model"))
       "localhost"))
    (name name)))

(defun k/system-is-p (name)
  "Is NAME equal to the hostname of this machine?"
  (string= (k/system-name) name))

;; TODO: somehow integrate this with evil
(cl-defmacro k/defun-string-or-region
    (name &optional no-replace docstring &rest body)
  "Define NAME as a function with BODY that works on both strings and regions.

By default, the resulting function replaces the active region with the
return value. If NO-REPLACE is non-nil, don't do that.

DOCSTRING is passed to `defun'.
`string' is bound in BODY representing the string input or region content.

Based on http://ergoemacs.org/emacs/elisp_command_working_on_string_or_region.html."
  (declare (indent 2)
           (doc-string 3))
  `(defun ,name (string &optional start end)
     ,(concat docstring "

If STRING is nil, the region between START and END is used as STRING."
              (if no-replace
                  ""
                "\nInteractively, replace the current paragraph or active region."))
     (interactive
      ;; when run interactively, STRING is always nil
      (if (use-region-p)
          (list nil (region-beginning) (region-end))
        (let ((bounds (bounds-of-thing-at-point 'paragraph)))
          (list nil (car bounds) (cdr bounds)))))
     (if string
         ,@body
       (save-excursion
         (let ((result
                (let ((string
                       (buffer-substring-no-properties start end)))
                  ,@body)))
           ,(if no-replace
                '(progn result)
              '(progn
                 (delete-region start end)
                 (goto-char start)
                 (insert result))))))))

;; (defmacro setq-mode (mode &rest clauses)
;;   "Shorthand for setting CLAUSES with setq in MODE's hook."
;;   `(add-hook ',(intern-soft (concat (symbol-name mode) "-hook")) (lambda () (setq ,@clauses))))
;; (defmacro k/function-multify (func arity)
;;   "Return a new version of FUNC that takes a list for each of its arguments.
;;
;; ARITY has to be manually passed.  I couldn't get `func-arity' in here for some reason."
;;   (let ((args (mapcar
;;                (lambda (arg)
;;                  (intern (concat "a" (number-to-string arg))))
;;                ;; I couldn't get func-arity in here for some reason, would complain func is invalid.
;;                (number-sequence 1 arity))))
;;     `(lambda ,args
;;        ;; Check if each argument is a list, if not, make it one.
;;        (let ,(mapcar (lambda (i) `(,i (if (listp ,i)
;;                                           ,i
;;                                         (list ,i))))
;;                      args)
;;          (cl-mapcar ,func ,@args)))))
;; (defun remove-hook* (hooks functions)
;;   "Like `remove-hook', but can also take multiple HOOKS and FUNCTIONS as lists."
;;   (funcall (k/function-multify #'remove-hook 2) hooks functions))
;; (defun add-hook* (hooks functions)
;;   "Like `add-hook', but can also take multiple HOOKS and FUNCTIONS as lists."
;;   (funcall (k/function-multify #'add-hook 2) hooks functions))

;; From deferred.el (L94)
(defmacro lambda/self (args &rest body)
  "Like `lambda', except a symbol `self' is bound to the function itself.

ARGS and BODY are as in `lambda'.

`interactive' and `declare' do not work."
  (declare (indent defun)
           (doc-string 2))
  (let ((argsyms (mapcar (lambda (_) (cl-gensym)) args)))
    `(lambda ,argsyms
       (let (self)
         (setq self (lambda ,args ,@body))
         (funcall self ,@argsyms)))))

(provide 'kisaragi-helpers)
;;; kisaragi-helpers.el ends here

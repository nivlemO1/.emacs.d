;;; kisaragi-hugo.el --- Hugo editing commands -*- lexical-binding: t -*-

;;; Commentary:

;; Convenience commands for managing my Hugo website.

;;; Code:

(require 'dash)
(require 'f)
(require 's)

(require 'org)
(require 'projectile)
(require 'kisaragi-helpers)

(defun k/hugo-jump-language (&optional create)
  "Jump to another variant of the current page.

Assumes that variants are stored as
project-root/[variant]/site-path. For example, these files are
variants of each other:

  /en/about.org
  /zh-tw/about.org

these are also variants of each other:

  /en/blog/abc.md
  /ja/blog/abc.md

When CREATE is non-nil (with a \\[universal-argument]), create a
variant of the current page instead of only looking for existing
variants."
  (interactive "P")
  (cl-block nil
    (when create
      (let ((variant (read-string "Variant: ")))
        (find-file (apply #'f-join
                          (projectile-project-root)
                          variant
                          (->> (f-relative (buffer-file-name)
                                           (projectile-project-root))
                            f-split
                            cdr))))
      (cl-return))
    (let ((variants (let* ((rel (->> (f-relative (buffer-file-name)
                                                 (projectile-project-root))
                                  f-split
                                  cdr)))
                      (with-temp-buffer
                        (call-process "find" nil '(t nil) nil
                                      (projectile-project-root)
                                      "-path" (concat "*" (apply #'f-join rel)))
                        (s-split "\n" (buffer-string) t)))))
      (pcase (length variants)
        (1 nil)
        (2 (find-file (car (remove (buffer-file-name) variants))))
        (_ (find-file
            (completing-read "Open variant: " (k/mark-category variants 'file))))))))

(defun k/hugo-blog-page-to-standalone (&optional new-id)
  "Turn a blog page into a standalone page named NEW-ID.

Only applicable to my site."
  (interactive "MNew page ID: ")
  (pcase-let ((`(,_ ,date ,old-id)
               (s-match (rx "content/blog/"
                            (group (= 4 digit) "-"
                                   (= 2 digit) "-"
                                   (= 2 digit)) "-"
                            (group (regexp ".*?"))
                            ".org")
                        (buffer-file-name))))
    (unless new-id (setq new-id old-id))
    (rename-file (buffer-file-name)
                 (f-join (projectile-project-root) "content" (concat new-id ".org")))
    ;; `find-file' because I expect you to manually place it in the
    ;; right place
    (find-file
     (f-join (projectile-project-root) "static" "_redirects"))
    (save-excursion
      (goto-char (point-min))
      (search-forward "# Blog -> content note migration" nil t)
      (insert (format "\n/blog/%s-%s /%s" date old-id new-id)))))

(defun k/hugo-snapshot-page ()
  "Add the current version of a page to the old version archive.

Only applicable to my site."
  (interactive)
  (let* ((default-directory (projectile-project-root))
         (id (f-base (buffer-file-name)))
         (keywords (org-collect-keywords '("created" "updated")))
         (version
          ;; updated > created
          (or (cadr (assoc "UPDATED" keywords))
              (cadr (assoc "CREATED" keywords))))
         (filename (replace-regexp-in-string "[:-]" "" version))
         (dir (f-join "content" "archive" id))
         (orig-content (buffer-string)))
    (make-directory dir t)
    (with-temp-file (f-join dir (concat filename ".org"))
      (insert orig-content)
      (goto-char (point-min))
      (forward-line)
      (insert "#+archive: " id "\n"))))

(defun k/hugo-new-term (term &optional taxonomy)
  "Create a new _index.org for TERM.

TAXONOMY is \"tags\" by default. Interactively, prompts for
another taxonomy with a \\[universal-argument]."
  (interactive
   (list (read-string "Term: ")
         (and current-prefix-arg
              (read-string "Taxonomy: "))))
  (unless taxonomy (setq taxonomy "tags"))
  (let ((default-directory (projectile-project-root)))
    (let ((dir (f-join "content" taxonomy term)))
      (make-directory dir t)
      (find-file (f-join dir "_index.org"))
      (insert "#+title: " term))))

(provide 'kisaragi-hugo)

;;; kisaragi-hugo.el ends here

;;; kisaragi-ledger.el --- extra ledger-mode commands  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Extra commands for Ledger.
;;; Code:

(require 'kisaragi-helpers)
(require 'kisaragi-units)
(require 'kisaragi-constants)
(require 'ledger-mode)
(require 's)
(require 'f)
(require 'dash)
(require 'projectile)

(defun k/ledger-monthly-subtotal (query)
  "Report the monthly subtotals for QUERY."
  (interactive (list (completing-read "Query: " (ledger-accounts-list))))
  (let ((subtotals (cl-loop for period in (k/all-months-last-year-to-now)
                            collect
                            (with-temp-buffer
                              (apply #'call-process
                                     "ledger" nil '(t nil) nil
                                     "--date-format" "%F"
                                     "--period" period
                                     "-f" ledger-master-file
                                     "emacs"
                                     (split-string-and-unquote query))
                              (if (= 0 (buffer-size))
                                  (cons period "0 TWD")
                                (--> (cl-loop
                                      for (_file _ _datetime _ _payee
                                                 (_ _account amount _))
                                      in (read (buffer-string))
                                      collect amount)
                                  (k/add-with-units it "TWD")
                                  (cons period it)))))))
    (with-current-buffer (get-buffer-create
                          (format "*k/ledger monthly subtotal for %s*" query))
      (erase-buffer)
      (cl-loop for (period . subtotal) in subtotals
               do (insert period ": " subtotal "\n"))
      (display-buffer (current-buffer)))))

(defun kisaragi-ledger-format-all-files ()
  "Format all ledger files (file name ending in .ledger) in current project."
  (interactive)
  (->> (projectile-project-files (projectile-project-root))
       (--filter (equal (file-name-extension it) "ledger"))
       (--map
        (k/with-file it t
          (k/ledger-format-buffer)))))

(cl-macrolet
    ((define-write-func
       (singular plural)
       `(defun ,(intern (concat "k/ledger-write-" plural)) (in-file out-file)
          ,(format "Write existing %s from IN-FILE into OUT-FILE." plural)
          (interactive
           `(,k/ledger-file
             ,(f-join k/ledger/ ,(format "%s.ledger" plural))))
          (with-temp-file out-file
            (shell-command (format ,(format "ledger %s -f %%s" plural) in-file)
                           (current-buffer))
            (setf (point) (point-min))
            (while (search-forward "\n" nil t)
              (save-excursion
                (forward-line -1)
                (insert ,(format "%s " singular))))))))
  (define-write-func "account" "accounts")
  (define-write-func "commodity" "commodities"))

(let ((allowed-value-func
       (lambda (prop)
         (when (member (downcase prop)
                       '("from" "to"))
           (cons ":ETC" (ledger-accounts-list))))))
  (define-minor-mode kisaragi-org-ledger-account-complete-mode ()
    "Minor mode adding completion for ledger accounts to some Org properties."
    :global nil
    (if kisaragi-org-ledger-account-complete-mode
        (add-hook 'org-property-allowed-value-functions
                  allowed-value-func
                  nil t)
      (remove-hook 'org-property-allowed-value-functions allowed-value-func t))))

(defun k/ledger-format-buffer ()
  "Format the current ledger buffer."
  (interactive)
  (ledger-post-align-postings (point-min) (point-max)))

(defun k/ledger-insert-account ()
  "Insert a ledger account."
  (interactive)
  (insert (completing-read "Account: " (ledger-accounts-list))))

(defun k/ledger-add-simple-transaction (payee from to amount date)
  "Insert a simple ledger transaction at end of buffer.

Interactively, if a prefix argument is given, prompt for DATE
instead of using today.

DATE PAYEE
    TO  AMOUNT
    FROM"
  (interactive
   (progn
     (require 'org) ; for org-parse-time-string and org-read-date
     (list
      (s-trim (completing-read "Payee: " (ledger-payees-in-buffer)))
      (completing-read "From account: " (ledger-accounts-list))
      (completing-read "To account: " (ledger-accounts-list))
      (s-trim (read-string "Amount: " "$"))
      (or (and current-prefix-arg
               (ledger-format-date
                (apply #'encode-time
                       (org-parse-time-string (org-read-date)))))
          (ledger-format-date)))))
  (save-excursion
    (setf (point) (point-max))
    (insert (format
             "
%s %s
    %s  %s
    %s"
             date payee to amount from))
    (ledger-post-align-dwim)))

(defun k/ledger-report-this-file ()
  "`ledger-report' with `ledger-master-file' set to nil."
  (interactive)
  (let ((ledger-master-file nil))
    (call-interactively #'ledger-report)))

(defun k/ledger-visit-latest ()
  "Visit the latest ledger file.

My ledger files are split into each month's; this visits the latest one.

More accurately speaking, this visits the file written exactly -2 chars from
`point-max'."
  (interactive)
  (find-file
   (with-temp-buffer
     (insert-file-contents k/ledger-file)
     (goto-char (- (point-max) 2))
     (concat (file-name-directory k/ledger-file) (thing-at-point 'filename)))))

(defun k/ledger-read-transaction ()
  "Read the text of a transaction.

This provides completion for existing payees, and fills in the
entire date."
  (let ((date (ledger-format-date
               (or ledger-add-transaction-last-date
                   (current-time)))))
    (read-string "Transaction: " (s-lex-format "${date} ")
                 'ledger-minibuffer-history)))

(defun k/ledger-xact-ensure-date-format ()
  "Make date of xact under point use - or / per `ledger-default-date-format'.

Only understands YYYY-MM-DD or YYYY/MM/DD as the original date."
  (save-excursion
    (ledger-navigate-beginning-of-xact)
    (pcase-let* ((`(,start . ,end) (bounds-of-thing-at-point 'sexp))
                 (timestamp (buffer-substring-no-properties start end))
                 (`(,year ,month ,day)
                  (mapcar #'string-to-number
                          (list (substring timestamp 0 4)
                                (substring timestamp 5 7)
                                (substring timestamp 8 10)))))
      (setf (buffer-substring start end)
            (ledger-format-date
             (encode-time 0 0 0 day month year 0))))))

(defun k/ledger-add-transaction (transaction-text &optional insert-at-point file)
  "Like `ledger-add-transaction', but in the context of FILE.

Use ledger xact TRANSACTION-TEXT to add a transaction to the buffer, in the
context of FILE.

If INSERT-AT-POINT is non-nil insert the transaction there,
otherwise call ledger-xact-find-slot to insert it at the
correct chronological place in the buffer."
  ;; changes from vanilla `ledger-add-transaction':
  ;; - explicitly require ledger-xact
  ;; - run ledger xact in the context of FILE (default `k/ledger-file')
  ;;   instead of the current buffer
  ;;   - HACK: Except when on Android, where the current buffer is
  ;;     always used. This is because recent builds on Android for
  ;;     some reason wouldn't resolve relative paths in include
  ;;     statements when the file is passed in through stdin, which
  ;;     `ledger-exec-ledger' does.
  ;; - remove a useless progn
  ;; - use k/ledger-read-transaction, which does not for some
  ;;   reason remove the day
  (interactive (list (k/ledger-read-transaction) nil
                     (if k/android?
                         (buffer-file-name)
                       k/ledger-file)))
  (require 'ledger-xact)
  (let* ((args (with-temp-buffer
                 (insert transaction-text)
                 (eshell-parse-arguments (point-min) (point-max))))
         (ledger-buf (get-buffer-create (symbol-name (gensym)))))
    (prog1
        (progn
          (with-current-buffer ledger-buf
            (insert-file-contents file))
          (unless insert-at-point
            (let* ((date (car args))
                   (parsed-date (ledger-parse-iso-date date)))
              (setq ledger-add-transaction-last-date parsed-date)
              (push-mark)
              (ledger-xact-find-slot (or parsed-date date))))
          (if (> (length args) 1)
              (save-excursion
                (insert
                 (with-temp-buffer
                   (apply #'ledger-exec-ledger ledger-buf (current-buffer) "xact"
                          (mapcar 'eval args))
                   (goto-char (point-min))
                   (ledger-post-align-postings (point-min) (point-max))
                   (k/ledger-xact-ensure-date-format)
                   (buffer-string))
                 "\n"))
            (insert (car args) " \n\n")
            (end-of-line -1)))
      (kill-buffer ledger-buf))))

(defun k/ledger-rename-account (account new &rest files)
  "Rename ACCOUNT to NEW in FILES."
  (interactive
   (list (completing-read "Rename account: " (ledger-accounts-list))
         (read-string "New name: ")
         (directory-files "." nil "ledger$")))
  (dolist (f files)
    (k/with-file f t
      (goto-char (point-min))
      (while (re-search-forward
              ;; c.f. `ledger-account-regexp'
              (format "\\(%s\\)" (regexp-quote account))
              nil t)
        (replace-match new t t nil 1)))))

(k/defun-string-or-region k/ledger-extract-accounts ()
  "Extract ledger account statements from STRING."
  (->> (s-split "\n" string t)
       (--filter (s-prefix? " " it))
       (--map (s-replace-regexp " +.\$.*$" "" it))
       (--map (s-replace-regexp "^ +" "account " it))
       (-sort (lambda (x y) (string-lessp x y)))
       (-uniq)
       (s-join "\n")))

(provide 'kisaragi-ledger)
;;; kisaragi-ledger.el ends here

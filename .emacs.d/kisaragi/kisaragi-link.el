;;; kisaragi-link.el --- Inserting links  -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; One function to insert links to files, notes, images, etc. in my notes.
;;
;;; Code:

(require 's)

(defun k/insert-link/double-key ()
  "Insert a link after calling this command twice with the same key.

For example, to insert a link after typing \"[[\":

  (general-def :keymaps 'org-mode-map :states 'insert
    \"[\" #'k/insert-link/double-key)"
  (interactive)
  (cond
   ((and (eq last-command #'k/insert-link/double-key)
         (eq (char-before) last-command-event))
    (delete-char -1)
    (call-interactively #'k/insert-link))
   (t (insert (string last-command-event)))))

(defun k/insert-link (target &optional desc)
  "Insert a link to TARGET, taking major mode and TARGET contents into account.

- If text is selected, turn it into a link to TARGET.
- If DESC is given, insert it (unless some text is already selected).
- Insert URLs directly.
- In Org mode, abbreviate TARGET per `org-link-abbrev-alist'.
- Offer completion from Org-roam's cache.

To quickly insert a link with a given description, write the
description first, select it, then run this command."
  (interactive (k/org-read-file/roam "Insert link to note: "))
  (when (region-active-p)
    (setq desc (buffer-substring-no-properties (region-beginning) (region-end)))
    (delete-active-region))
  (message "%s, %s" target desc)
  (insert (k/format-link target desc)))

(defun k/format-link (target &optional desc)
  "Format TARGET and DESC as a link according to the major mode.

`org-link-abbrev-alist' is applied when in Org mode unless it has a protocol."
  (let ((url? (s-matches? "https?://" target))
        ;; - Allow `k/org-apply-link-abbrev' to work even on file links
        ;; - Allow f.el to work in general
        (target (s-replace-regexp "^file://" "" target)))
    (cond
     ((derived-mode-p 'org-mode)
      ;; Don't apply link-abbrev if TARGET is https or http
      (unless url?
        (setq target (k/org-apply-link-abbrev target)))
      (if (and (not desc) url?)
          target
        (org-link-make-string target desc)))
     ((derived-mode-p 'markdown-mode)
      (cond ((and (not desc) url?)
             ;; plain URL links
             (format "<%s>" target))
            ((not desc)
             ;; Just a URL
             (format "[%s](%s)"
                     (f-filename target)
                     (f-relative target)))
            (t
             (format "[%s](%s)" desc target))))
     ;; No common way to insert descriptions
     (t target))))

(provide 'kisaragi-link)
;;; kisaragi-link.el ends here

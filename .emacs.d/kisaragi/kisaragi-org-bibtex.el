;;; kisaragi-org-bibtex.el --- Convert between Bibtex and Org  -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; I'm not happy with `org-bibtex', so here's my own version.
;;
;;; Code:

(require 's)
(require 'org)
(require 'calendar)
(require 'dash)
(require 'bibtex)

(require 'kisaragi-helpers)

(defvar kisaragi-org-bibtex-property-indent "\t"
  "What to indent bibtex properties with.")

(defvar kisaragi-org-bibtex-ignored-properties nil
  "List of properties not exported to Bibtex.")

(defvar kisaragi-org-bibtex-ignored-tags nil
  "List of tags to exclude from exporting to \"keywords\" in Bibtex.")

(defvar kisaragi-org-bibtex-ignored-todo-keywords nil
  "List of todo keywords to exclude when exporting.
When this is t, don't export any todo keyword.")

(defvar kisaragi-org-bibtex-file nil
  "Default BibTeX file to export to.")

(defun kisaragi-org-bibtex--type-translate (type-before)
  "Convert TYPE-BEFORE to something more suitable as a type."
  ;; FIXME: obviously crude and doesn't cover most cases
  (s-replace-regexp "s$" "" type-before))

(defun kisaragi-org-bibtex--get-type ()
  "Get type of current heading."
  (-some-> (or (org-entry-get nil "BIBTEX_TYPE" t)
               (org-entry-get nil "CATEGORY" t))
    (kisaragi-org-bibtex--type-translate)))

(defun kisaragi-org-bibtex--get-id ()
  "Get ID of current heading."
  (org-entry-get nil "BIBTEX_ID"))

(defun kisaragi-org-bibtex--get-todo ()
  "Get the todo keyword of the current heading."
  (let ((keyword (org-entry-get nil "TODO")))
    ;; disable this if ignored-todo-keywords is t
    (and (not (eq t kisaragi-org-bibtex-ignored-todo-keywords))
         (not (member keyword (mapcar #'upcase kisaragi-org-bibtex-ignored-todo-keywords)))
         keyword)))

(defun kisaragi-org-bibtex--get-title ()
  "Get title of current heading.

Use the TITLE property if it is set, the headline itself if not."
  (or (org-entry-get nil "TITLE")
      (org-entry-get nil "ITEM")))

(defun kisaragi-org-bibtex--get-keywords ()
  "Get keywords of current heading.

If the KEYWORDS property is present, use it. It'll be returned as
a string, which will go directly into the bibtex file.

Otherwise, use the heading's tags as keywords; inheritance is
controlled by `org-use-tag-inheritance'."
  (or (org-entry-get nil "KEYWORDS")
      (-non-nil
       (cons
        (kisaragi-org-bibtex--get-todo)
        (-difference (org-get-tags)
                     kisaragi-org-bibtex-ignored-tags)))))

(defun kisaragi-org-bibtex--get-abstract ()
  "Get the abstract of current heading."
  (org-entry-get nil "ABSTRACT"))

(defun kisaragi-org-bibtex--property-to-bibtex (&rest kv)
  "Return a bibtex property string mapping KEY to VALUE.

KV is (KEY VALUE ...), like `setq'.

Special processing is done if KEY is `date': the year, month, and
day elements are returned as separate bibtex properties. Month is
also translated into bibtex references (like `mon', `feb').

KEY can be specified as symbols, keywords, or strings -- they're
all converted to strings in the output.

If VALUE is a symbol, don't quote it in the output. If it is nil,
don't show that pair in the output."
  (unless (cl-evenp (length kv))
    (error "Keys and values must be in pairs"))
  (->> (cl-loop for pair in (-partition 2 kv)
                collect
                (catch 'ret
                  (-let (((key value) pair))
                    (unless value
                      (throw 'ret nil))
                    ;; convert keywords to strings
                    (when (keywordp key)
                      (setq key (-> (symbol-name key)
                                    (substring 1))))
                    ;; force it to be lowercase
                    (setq key (downcase (format "%s" key)))
                    ;; if "month" is an abbreviated month string, set
                    ;; it to a symbol
                    (when (and (equal key "month")
                               (stringp value)
                               (cl-position
                                (capitalize value)
                                calendar-month-abbrev-array
                                :test #'equal))
                      (setq value (intern value)))
                    (when (equal key "date")
                      (-let* (((_ _ _ day month year)
                               (or
                                (ignore-errors
                                  (decode-time
                                   (parse-iso8601-time-string value)))
                                (org-parse-time-string value)))
                              (month
                               ;; Convert numerical month to Bibtex
                               ;; abbreviated reference
                               (-some-> month
                                 (calendar-month-name :abbrev)
                                 downcase
                                 intern)))
                        (throw 'ret (kisaragi-org-bibtex--property-to-bibtex
                                     :day day :month month :year year))))
                    (cl-typecase value
                      (symbol
                       (format "%s%s = %s" kisaragi-org-bibtex-property-indent key value))
                      (list
                       (format "%s%s = {%s}" kisaragi-org-bibtex-property-indent key
                               ;; '(a b c) -> "a, b, c"
                               (->> value
                                    (--map (format "{%s}" it))
                                    (s-join ", "))))
                      (t
                       (format "%s%s = {%s}" kisaragi-org-bibtex-property-indent key value))))))
       -non-nil
       (-sort #'string<)
       (s-join ",\n")))

(defun kisaragi-org-bibtex--heading-to-bibtex ()
  "Turn heading under point into bibtex."
  (let* ((org-properties (--> (org-entry-properties)
                              ;; don't convert special properties
                              (--remove
                               (member (car it)
                                       ;; or "BIBTEX_ID"
                                       (append '("BIBTEX_ID" "CATEGORY"
                                                 "ID"
                                                 "TITLE" "ABSTRACT")
                                               kisaragi-org-bibtex-ignored-properties
                                               org-special-properties))
                               it)
                              (map-into it 'plist)))
         (properties (apply #'kisaragi-org-bibtex--property-to-bibtex
                            :title (kisaragi-org-bibtex--get-title)
                            :abstract (kisaragi-org-bibtex--get-abstract)
                            :keywords (kisaragi-org-bibtex--get-keywords)
                            org-properties))
         (type (kisaragi-org-bibtex--get-type))
         (id (kisaragi-org-bibtex--get-id)))
    (s-lex-format "@${type}{${id},
${properties}
}")))

(defun kisaragi-org-bibtex--bibtex-parse-entry ()
  "Run `bibtex-parse-entry', and process the result a bit."
  (cl-loop for (k . v) in (bibtex-parse-entry t)
           ;; use symbol keys
           collect (cons (intern k) v)))

(defun kisaragi-org-bibtex--bibtex-to-heading (entry)
  "Turn bibtex parsed ENTRY into Org.

ENTRY should have symbol keys; you must process the output of
`bibtex-parse-entry' yourself to ensure that."
  (with-temp-buffer
    (insert "* " (alist-get 'title entry) "\n")
    (org-entry-put nil "bibtex_id" (alist-get '=key= entry))
    (org-entry-put nil "bibtex_type" (alist-get '=type= entry))
    (cl-loop for (k . v) in entry
             unless (member k '(=type= =key= title))
             do (org-entry-put nil
                               (symbol-name k)
                               (s-replace "\n" " " v)))
    (buffer-string)))

;;;###autoload
(defun kisaragi-org-bibtex-export (&optional file)
  "Export headings in the current buffer as Bibtex to FILE.

If FILE is nil, use `kisaragi-org-bibtex-file'. Interactively,
use `kisaragi-org-bibtex-file' by default, but prompt for another
file with a \\[universal-argument].

Headings need to have the BIBTEX_ID property in order to be exported.

They should also have a CATEGORY property (perhaps inherited),
which is used as their Bibtex entry type.

The headline itself is used as the title, except if the TITLE
property is set, that is used instead.

Dates (in the DATE property) are automatically converted:

  :DATE:  2019-03-24

in Org becomes

  date = {24},
  month = mar,
  year = {2019},

in Bibtex.

Other non-special properties are exported as-is."
  (interactive
   (list (and current-prefix-arg
              (read-file-name "Export as Bibtex to: "))))
  (unless file
    (setq file kisaragi-org-bibtex-file))
  (let ((case-fold-search t)
        ret)
    (save-excursion
      (goto-char (point-max))
      ;; Search from bottom, so that the last entry in the file is the
      ;; last entry in `ret' without having to use `nreverse'.
      (while (re-search-backward "^:bibtex_id:" nil t)
        (push (kisaragi-org-bibtex--heading-to-bibtex) ret)))
    (with-temp-file file
      (dolist (entry ret)
        (insert entry "\n\n")))))

;;;###autoload
(defun kisaragi-org-bibtex-import (file &optional all)
  "Import Bibtex entries from FILE.

Insert them into the end of the current buffer.

Entries that are already in the current file are skipped, unless
ALL is non-nil. Interactively, this is triggered with a
\\[universal-argument]."
  (interactive (list (read-file-name "Import from Bibtex file: ")
                     current-prefix-arg))
  (save-excursion
    (let (entries existing-ids)
      (k/with-file file nil
        (goto-char (point-max))
        ;; Backwards so that top entry is first in list; also positions
        ;; the cursor at the right place (on the "@") for
        ;; `bibtex-parse-entry'.
        (while (re-search-backward "^@" nil t)
          (save-excursion
            (push (kisaragi-org-bibtex--bibtex-parse-entry) entries))))
      (unless all
        (goto-char (point-min))
        (let ((case-fold-search t))
          (while (re-search-forward ":bibtex_id:" nil t)
            (push (org-entry-get nil "BIBTEX_ID") existing-ids))))
      (goto-char (point-max))
      (dolist (entry entries)
        (if entry
            (unless (member (alist-get '=key= entry) existing-ids)
              (insert (kisaragi-org-bibtex--bibtex-to-heading entry)))
          (message "An entry wasn't parsed successfully!"))))))

;;;###autoload
(defun kisaragi-org-bibtex-generate-id (&optional pom)
  "Set bibtex ID of heading at POM according to its date and author.

Heading at POM defaults to the current heading."
  (interactive)
  (unless (org-entry-get pom "bibtex_id")
    (-let* ((author (-some->> (org-entry-get pom "author")
                      (s-replace-all '((" " . "")))
                      downcase))
            (date (-some->> (or (org-entry-get pom "date")
                                (org-entry-get pom "year"))
                    (s-replace "--" "–")
                    (s-replace "-" "")
                    (s-replace "–" "--")
                    ;; this should handle ISO 8601 timestamps
                    (s-replace-regexp "T[[:digit:]].*" "")))
            (new-id (concat author (or date (read-string "Date is not given. Suffix: ")))))
      (org-entry-put pom "bibtex_id" new-id))))

;;;###autoload
(define-minor-mode kisaragi-org-bibtex-auto-export-mode
  "Minor mode for automatically exporting Org to BibTeX entries."
  :global nil
  (if kisaragi-org-bibtex-auto-export-mode
      (add-hook 'after-save-hook #'kisaragi-org-bibtex-export nil t)
    (remove-hook 'after-save-hook #'kisaragi-org-bibtex-export t)))

(defun kisaragi-org-bibtex-fast--props (props)
  "Prompt the user for each property in PROPS, then generate a bibtex ID."
  ;; TODO: use bibtex-field-alist
  (dolist (prop props)
    (let ((value (org-read-property-value prop)))
      (unless (string-empty-p value)
        ;; Special handling of `date': if it's a year range, or if
        ;; it's 4 numbers, set "year" instead
        (when (and (equal prop "date")
                   (or (s-contains? "--" value)
                       (s-matches? "^[[:digit:]]\\{4\\}$" value)))
          (setq prop "year"))
        (org-entry-put nil prop value))))
  (kisaragi-org-bibtex-generate-id))

;;;###autoload
(defun kisaragi-org-bibtex-fast-inbook ()
  "Fill in a chapter of a book."
  (interactive)
  (let ((inherit (lambda (prop)
                   (org-entry-put nil prop (org-entry-get nil prop t)))))
    (funcall inherit "year")
    (funcall inherit "author")
    (org-entry-delete nil "bibtex_id")
    (org-entry-put nil "bibtex_type" "inbook")
    (org-entry-put nil "bibtex_id"
                   (concat (org-entry-get nil "bibtex_id" t)
                           (->> (org-entry-get nil "ITEM")
                                (s-split " ")
                                car
                                downcase)))))

;;;###autoload
(defun kisaragi-org-bibtex-fast-book ()
  "Fill in a Book entry."
  (interactive)
  (kisaragi-org-bibtex-fast--props
   '("author" "ISBN" "date" "publisher" "translator")))

;;;###autoload
(defun kisaragi-org-bibtex-fast-video ()
  "Fill in a Video entry."
  (interactive)
  (kisaragi-org-bibtex-fast--props
   '("url" "author" "date" "organization")))

;;;###autoload
(defun kisaragi-org-bibtex-fast-article ()
  "Fill in an Article entry."
  (interactive)
  (kisaragi-org-bibtex-fast--props
   '("url" "author" "date")))

;;;###autoload
(defun kisaragi-org-bibtex-fast-url (url &optional timestamp)
  "Insert URL as an Article entry.

Set the CREATED property to TIMESTAMP. If TIMESTAMP is nil, use
the current time (formatted as a full ISO 8601 timestamp).

Interactively, prompt for URL. With a \\[universal-argument],
read a timestamp from the same prompt: enter them as
\"<url>  <timestamp>\", with two spaces between, in that order."
  (interactive
   (if current-prefix-arg
       (->> (read-string
             "URL and timestamp (in that order, with two spaces between): ")
         (s-split "  ")
         (-take 2))
     (list (read-string "URL: ") nil)))
  (let* ((current-buffer-read-only buffer-read-only)
         (dest-buf (current-buffer))
         (dest-point (point))
         (url (s-trim url))
         (timestamp (if timestamp (s-trim timestamp) (k/date-iso8601))))
    (message "Retrieving title from %s..." url)
    (when-let ((buf (url-retrieve-synchronously url :silent)))
      (with-current-buffer buf
        (decode-coding-region (point-min) (point-max) 'utf-8)
        (goto-char (point-min))
        ;; move point to after the headers
        (eww-parse-headers)
        (let* ((inhibit-read-only t)
               (dom (libxml-parse-html-region (point) (point-max)))
               (title
                (or
                 ;; <meta name="title" content="...">
                 ;; Youtube puts "- YouTube" in <title>, so I want
                 ;; this first.
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal (dom-attr it 'name) "title") it)
                   (dom-attr it 'content))
                 ;; Open Graph
                 ;; Also often without the site suffix
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal (dom-attr it 'property) "og:title") it)
                   (dom-attr it 'content))
                 ;; <title>
                 (dom-texts (car (dom-by-tag dom 'title)))))
               (author
                (or
                 ;; <meta name="author" content="...">
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal "author" (dom-attr it 'name)) it)
                   (dom-attr it 'content))
                 ;; <meta name="cXenseParse:author" content="...">
                 ;; nippon.com uses this
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal "cXenseParse:author" (dom-attr it 'name)) it)
                   (dom-attr it 'content))
                 ;; WordPress entry header
                 ;; <body><div class="entry-meta">...<a rel="author" href=...>
                 (-some--> (dom-by-class dom "entry-meta")
                   (dom-by-tag it 'a)
                   (--first (equal "author" (dom-attr it 'rel)) it)
                   (dom-text it))
                 ;; YouTube stores this in a <div> in <body>...
                 ;; And yes, it's in a <span> for some reason
                 (-some--> (dom-by-tag dom 'span)
                   (--first (equal "author" (dom-attr it 'itemprop)) it)
                   dom-children
                   (--first (equal "name" (dom-attr it 'itemprop)) it)
                   (dom-attr it 'content))))
               (publishdate
                (or
                 ;; Open Graph
                 ;; <meta property="article:published_time"
                 ;;       content="2019-08-29T09:54:00-04:00" />
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal "article:published_time"
                                   (dom-attr it 'property))
                            it)
                   (dom-attr it 'content))
                 ;; WordPress entry header
                 ;; <body><div class="entry-meta">...<time class="entry-date"...>
                 (-some--> (dom-by-class dom "entry-meta")
                   (dom-by-tag it 'time)
                   (--first (equal "entry-date" (dom-attr it 'class)) it)
                   (dom-text it))
                 ;; YouTube
                 (-some--> (dom-by-tag dom 'meta)
                   (--first (equal "datePublished" (dom-attr it 'itemprop)) it)
                   (dom-attr it 'content)))))
          (with-current-buffer dest-buf
            (goto-char dest-point)
            ;; Ensure we start inserting from an empty line
            (unless (eq ?\n (char-before))
              (insert "\n"))
            (insert (format "%s %s\n"
                            (make-string (1+ (org-current-level)) ?*)
                            title))
            (org-entry-put nil "url" url)
            (org-entry-put nil "author" author)
            (org-entry-put nil "date" publishdate)
            (k/org-set-heading-timestamp timestamp)
            (message "Retrieving title from %s...done" url)
            (kisaragi-org-bibtex-fast-article)))))))

;;;###autoload
(defun kisaragi-org-bibtex-entries-fill-info (&optional level)
  "Fill out bibtex ID for entries at LEVEL."
  (interactive (list (read-number "Level: ")))
  (save-excursion
    (while (<= level (org-current-level))
      (org-up-heading-all 1))
    (org-map-tree
     (lambda ()
       (when (and (= level (org-current-level))
                  (not (org-entry-get nil "bibtex_id")))
         (let ((header-line-format (org-entry-get nil "ITEM")))
           (kisaragi-org-bibtex-fast-book)))))))

;;;###autoload
(defun kisaragi-org-bibtex-entries-fill-bibtex-id ()
  "Fill out bibtex ID for entries."
  (interactive)
  (org-map-tree
   (lambda ()
     (when (and (or (org-entry-get nil "date")
                    (org-entry-get nil "year"))
                (org-entry-get nil "author"))
       (kisaragi-org-bibtex-generate-id)))))

(provide 'kisaragi-org-bibtex)
;;; kisaragi-org-bibtex.el ends here

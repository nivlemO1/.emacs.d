;;; kisaragi-org-highlight-links-in-title.el --- Highlight links in #+title -*- lexical-binding: t -*-

;;; Commentary:

;; Org title highlighting overwrites links, so a link in the #+title
;; keyword won't be highlighted. Try:
;;
;;   #+title: [[https://gnu.org][GNU]]
;;
;; will not be highlighted as a link, even though the link still works.
;;
;; This is because:
;;
;; (a) Org keywords and blocks are highlighted using the same logic
;;     (as blocks are implemented with #+begin and #+end keywords), and
;; (b) You want blocks to not be highlighted like ordinary Org syntax.
;;
;; I don't know what's the best solution inside of Org, but we can
;; still achieve this one thing by adding overlays afterwards. That's
;; what this package does.

;;; Code:

(require 'org)

(defun kisaragi-org-highlight-links-in-title ()
  "Fix links in the #+title keyword not being highlighted."
  (save-excursion
    (save-match-data
      ;; - We only process the first #+title.
      ;; - Between correctness and acceptable performance, I choose
      ;;   performance. We might be able to use org-element to ensure
      ;;   we're actually highlighting a keyword, but parsing the
      ;;   context with org-element is slow.
      ;;
      ;; 1. Find the first #+title
      (setf (point) (point-min))
      (when (re-search-forward (rx bol (or "#+title:" "#+TITLE:")) nil t)
        ;; 2. For each link inside
        (while (re-search-forward org-link-any-re (line-end-position) t)
          ;; 3. Get rid of all existing overlays at the link
          (when-let ((existing (overlays-at (1- (point)))))
            (dolist (o existing)
              (delete-overlay o)))
          ;; 4. Create an overlay for the link
          (let ((ov (make-overlay
                     (match-beginning 0)
                     (match-end 0)
                     (current-buffer)
                     :front-advance)))
            (overlay-put ov 'face 'org-link)))))))

(define-minor-mode kisaragi-org-highlight-links-in-title-mode
  "Minor mode for ensuring links in Org titles are highlighted."
  :global nil :lighter nil
  (if kisaragi-org-highlight-links-in-title-mode
      (progn
        (kisaragi-org-highlight-links-in-title)
        (add-hook 'before-save-hook #'kisaragi-org-highlight-links-in-title nil t))
    (remove-hook 'before-save-hook #'kisaragi-org-highlight-links-in-title t)))

(provide 'kisaragi-org-highlight-links-in-title)

;;; kisaragi-org-highlight-links-in-title.el ends here

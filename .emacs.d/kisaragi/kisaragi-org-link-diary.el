;;; kisaragi-org-link-diary.el --- lookup diary entry with [[diary:timestamp]]  -*- lexical-binding: t; -*-
;;; Commentary:
;; Make it possible to write
;;
;;     [[diary:2005-09]]
;;
;; in Org.
;;
;; Setup:
;; (add-to-list 'org-link-abbrev-alist '("diary" . kisaragi-org-link-abbrev-diary))
;;
;;; Code:

(require 'cl-lib)
(require 'dash)
(require 'kisaragi-constants)
(require 'kisaragi-org)

(defun k/org-link-abbrev-diary (tag)
  "Locate a diary entry as specified by TAG.

TAG is a timestamp; look for an Org property in my diary
directory (under `k/notes/') that matches TAG.

This makes it possible to write

    [[diary:2019-07-29T16:26:20+0900]]

in Org, after setting up `org-link-abbrev-alist'."
  (cl-block body
    (unless k/notes/
      (cl-return-from body nil))
    (let (;; Search for TAG as an Org property or as a heading (prefix).
          ;;
          ;; This needs to exclude TAG as a link so we don't end up
          ;; linking to the same file as the link itself.
          (regexp
           ;; "or" shouldn't be escaped in ag's regexp engine.
           ;; (other than that, ag uses regexps produced by `rx' just fine.)
           (format "%s|%s"
                   (rx-to-string `(seq ":" (+? " ") ,tag) t)
                   (rx-to-string `(seq bol (+? "*") " " ,tag) t))))
      (k/search-in-directory regexp k/notes/
        (pcase-let
            ((`(,file ,line)
              (->> (buffer-substring-no-properties (point-min) (line-end-position))
                   ;; "file name:line number:match string"
                   ;; keep file name and line number
                   ;; Assumes file does not have colons in it; breaks
                   ;; horribly if they do.
                   (s-split ":")
                   (-take 2))))
          (concat file "::" line))))))

;; add space so that we only match
(provide 'kisaragi-org-link-diary)
;;; kisaragi-org-link-diary.el ends here

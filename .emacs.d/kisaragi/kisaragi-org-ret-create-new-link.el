;;; kisaragi-org-ret-create-new-link.el --- Create a new note with RET  -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; Create a new note when I press RET on a word in Org, or when I
;; select some text and press RET.
;;
;;; Code:

(require 'kisaragi-helpers)
(require 'general)

(require 's)

(defun k/org-format-title-to-file-name (title)
  "Turn TITLE into a file name.

If TITLE is actually a URL, return it as-is."
  (if (s-matches? "https?://" title)
      title
    (format "notes:%s.org"
            (->> title
              (s-replace-regexp "\\( +\\)" "-")
              downcase))))

(defun k/org-create-new-link-region (start end)
  "Turn text between START and END into a link to a new note and visit it.

For example, when STRING is \"Google Calendar\", it will be
replaced with \"[[notes:google-calendar.org][Google Calendar]]\",
and the new file will be opened."
  (interactive "r")
  (let ((string (buffer-substring-no-properties start end)))
    (unless
        ;; do nothing if STRING contains a link
        (s-matches? (rx "[[" (0+ any) "]]") string)
      (let* ((abbrev-target (k/org-format-title-to-file-name string))
             (full-target (org-link-expand-abbrev abbrev-target)))
        (delete-region start end)
        (goto-char start)
        (insert (org-link-make-string abbrev-target string))
        (basic-save-buffer)
        (find-file full-target)
        ;; Now in the (maybe) new file
        (unless (file-exists-p buffer-file-name)
          (insert "#+title: " string))))))

(define-minor-mode kisaragi-org-ret-create-new-link-mode
  "Minor mode for Org.

When this mode is activated, pressing RET after selecting some
text in Org turns it into a link to its corresponding note;
existing links are not affected."
  :global t
  ;; I don't advise `org-open-at-point' any more, but I'm don't want
  ;; to bother changing the sementics here. So global it is.
  (cond (kisaragi-org-ret-create-new-link-mode
         (general-def
           :states 'visual
           :keymaps 'org-mode-map
           "RET" #'k/org-create-new-link-region))
        (t
         (general-unbind
           :states 'visual
           :keymaps 'org-mode-map
           "RET"))))

(provide 'kisaragi-org-ret-create-new-link)
;;; kisaragi-org-ret-create-new-link.el ends here

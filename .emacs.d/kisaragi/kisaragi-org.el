;;; kisaragi-org.el --- extra Org commands  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Extra commands for Org.
;;; Code:

(require 'kisaragi-helpers)
(require 'kisaragi-constants)
(require 'dash)
(require 'dom)
(require 'eww)
(require 'org)
(require 's)

(defun k/org-context ()
  "Experiment: Extract context around point."
  (let (elements)
    (save-excursion
      (push (org-element-at-point) elements)
      ;; This falls apart pretty quickly eg. at the first line of a
      ;; quote
      (org-backward-paragraph)
      (push (org-element-at-point) elements))
    (setq elements (nreverse elements))
    (buffer-substring-no-properties
     (-> (elt elements 1) cadr (plist-get :contents-begin))
     (-> (elt elements 0) cadr (plist-get :contents-end)))))

(defun k/org-element-extract-content (element)
  "Extract text content between ELEMENT's `:content-begin' and `:content-end' props."
  (buffer-substring-no-properties
   (plist-get (cadr element) :contents-begin)
   (plist-get (cadr element) :contents-end)))

(defun k/org-extend-today-further ()
  "Increment `org-extend-today-until'.

Good for when I stay up later than expected."
  (interactive)
  (cl-incf org-extend-today-until)
  (message "Today now extends to: %02.0f:00" (+ 24 org-extend-today-until)))

(defun k/org-toggle-hiding-stuff ()
  "Toggle link display and pretty entities."
  (interactive)
  ;; Don't show any message, just like parinfer.
  (let ((inhibit-message t))
    (k/toggle-multiple
     `((org-link-descriptive . org-toggle-link-display)
       (org-pretty-entities . org-toggle-pretty-entities)
       (org-inline-image-overlays . org-toggle-inline-images)
       (org-hide-emphasis-markers . ,(lambda ()
                                       (interactive)
                                       (setq org-hide-emphasis-markers
                                             (not org-hide-emphasis-markers))))))))

(defun k/org-wrap-subtree (type)
  "Wrap subtree under point in a block of TYPE.

For example, when TYPE is \"quote\", turn this:

  * A heading
  some |content (the bar is the cursor)

into this:

  * A heading
  #+begin_quote
  some |content (the bar is the cursor)
  #+end_quote"
  (interactive "MBlock type (such as quote, src ...): ")
  (save-mark-and-excursion
    (org-mark-subtree)
    (forward-line)
    (let ((begin (region-beginning))
          (end (region-end)))
      ;; Inserting at BEGIN first would push the text further, such
      ;; that END isn't actually the end of the subtree anymore.
      (goto-char end)
      (insert "#+end_" type "\n")
      (goto-char begin)
      (insert "#+begin_" type "\n"))))

(defun k/org-split-quote ()
  "Insert an end quote and a begin quote to split the quote block in two.

Place cursor at a blank line:

  A paragraph
  | <- here
  Another paragraph

Run this, which turns it into

  A paragraph
  #+end_quote

  #+begin_quote
  Another paragraph"
  (interactive)
  (insert "#+end_quote\n\n#+begin_quote"))

(defun k/org-wrap-subtree/quote ()
  "Wrap subtree under point in a quote block."
  (interactive)
  (k/org-wrap-subtree "quote"))

(defun k/org-wrap-element-in-quote ()
  "Put everything in the element under point in a quote block."
  (interactive)
  (save-mark-and-excursion
    (org-mark-element)
    (goto-char (region-beginning))
    (insert "#+begin_quote\n")
    (goto-char (region-end))
    (insert "#+end_quote\n")))

(defun k/org-read-file (prompt)
  "Read (with PROMPT) an Org file in this project from the user."
  (completing-read prompt
                   ;; expand paths relative to project root to absolute
                   ;; needed for k/org-apply-link-abbrev
                   (--map (f-expand
                           it
                           (projectile-project-root))
                          (projectile-current-project-files))))

(defun k/org-read-file/roam (&optional prompt)
  "Read an Org file (with PROMPT) using Org-roam's infrastructure.

Depends on Org-roam's private functions.

Return a list of '(PATH TITLE), or the user's answer (as '(PATH))
if it doesn't match an existing entry."
  (let (completions answer)
    (setq completions (org-roam--get-title-path-completions)
          answer (completing-read (or prompt "File: ") completions))
    (or (-some--> (cdr (assoc answer completions))
          (list
           (plist-get it :path)
           (plist-get it :title)))
        (list answer))))

(cl-defun k/org-files-in-org-directory (&optional (dir org-directory))
  "Return all .org files in DIR.

DIR defaults to `org-directory'."
  (->> (projectile-project-files (or dir org-directory))
    (--filter (s-matches? (rx bol (not (any ".")) (0+ any) ".org" eol)
                          it))
    (--map (f-expand it dir))))

(defun k/org-clean-up-logbook ()
  "Delete all :LOGBOOK: entries.

Despite specifying nologdone in my vocabulary file, Org still
keeps adding entries to logbooks. Instead of figuring out the
currect way to fix it, I'll just delete them every so often."
  (interactive)
  (goto-char (point-min))
  (while (search-forward ":LOGBOOK:\n" nil t)
    (save-mark-and-excursion
      (org-mark-element)
      (delete-region (region-beginning) (region-end)))))

(defun k/firefox-to-org//level-from-title (title)
  "Extract nested level information from TITLE.
If TITLE looks like \"> abc...\", it's level 1, \">> ...\" means
level 2, and so on.

Return a list: first element is the title with the prefix \">\"
removed, second element is the nested level."
  (-let (((_ arrows just-title)
          (s-match (rx (group (0+ ">"))
                       (opt " ")
                       (group (0+ any)))
                   title)))
    (list just-title
          (s-count-matches ">" arrows))))

(defun k/firefox-to-org/from-list-file (file)
  "Import Firefox bookmarks into Org from FILE.
FILE: Use the \"backup\" function in Firefox to export a JSON,
then extract list of bookmarks from it. This does not yet handle
Firefox's own folders. (But Tree Style Tab levels are
supported.)"
  (interactive "fFirefox exported (and extracted) JSON: ")
  (save-excursion
    (goto-char (point-max))
    (let ((json (json-read-file file)))
      (seq-doseq (bookmark json)
        (let-alist bookmark
          (-let* (((title level) (k/firefox-to-org//level-from-title .title))
                  (stars (make-string (1+ level) ?*)))
            (insert (format "%s %s
:PROPERTIES:
:url:  %s
:created:  %s
:END:\n"
                            stars
                            title
                            .uri
                            (k/date-iso8601 (seconds-to-time (/ .dateAdded 1000000)))))))))))

(defun k/simplenote-to-org (export)
  "Convert Simplenote EXPORT to Org headings.

Insert those headings into the end of the current buffer.

EXPORT is either the exported zip file, or the JSON
representation located in source/notes.json in the zip file. When
given a zip file, this command will unzip it for you.

Simplenote exports contain notes as individual files, but the
JSON file contains more information such as creation timestamp.

All active notes (not trashed) are inserted as Org headings at
the end the current buffer."
  (interactive "fSimplenote notes.json: ")
  (save-excursion
    (let ((parsed-json (if (f-ext? export "zip")
                           (k/with-temp-dir
                             (call-process "7z" nil nil nil "x" export)
                             (json-read-file (f-join "source" "notes.json")))
                         (json-read-file export))))
      (let-alist parsed-json
        (goto-char (point-max))
        (seq-doseq (note .activeNotes)
          (let-alist note
            (let ((date-in-current-timezone
                   ;; This loses the subsecond portion, but I don't
                   ;; really care.
                   (->> .creationDate
                     parse-iso8601-time-string
                     (format-time-string "%FT%T%z"))))
              (insert "\n* " date-in-current-timezone)
              ;; just run this as we're still on the heading
              (org-set-tags (cl-coerce .tags 'list))
              (org-set-property "created" date-in-current-timezone)
              ;; Make sure we don't insert before the property drawer
              (goto-char (point-max))
              (insert "\n"
                      ;; Simplenote entries seem to be stored with CRLF.
                      ;; Delete the CR characters.
                      (s-replace "" "" .content)))))
        (insert "\n")))))

(defun k/org-set-updated-keyword (timestamp)
  "Set \"#+updated\" keyword to TIMESTAMP.

Interactively, use current time by default; prompt for time with \\[universal-argument]."
  (interactive
   (list
    (if current-prefix-arg
        (read-string "Time (in full ISO 8601 format): " (k/date-iso8601))
      (k/date-iso8601))))
  (org-roam--set-global-prop "updated" timestamp))

(defun k/org-set-heading-timestamp (timestamp)
  "Set \"created\" property of heading under point to TIMESTAMP.

Interactively, use current time by default; prompt for time with \\[universal-argument]."
  (interactive
   (list
    (if current-prefix-arg
        (read-string "Time (in full ISO 8601 format): " (k/date-iso8601))
      (k/date-iso8601))))
  (org-set-property "created" timestamp))

(defun k/org-insert-entry-from-url (url &optional timestamp)
  "Insert an Org entry for URL.")

(defun k/org-turn-heading-into-habit ()
  "Turn heading that point is on into a habit.

This uses a custom TODO keyword \"HABIT\" if it exists; otherwise
it'll fall back to \"TODO\"."
  (interactive)
  (condition-case nil (org-todo "HABIT")
    (user-error (org-todo "TODO")))
  (org-set-property "STYLE" "habit"))

(k/defun-string-or-region k/org-turn-lines-into-links ()
  "Turn STRING into links.

STRING look like this:

Description
URL

Description
URL
..."
  (--> string
    (s-split "\n" it :omit)
    (-partition 2 it)
    (mapconcat (pcase-lambda (`(,desc ,url))
                 (format "[[%s][%s]]" url desc))
               it "\n")))

(defun k/org-sort-entries-by-rating ()
  "Sort Org entries by their RATING property.
- Items without the RATING property come first
- Then compare RATING as numbers, higher rated items come first."
  (interactive)
  (org-sort-entries
   nil ?f
   (lambda ()
     ;; `sort-subr' behaves differently if this returns nil, but in
     ;; we'll never hit that special case here.
     (list (org-entry-get (point) "rating")
           (org-entry-get (point) "item")))
   (pcase-lambda (`(,rating-a ,title-a) `(,rating-b ,title-b))
     (and rating-b ; Short circuit: rating = nil -> B should be in front
          (or (not rating-a) ; empty rating first
              (equal "" (string-trim rating-a)) ; same
              (let
                  ;; convert to numbers first
                  ((rating-a (string-to-number rating-a))
                   (rating-b (string-to-number rating-b)))
                ;; catch equal ratings, fall back explicitly to `string>'
                ;; If we want to do something else we can do it here.
                (if (= rating-a rating-b)
                    (string> title-a title-b)
                  (> rating-a rating-b))))))
   nil nil))

(defun k/org-sort-entries-by-created-timestamp ()
  "Sort Org entries by their CREATED property.
- Items without the CREATED property come first
- Then compare CREATED as ISO 8601 timestamps, earlier items come first."
  (interactive)
  (require 'canrylog-types) ; for `canrylog-iso8601-<'
  (org-sort-entries
   nil ?f
   (lambda ()
     ;; `sort-subr' behaves differently if this returns nil, but in
     ;; we'll never hit that special case here.
     (list (org-entry-get (point) "created")
           (org-entry-get (point) "item")))
   (pcase-lambda (`(,created-a ,title-a) `(,created-b ,title-b))
     (and created-b ; Short circuit: created = nil -> B should be in front
          (or (not created-a) ; empty timestamps first
              (equal "" (string-trim created-a)) ; same
              ;; catch equal timestamps explicitly, even though it's
              ;; not really going to happen, to `string>'
              (if (= created-a created-b)
                  (string> title-a title-b)
                (canrylog-iso8601-< created-b created-a)))))
   nil nil))

(defvar kisaragi-org-send-task--hist nil
  "History for `k/org-send-task' prompt.")

;; maybe this is reimplementing part of org-refile?
(defun k/org-send-task (target-file &optional reason action-keyword todo-keyword)
  "Send task under point to TARGET-FILE with REASON.

Move the top level heading and its contents under point to
`target-file', then add the ACTION name into it, optionally with a REASON.

Set the todo keyword after moving to TODO-KEYWORD if it's
present.

Add a date attribute under ACTION-KEYWORD if it's present. For
example, if ACTION-KEYWORD is \"closed\", \"CLOSED:
<yyyy-mm-dd>\" is inserted under the heading.

This is a generalized function for eg. cancelling or finishing a task
by moving it to another file."
  (interactive
   (list (completing-read "Send to file: "
                          (k/org-files-in-org-directory)
                          nil t
                          nil 'kisaragi-org-send-task--hist)))
  (let ((original-file (buffer-file-name))
        (cursor-origin (point)))
    (org-up-heading-safe)
    (let* ((element (org-element-at-point))
           (begin (plist-get (cl-second element) :begin))
           (end (plist-get (cl-second element) :end))
           (content (buffer-substring-no-properties begin end)))
      (find-file target-file)
      (goto-char (point-max))
      (insert content)
      (org-up-heading-safe)
      (when todo-keyword
        (org-todo todo-keyword))
      (end-of-line)
      (electric-newline-and-maybe-indent)
      (when action-keyword
        (insert
         (concat "  " (upcase action-keyword) ": "
                 (format-time-string "<%Y-%m-%d %a>"))))
      (when (and (stringp reason)
                 (not (equal reason "")))
        (electric-newline-and-maybe-indent)
        (insert reason))
      (insert "\n")
      (save-buffer)
      (find-file original-file)
      ;; delete the task in the original file
      (delete-region begin end)
      (save-buffer)
      (goto-char cursor-origin))))

(defun k/org-cancel-task (&optional reason)
  "Cancel task under point, with REASON.

Move the top level heading and its contents under point to cancelled.org under
`k/notes/' then add current date and the reason to the task."
  (interactive)
  (k/org-send-task (f-join k/notes/ "task-archive.org")
                   reason
                   "closed"
                   "CANCELLED"))

(defun k/org-finish-task ()
  "Finish task under point.

Move the top level heading and its contents under point to done.org under
`k/notes/' then add current date into the task."
  (interactive)
  (k/org-send-task (f-join k/notes/ "task-archive.org")
                   nil
                   "closed"
                   "DONE"))

(defun k/org-apply-link-abbrev (file)
  "Apply `org-link-abbrev-alist' to FILE.
For example, if `org-link-abbrev-alist' maps \"x\" to \"/home/\",
and FILE is \"/home/abc\", this returns \"x:abc\".

Inverse of `org-link-expand-abbrev'."
  (catch 'ret
    (setq file (file-truename file))
    (pcase-dolist (`(,key . ,abbrev) org-link-abbrev-alist)
      ;; Get the symbol property if the value is a function / symbol
      (when (symbolp abbrev)
        (setq abbrev (get abbrev 'k/file-finders-abbrev-path)))
      ;; Only do something when we actually have a string
      (when (stringp abbrev)
        ;; Resolving symlinks here allows us to treat different ways
        ;; to reach a file as the same
        (setq abbrev (file-truename abbrev))
        ;; starts-with is more accurate
        (when (s-starts-with? abbrev file)
          (throw 'ret (s-replace abbrev (concat key ":") file)))))
    (throw 'ret file)))

(provide 'kisaragi-org)
;;; kisaragi-org.el ends here

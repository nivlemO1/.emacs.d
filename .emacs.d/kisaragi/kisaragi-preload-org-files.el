;;; kisaragi-preload-org-files.el --- Load many Org files  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Try to load potentially thousands of Org files into memory "in the
;;; background", minimizing disruption and freezes.
;;; Code:

(require 'generator)
(require 'background-job)
(require 'kisaragi-org)

(iter-defun kisaragi-preload-org-files/make-iterator (files)
  "Return an iterator that loads FILES."
  (let ((index 1)
        (total (length files))
        ;; Make sure quit doesn't happen within the loop
        ;; I'm not sure if this will be restored when we're back on
        ;; the main thread though.
        (inhibit-quit t))
    (dolist (file files)
      (message "Preloading Org files (%s/%s)..." index total)
      (cl-incf index)
      (unless (get-file-buffer file)
        (iter-yield (find-file-noselect file))))))

(defun kisaragi-preload-org-files/do-preload (&rest _)
  "Preload Org files."
  (interactive)
  (background-job-start
   (kisaragi-preload-org-files/make-iterator
    (k/org-files-in-org-directory))
   (lambda ()
     (message "All Org files loaded!")))
  ;; don't bother printing the iterator, which, as it contains paths
  ;; to all 2000+ files in my org directory, is actually what blocks.
  nil)

(provide 'kisaragi-preload-org-files)
;;; kisaragi-preload-org-files.el ends here

;;; kisaragi-tags.el --- My tags  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Functions to manage a tag library.
;;; Code:

;; Tags are stored as Org headings without children.

(require 'org)
(require 'f)

(defvar kisaragi-tags/file (f-join k/notes/ "tags.org")
  "Path to the tag library file.")

(defun k/org-has-children? ()
  "Does the current Org heading have child entries?

Cursor should be placed on the heading line."
  (let ((this-level (or (org-current-level) 0))
        (next-level (save-excursion
                      (or (and (goto-char (line-end-position))
                               (re-search-forward org-heading-regexp nil t)
                               (org-current-level))
                          0))))
    (< this-level next-level)))

(defun kisaragi-tags/library/tags-in-buffer ()
  "Return all tags in the current buffer."
  (let (ret)
    (org-map-region
     (lambda ()
       (unless (k/org-has-children?)
         (push (org-entry-get nil "ITEM") ret)))
     (point-min)
     (point-max))
    ret))

(defun kisaragi-tags/library ()
  "Return all tags in library."
  (with-temp-buffer
    (org-mode)
    (insert-file-contents-literally kisaragi-tags/file)
    (decode-coding-region (point-min) (point-max) 'utf-8)
    (kisaragi-tags/library/tags-in-buffer)))

(defvar kisaragi-tags/insert/history nil)

(defun kisaragi-tags/insert ()
  "Select and insert a tag from the library."
  (interactive)
  (let ((tag (completing-read
              "Tag: " (kisaragi-tags/library)
              nil t nil 'kisaragi-tags/insert/history)))
    (insert tag)))

(provide 'kisaragi-tags)
;;; kisaragi-tags.el ends here

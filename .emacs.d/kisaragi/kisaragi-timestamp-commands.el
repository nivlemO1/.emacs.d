;;; kisaragi-timestamp-commands.el --- Commands related to timestamp formatting -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'kisaragi-helpers)
(require 'dash)
(require 's)

(k/defun-string-or-region kisaragi-timestamp-commands/iso8601-no-symbol-to-full ()
  "Turn STRING, an ISO 8601 timestamp without the dashes and colons, into a full timestamp.

20000102T030405+0600 -> 2000-01-02T03:04:05+0600"
  (save-match-data
    (->> string
         (s-match-strings-all "\\(....\\)\\(..\\)\\(..\\)T\\(..\\)\\(..\\)\\(..\\)\\(.*\\)")
         cdar
         (apply #'format "%s-%s-%sT%s:%s:%s%s"))))

(k/defun-string-or-region kisaragi-timestamp-commands/commit-timestamp-to-iso8601 ()
  "Convert STRING, a timestamp seen in git commits, to ISO 8601.

Preserves the zone information.

While not preserving timezone doesn't mean the time would be
offset (it'll be taken into account and turned into the system's
current timezone), it does mean that I wouldn't be able to see
which timezone I used at the time."
  (let* ((decoded (parse-time-string string))
         (zone (car (last decoded)))
         (encoded (encode-time decoded)))
    (format-time-string "%FT%T%z" encoded zone)))

(k/defun-string-or-region kisaragi-timestamp-commands/convert-iso8601-timezone nil
  "Convert STRING, an ISO 8601 timestamp, to use local timezone.

Resulting timestamp still represents the same moment in time as
the original."
  (k/date-iso8601
   (parse-iso8601-time-string
    string)))

(provide 'kisaragi-timestamp-commands)

;;; kisaragi-timestamp-commands.el ends here

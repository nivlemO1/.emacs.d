;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'f)
(require 'dash)

(defun k/rename-screenshots-from-android (directory)
  "Rename screenshots under DIRECTORY made by Android into a ISO 8601 format."
  (let ((default-directory directory))
    (cl-loop for file in
             ;; files named Screenshot_...
             (->> (directory-files ".")
                  (--filter (and (s-contains? "_" it)
                                 (s-starts-with? "Screenshot" it))))
             collect
             (let* ((datetime (->> (f-base file)
                                   (s-split "_")
                                   cl-second))
                    (T-datetime (s-replace "-" "T" datetime)))
               (message "rename Screenshot_%s to Screenshot-%s" datetime T-datetime)
               ;; does another file with T-datetime already exist?
               ;; (commented out as they've all been deleted)
               ;; (when (directory-files "." nil
               ;;                        (format "^Screenshot-%s" T-datetime))
               ;;   ;; delete this file then
               ;;   (delete-file file))))

               ;; checked beforehand that all underlined files are pngs
               (rename-file file (format "Screenshot-%s.png" T-datetime))))))

(defun k/rename-screenshots-from-comico (directory)
  "Rename screenshots under DIRECTORY made by Comico into a ISO 8601 format."
  (let ((default-directory directory))
    (--map
     (let* ((timestamp (->> (f-base it)
                            (s-split "_")
                            cl-second))
            (unixtime (-> timestamp
                          string-to-number
                          ;; comico timestamps are miliseconds
                          (* 0.001)
                          seconds-to-time))
            (iso8601 (concat (format-time-string "%Y%m%dT%H%M%S" unixtime)
                             "." (substring timestamp -3)
                             (format-time-string "%z"))))
       (rename-file it (concat "Screenshot-" iso8601 ".jpg")))
     (directory-files "." nil "^comico"))))

;; Local Variables:
;; mode: lisp-interaction
;; End:

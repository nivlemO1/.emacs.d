;;; rainbow-parinfer.el --- Show Parinfer state with rainbow delimiters -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This sets up parinfer to enable rainbow delimiters in paren mode,
;; and disable it in indent mode.

;;; Code:

(require 'parinfer)
(require 'rainbow-delimiters)

(defun rainbow-parinfer--update (&optional mode)
  "Enable `rainbow-delimiters' if MODE is `paren'; otherwise disable it."
  ;; `parinfer-switch-mode-hook' gets passed an argument unlike what
  ;; the name suggests.
  (interactive)
  (if (eq (or mode (parinfer-current-mode)) 'paren)
      (rainbow-delimiters-mode)
    (rainbow-delimiters-mode -1)))

(define-minor-mode rainbow-parinfer-mode
  "Show whether Parinfer is in indent or paren mode with rainbow delimiters."
  :global t
  (cond
   (rainbow-parinfer-mode
    (add-hook 'parinfer-switch-mode-hook #'rainbow-parinfer--update)
    (add-hook 'parinfer-mode-hook #'rainbow-parinfer--update))
   (t
    (remove-hook 'parinfer-switch-mode-hook #'rainbow-parinfer--update)
    (remove-hook 'parinfer-mode-hook #'rainbow-parinfer--update))))

(provide 'rainbow-parinfer)

;;; rainbow-parinfer.el ends here

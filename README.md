# Kisaragi Hiu’s Emacs Configuration

- [straight.el](https://github.com/raxod502/straight.el)
- [general.el](https://github.com/noctuid/general.el)

## License

GPL v3, see LICENSE.
